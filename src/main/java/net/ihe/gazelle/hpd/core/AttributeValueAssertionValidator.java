package net.ihe.gazelle.hpd.core;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        AttributeValueAssertion
  * package :   core
  * 
  */
public final class AttributeValueAssertionValidator{


    private AttributeValueAssertionValidator() {}



	/**
	* Validation of instance by a constraint : constraint_dsml_attributeValueAssertionNamePattern
	* Attributes of type AttributeDescriptionValue must respect the following pattern ((([0-2](\\.[0-9]+)+)|([a-zA-Z]+([a-zA-Z0-9]|[-])*))(;([a-zA-Z0-9]|[-])+)*) (see DSMLv2 schema)
	* 
	*/
	private static boolean _validateAttributeValueAssertion_Constraint_dsml_attributeValueAssertionNamePattern(net.ihe.gazelle.hpd.AttributeValueAssertion aClass){
		return (!(((String) aClass.getName()) == null) && aClass.matches(aClass.getName(), "((([0-2](\\.[0-9]+)+)|([a-zA-Z]+([a-zA-Z0-9]|[\\-])*))(;([a-zA-Z0-9]|[\\-])+)*)"));
		
	}				

	 /**
     * Validate an instance of {@link  AttributeValueAssertion}
     * 
     */
    public static void _validateAttributeValueAssertion(net.ihe.gazelle.hpd.AttributeValueAssertion aClass, String location, List<Notification> diagnostic) {
		executeCons_AttributeValueAssertion_Constraint_dsml_attributeValueAssertionNamePattern(aClass, location, diagnostic);
    }

	private static void executeCons_AttributeValueAssertion_Constraint_dsml_attributeValueAssertionNamePattern(net.ihe.gazelle.hpd.AttributeValueAssertion aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAttributeValueAssertion_Constraint_dsml_attributeValueAssertionNamePattern(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_dsml_attributeValueAssertionNamePattern");
		notif.setDescription("Attributes of type AttributeDescriptionValue must respect the following pattern ((([0-2](\\.[0-9]+)+)|([a-zA-Z]+([a-zA-Z0-9]|[-])*))(;([a-zA-Z0-9]|[-])+)*) (see DSMLv2 schema)");
		notif.setLocation(location);
		notif.setIdentifiant("core-AttributeValueAssertion-constraint_dsml_attributeValueAssertionNamePattern");
		
		diagnostic.add(notif);
	}



}
