package net.ihe.gazelle.hpd.core;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        ExtendedResponse
  * package :   core
  * 
  */
public final class ExtendedResponseValidator{


    private ExtendedResponseValidator() {}



	/**
	* Validation of instance by a constraint : constraint_dsml_extendedResponseNamePattern
	* Attributes of type NumericOID must respect the following pattern [0-2]\\.[0-9]+(\\.[0-9]+)* (see DSMLv2 schema)
	* 
	*/
	private static boolean _validateExtendedResponse_Constraint_dsml_extendedResponseNamePattern(net.ihe.gazelle.hpd.ExtendedResponse aClass){
		return (!(((String) aClass.getResponseName()) == null) && aClass.matches(aClass.getResponseName(), "[0-2]\\.[0-9]+(\\.[0-9]+)*"));
		
	}				

	 /**
     * Validate an instance of {@link  ExtendedResponse}
     * 
     */
    public static void _validateExtendedResponse(net.ihe.gazelle.hpd.ExtendedResponse aClass, String location, List<Notification> diagnostic) {
		executeCons_ExtendedResponse_Constraint_dsml_extendedResponseNamePattern(aClass, location, diagnostic);
    }

	private static void executeCons_ExtendedResponse_Constraint_dsml_extendedResponseNamePattern(net.ihe.gazelle.hpd.ExtendedResponse aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateExtendedResponse_Constraint_dsml_extendedResponseNamePattern(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_dsml_extendedResponseNamePattern");
		notif.setDescription("Attributes of type NumericOID must respect the following pattern [0-2]\\.[0-9]+(\\.[0-9]+)* (see DSMLv2 schema)");
		notif.setLocation(location);
		notif.setIdentifiant("core-ExtendedResponse-constraint_dsml_extendedResponseNamePattern");
		
		diagnostic.add(notif);
	}



}
