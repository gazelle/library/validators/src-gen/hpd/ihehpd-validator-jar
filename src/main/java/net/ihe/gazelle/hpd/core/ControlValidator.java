package net.ihe.gazelle.hpd.core;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        Control
  * package :   core
  * 
  */
public final class ControlValidator{


    private ControlValidator() {}



	/**
	* Validation of instance by a constraint : constraint_dsml_controlTypePattern
	* Attributes of type NumericOID must respect the following pattern [0-2]\\.[0-9]+(\\.[0-9]+)* (see DSMLv2 schema)
	* 
	*/
	private static boolean _validateControl_Constraint_dsml_controlTypePattern(net.ihe.gazelle.hpd.Control aClass){
		return (!(((String) aClass.getType()) == null) && aClass.matches(aClass.getType(), "[0-2]\\.[0-9]+(\\.[0-9]+)*"));
		
	}				

	 /**
     * Validate an instance of {@link  Control}
     * 
     */
    public static void _validateControl(net.ihe.gazelle.hpd.Control aClass, String location, List<Notification> diagnostic) {
		executeCons_Control_Constraint_dsml_controlTypePattern(aClass, location, diagnostic);
    }

	private static void executeCons_Control_Constraint_dsml_controlTypePattern(net.ihe.gazelle.hpd.Control aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateControl_Constraint_dsml_controlTypePattern(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_dsml_controlTypePattern");
		notif.setDescription("Attributes of type NumericOID must respect the following pattern [0-2]\\.[0-9]+(\\.[0-9]+)* (see DSMLv2 schema)");
		notif.setLocation(location);
		notif.setIdentifiant("core-Control-constraint_dsml_controlTypePattern");
		
		diagnostic.add(notif);
	}



}
