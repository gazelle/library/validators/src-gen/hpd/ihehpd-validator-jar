package net.ihe.gazelle.hpd.core;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        MatchingRuleAssertion
  * package :   core
  * 
  */
public final class MatchingRuleAssertionValidator{


    private MatchingRuleAssertionValidator() {}



	/**
	* Validation of instance by a constraint : constraint_dsml_matchingRuleAssertionNamePattern
	* Attributes of type AttributeDescriptionValue must respect the following pattern ((([0-2](\\.[0-9]+)+)|([a-zA-Z]+([a-zA-Z0-9]|[-])*))(;([a-zA-Z0-9]|[-])+)*) (see DSMLv2 schema)
	* 
	*/
	private static boolean _validateMatchingRuleAssertion_Constraint_dsml_matchingRuleAssertionNamePattern(net.ihe.gazelle.hpd.MatchingRuleAssertion aClass){
		return (!(((String) aClass.getName()) == null) && aClass.matches(aClass.getName(), "((([0-2](\\.[0-9]+)+)|([a-zA-Z]+([a-zA-Z0-9]|[\\-])*))(;([a-zA-Z0-9]|[\\-])+)*)"));
		
	}				

	 /**
     * Validate an instance of {@link  MatchingRuleAssertion}
     * 
     */
    public static void _validateMatchingRuleAssertion(net.ihe.gazelle.hpd.MatchingRuleAssertion aClass, String location, List<Notification> diagnostic) {
		executeCons_MatchingRuleAssertion_Constraint_dsml_matchingRuleAssertionNamePattern(aClass, location, diagnostic);
    }

	private static void executeCons_MatchingRuleAssertion_Constraint_dsml_matchingRuleAssertionNamePattern(net.ihe.gazelle.hpd.MatchingRuleAssertion aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateMatchingRuleAssertion_Constraint_dsml_matchingRuleAssertionNamePattern(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_dsml_matchingRuleAssertionNamePattern");
		notif.setDescription("Attributes of type AttributeDescriptionValue must respect the following pattern ((([0-2](\\.[0-9]+)+)|([a-zA-Z]+([a-zA-Z0-9]|[-])*))(;([a-zA-Z0-9]|[-])+)*) (see DSMLv2 schema)");
		notif.setLocation(location);
		notif.setIdentifiant("core-MatchingRuleAssertion-constraint_dsml_matchingRuleAssertionNamePattern");
		
		diagnostic.add(notif);
	}



}
