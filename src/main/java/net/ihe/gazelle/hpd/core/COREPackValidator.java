package net.ihe.gazelle.hpd.core;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





public class COREPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public COREPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hpd.AttributeDescription){
			net.ihe.gazelle.hpd.AttributeDescription aClass = ( net.ihe.gazelle.hpd.AttributeDescription)obj;
		}
		if (obj instanceof net.ihe.gazelle.hpd.AttributeValueAssertion){
			net.ihe.gazelle.hpd.AttributeValueAssertion aClass = ( net.ihe.gazelle.hpd.AttributeValueAssertion)obj;
		}
		if (obj instanceof net.ihe.gazelle.hpd.Control){
			net.ihe.gazelle.hpd.Control aClass = ( net.ihe.gazelle.hpd.Control)obj;
		}
		if (obj instanceof net.ihe.gazelle.hpd.DsmlAttr){
			net.ihe.gazelle.hpd.DsmlAttr aClass = ( net.ihe.gazelle.hpd.DsmlAttr)obj;
		}
		if (obj instanceof net.ihe.gazelle.hpd.DsmlModification){
			net.ihe.gazelle.hpd.DsmlModification aClass = ( net.ihe.gazelle.hpd.DsmlModification)obj;
		}
		if (obj instanceof net.ihe.gazelle.hpd.ExtendedRequest){
			net.ihe.gazelle.hpd.ExtendedRequest aClass = ( net.ihe.gazelle.hpd.ExtendedRequest)obj;
		}
		if (obj instanceof net.ihe.gazelle.hpd.ExtendedResponse){
			net.ihe.gazelle.hpd.ExtendedResponse aClass = ( net.ihe.gazelle.hpd.ExtendedResponse)obj;
		}
		if (obj instanceof net.ihe.gazelle.hpd.MatchingRuleAssertion){
			net.ihe.gazelle.hpd.MatchingRuleAssertion aClass = ( net.ihe.gazelle.hpd.MatchingRuleAssertion)obj;
		}
		if (obj instanceof net.ihe.gazelle.hpd.SubstringFilter){
			net.ihe.gazelle.hpd.SubstringFilter aClass = ( net.ihe.gazelle.hpd.SubstringFilter)obj;
		}
	
	}

}

