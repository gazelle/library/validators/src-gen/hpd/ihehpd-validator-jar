package net.ihe.gazelle.hpd.core;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        AttributeDescription
  * package :   core
  * 
  */
public final class AttributeDescriptionValidator{


    private AttributeDescriptionValidator() {}



	/**
	* Validation of instance by a constraint : constraint_dsml_attributeDescriptionNamePattern
	* Attributes of type AttributeDescriptionValue must respect the following pattern ((([0-2](\\.[0-9]+)+)|([a-zA-Z]+([a-zA-Z0-9]|[-])*))(;([a-zA-Z0-9]|[-])+)*) (see DSMLv2 schema)
	* 
	*/
	private static boolean _validateAttributeDescription_Constraint_dsml_attributeDescriptionNamePattern(net.ihe.gazelle.hpd.AttributeDescription aClass){
		return (!(((String) aClass.getName()) == null) && aClass.matches(aClass.getName(), "((([0-2](\\.[0-9]+)+)|([a-zA-Z]+([a-zA-Z0-9]|[\\-])*))(;([a-zA-Z0-9]|[\\-])+)*)"));
		
	}				

	 /**
     * Validate an instance of {@link  AttributeDescription}
     * 
     */
    public static void _validateAttributeDescription(net.ihe.gazelle.hpd.AttributeDescription aClass, String location, List<Notification> diagnostic) {
		executeCons_AttributeDescription_Constraint_dsml_attributeDescriptionNamePattern(aClass, location, diagnostic);
    }

	private static void executeCons_AttributeDescription_Constraint_dsml_attributeDescriptionNamePattern(net.ihe.gazelle.hpd.AttributeDescription aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAttributeDescription_Constraint_dsml_attributeDescriptionNamePattern(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_dsml_attributeDescriptionNamePattern");
		notif.setDescription("Attributes of type AttributeDescriptionValue must respect the following pattern ((([0-2](\\.[0-9]+)+)|([a-zA-Z]+([a-zA-Z0-9]|[-])*))(;([a-zA-Z0-9]|[-])+)*) (see DSMLv2 schema)");
		notif.setLocation(location);
		notif.setIdentifiant("core-AttributeDescription-constraint_dsml_attributeDescriptionNamePattern");
		
		diagnostic.add(notif);
	}



}
