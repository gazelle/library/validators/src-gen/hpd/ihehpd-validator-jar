package net.ihe.gazelle.hpd.core;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        ExtendedRequest
  * package :   core
  * 
  */
public final class ExtendedRequestValidator{


    private ExtendedRequestValidator() {}



	/**
	* Validation of instance by a constraint : constraint_dsml_extendedRequestNamePattern
	* Attributes of type NumericOID must respect the following pattern [0-2]\\.[0-9]+(\\.[0-9]+)* (see DSMLv2 schema)
	* 
	*/
	private static boolean _validateExtendedRequest_Constraint_dsml_extendedRequestNamePattern(net.ihe.gazelle.hpd.ExtendedRequest aClass){
		return (!(((String) aClass.getRequestName()) == null) && aClass.matches(aClass.getRequestName(), "[0-2]\\.[0-9]+(\\.[0-9]+)*"));
		
	}				

	 /**
     * Validate an instance of {@link  ExtendedRequest}
     * 
     */
    public static void _validateExtendedRequest(net.ihe.gazelle.hpd.ExtendedRequest aClass, String location, List<Notification> diagnostic) {
		executeCons_ExtendedRequest_Constraint_dsml_extendedRequestNamePattern(aClass, location, diagnostic);
    }

	private static void executeCons_ExtendedRequest_Constraint_dsml_extendedRequestNamePattern(net.ihe.gazelle.hpd.ExtendedRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateExtendedRequest_Constraint_dsml_extendedRequestNamePattern(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_dsml_extendedRequestNamePattern");
		notif.setDescription("Attributes of type NumericOID must respect the following pattern [0-2]\\.[0-9]+(\\.[0-9]+)* (see DSMLv2 schema)");
		notif.setLocation(location);
		notif.setIdentifiant("core-ExtendedRequest-constraint_dsml_extendedRequestNamePattern");
		
		diagnostic.add(notif);
	}



}
