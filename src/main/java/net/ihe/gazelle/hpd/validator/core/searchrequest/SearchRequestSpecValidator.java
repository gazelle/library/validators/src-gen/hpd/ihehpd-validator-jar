package net.ihe.gazelle.hpd.validator.core.searchrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        SearchRequestSpec
  * package :   searchRequest
  * Constraint Spec Class
  * class of test : SearchRequest
  * 
  */
public final class SearchRequestSpecValidator{


    private SearchRequestSpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_searchRequest_dnSyntax
	* dn attribute SHALL be formatted as a Distinguish name string.
	*
	*/
	private static boolean _validateSearchRequestSpec_Constraint_hpd_searchRequest_dnSyntax(net.ihe.gazelle.hpd.SearchRequest aClass){
		return aClass.matches(((String) aClass.getDn()), "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$");
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_searchRequest_dnValidOu
	* Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization, ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)               
	*
	*/
	private static boolean _validateSearchRequestSpec_Constraint_hpd_searchRequest_dnValidOu(net.ihe.gazelle.hpd.SearchRequest aClass){
		return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=(hcprofessional|hcregulatedorganization|relationship|hpdcredential)){1}.*");
		
	}

	/**
	* Validation of class-constraint : SearchRequestSpec
    * Verify if an element of type SearchRequestSpec can be validated by SearchRequestSpec
	*
	*/
	public static boolean _isSearchRequestSpec(net.ihe.gazelle.hpd.SearchRequest aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   SearchRequestSpec
     * class ::  net.ihe.gazelle.hpd.SearchRequest
     * 
     */
    public static void _validateSearchRequestSpec(net.ihe.gazelle.hpd.SearchRequest aClass, String location, List<Notification> diagnostic) {
		if (_isSearchRequestSpec(aClass)){
			executeCons_SearchRequestSpec_Constraint_hpd_searchRequest_dnSyntax(aClass, location, diagnostic);
			executeCons_SearchRequestSpec_Constraint_hpd_searchRequest_dnValidOu(aClass, location, diagnostic);
		}
	}

	private static void executeCons_SearchRequestSpec_Constraint_hpd_searchRequest_dnSyntax(net.ihe.gazelle.hpd.SearchRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSearchRequestSpec_Constraint_hpd_searchRequest_dnSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_searchRequest_dnSyntax");
		notif.setDescription("dn attribute SHALL be formatted as a Distinguish name string.");
		notif.setLocation(location);
		notif.setIdentifiant("searchRequest-SearchRequestSpec-constraint_hpd_searchRequest_dnSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-093"));
		diagnostic.add(notif);
	}

	private static void executeCons_SearchRequestSpec_Constraint_hpd_searchRequest_dnValidOu(net.ihe.gazelle.hpd.SearchRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSearchRequestSpec_Constraint_hpd_searchRequest_dnValidOu(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_searchRequest_dnValidOu");
		notif.setDescription("Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization, ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)");
		notif.setLocation(location);
		notif.setIdentifiant("searchRequest-SearchRequestSpec-constraint_hpd_searchRequest_dnValidOu");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-094"));
		diagnostic.add(notif);
	}

}
