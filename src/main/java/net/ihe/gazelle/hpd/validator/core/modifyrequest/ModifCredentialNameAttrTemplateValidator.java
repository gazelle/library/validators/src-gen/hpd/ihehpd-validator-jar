package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifCredentialNameAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifCredentialNameAttrTemplateValidator{


    private ModifCredentialNameAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_credentialNameSyntax
	* credentialName attribute SHALL follow the ISO21091 naming format as that of the HCStandardRole or credential@Locality (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)
	*
	*/
	private static boolean _validateModifCredentialNameAttrTemplate_Constraint_hpd_modifyRequest_credentialNameSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, ".+[@]{1}.+")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_credentialNameValueCard
	* credentialName attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)
	*
	*/
	private static boolean _validateModifCredentialNameAttrTemplate_Constraint_hpd_modifyRequest_credentialNameValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifCredentialNameAttrTemplate
    * Verify if an element can be token as a Template of type modifCredentialNameAttrTemplate
	*
	*/
	private static boolean _isModifCredentialNameAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("credentialname");
				
	}
	/**
	* Validation of class-constraint : modifCredentialNameAttrTemplate
    * Verify if an element of type modifCredentialNameAttrTemplate can be validated by modifCredentialNameAttrTemplate
	*
	*/
	public static boolean _isModifCredentialNameAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifCredentialNameAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifCredentialNameAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifCredentialNameAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifCredentialNameAttrTemplate(aClass)){
			executeCons_ModifCredentialNameAttrTemplate_Constraint_hpd_modifyRequest_credentialNameSyntax(aClass, location, diagnostic);
			executeCons_ModifCredentialNameAttrTemplate_Constraint_hpd_modifyRequest_credentialNameValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifCredentialNameAttrTemplate_Constraint_hpd_modifyRequest_credentialNameSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifCredentialNameAttrTemplate_Constraint_hpd_modifyRequest_credentialNameSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_credentialNameSyntax");
		notif.setDescription("credentialName attribute SHALL follow the ISO21091 naming format as that of the HCStandardRole or credential@Locality (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifCredentialNameAttrTemplate-constraint_hpd_modifyRequest_credentialNameSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-110"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifCredentialNameAttrTemplate_Constraint_hpd_modifyRequest_credentialNameValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifCredentialNameAttrTemplate_Constraint_hpd_modifyRequest_credentialNameValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_credentialNameValueCard");
		notif.setDescription("credentialName attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifCredentialNameAttrTemplate-constraint_hpd_modifyRequest_credentialNameValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-109"));
		diagnostic.add(notif);
	}

}
