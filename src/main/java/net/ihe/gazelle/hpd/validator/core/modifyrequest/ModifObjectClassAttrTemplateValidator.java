package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifObjectClassAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifObjectClassAttrTemplateValidator{


    private ModifObjectClassAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_objectClassValue
	* objectClass attribute SHALL be present in every entry, one of the values is either  top  or  alias  (see RFC 2256 section 5.1 objectClass)
	*
	*/
	private static boolean _validateModifObjectClassAttrTemplate_Constraint_hpd_modifyRequest_objectClassValue(net.ihe.gazelle.hpd.DsmlModification aClass){
		java.util.ArrayList<String> result1;
		result1 = new java.util.ArrayList<String>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if ((anElement1.toLowerCase().equals("top") || anElement1.toLowerCase().equals("alias"))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_objectClassValueCard
	*  The objectClass attribute is present in every entry, with at least two values.  (see RFC 2256 section 5.1 objectClass)
	*
	*/
	private static boolean _validateModifObjectClassAttrTemplate_Constraint_hpd_modifyRequest_objectClassValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) >= new Integer(2));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifObjectClassAttrTemplate
    * Verify if an element can be token as a Template of type modifObjectClassAttrTemplate
	*
	*/
	private static boolean _isModifObjectClassAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("objectclass");
				
	}
	/**
	* Validation of class-constraint : modifObjectClassAttrTemplate
    * Verify if an element of type modifObjectClassAttrTemplate can be validated by modifObjectClassAttrTemplate
	*
	*/
	public static boolean _isModifObjectClassAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifObjectClassAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifObjectClassAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifObjectClassAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifObjectClassAttrTemplate(aClass)){
			executeCons_ModifObjectClassAttrTemplate_Constraint_hpd_modifyRequest_objectClassValue(aClass, location, diagnostic);
			executeCons_ModifObjectClassAttrTemplate_Constraint_hpd_modifyRequest_objectClassValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifObjectClassAttrTemplate_Constraint_hpd_modifyRequest_objectClassValue(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifObjectClassAttrTemplate_Constraint_hpd_modifyRequest_objectClassValue(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_objectClassValue");
		notif.setDescription("objectClass attribute SHALL be present in every entry, one of the values is either  top  or  alias  (see RFC 2256 section 5.1 objectClass)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifObjectClassAttrTemplate-constraint_hpd_modifyRequest_objectClassValue");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-147"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifObjectClassAttrTemplate_Constraint_hpd_modifyRequest_objectClassValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifObjectClassAttrTemplate_Constraint_hpd_modifyRequest_objectClassValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_objectClassValueCard");
		notif.setDescription("The objectClass attribute is present in every entry, with at least two values.  (see RFC 2256 section 5.1 objectClass)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifObjectClassAttrTemplate-constraint_hpd_modifyRequest_objectClassValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-146"));
		diagnostic.add(notif);
	}

}
