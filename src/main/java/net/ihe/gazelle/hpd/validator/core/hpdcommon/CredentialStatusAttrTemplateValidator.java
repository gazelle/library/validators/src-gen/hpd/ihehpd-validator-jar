package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        credentialStatusAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class CredentialStatusAttrTemplateValidator{


    private CredentialStatusAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_credentialStatusValue
	* Valid values for credentialStatus attribute are Active, Inactive, Revoked and Suspended (see Table 3.58.4.1.2.3-1: Status Code Category Values)
	*
	*/
	private static boolean _validateCredentialStatusAttrTemplate_Constraint_hpd_common_credentialStatusValue(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!(((anElement1.toLowerCase().equals("active") || anElement1.toLowerCase().equals("inactive")) || anElement1.toLowerCase().equals("revoked")) || anElement1.toLowerCase().equals("suspended"))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_common_credentialStatusValueCard
	* credentialStatus attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-3: HPDProviderCredential Optional Attributes)
	*
	*/
	private static boolean _validateCredentialStatusAttrTemplate_Constraint_hpd_common_credentialStatusValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : credentialStatusAttrTemplate
    * Verify if an element can be token as a Template of type credentialStatusAttrTemplate
	*
	*/
	private static boolean _isCredentialStatusAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("credentialstatus");
				
	}
	/**
	* Validation of class-constraint : credentialStatusAttrTemplate
    * Verify if an element of type credentialStatusAttrTemplate can be validated by credentialStatusAttrTemplate
	*
	*/
	public static boolean _isCredentialStatusAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isCredentialStatusAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   credentialStatusAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateCredentialStatusAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isCredentialStatusAttrTemplate(aClass)){
			executeCons_CredentialStatusAttrTemplate_Constraint_hpd_common_credentialStatusValue(aClass, location, diagnostic);
			executeCons_CredentialStatusAttrTemplate_Constraint_hpd_common_credentialStatusValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CredentialStatusAttrTemplate_Constraint_hpd_common_credentialStatusValue(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCredentialStatusAttrTemplate_Constraint_hpd_common_credentialStatusValue(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_credentialStatusValue");
		notif.setDescription("Valid values for credentialStatus attribute are Active, Inactive, Revoked and Suspended (see Table 3.58.4.1.2.3-1: Status Code Category Values)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-credentialStatusAttrTemplate-constraint_hpd_common_credentialStatusValue");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-021"));
		diagnostic.add(notif);
	}

	private static void executeCons_CredentialStatusAttrTemplate_Constraint_hpd_common_credentialStatusValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCredentialStatusAttrTemplate_Constraint_hpd_common_credentialStatusValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_credentialStatusValueCard");
		notif.setDescription("credentialStatus attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-3: HPDProviderCredential Optional Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-credentialStatusAttrTemplate-constraint_hpd_common_credentialStatusValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-020"));
		diagnostic.add(notif);
	}

}
