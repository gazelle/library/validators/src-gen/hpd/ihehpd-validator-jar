package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        hpdProviderLegalAddressAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HpdProviderLegalAddressAttrTemplateValidator{


    private HpdProviderLegalAddressAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_hpdProviderLegalAddressValueCard
	* hpdProviderLegalAddress attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-1 HPDProvider Optional Attributes, CP-ITI-601-04)
	*
	*/
	private static boolean _validateHpdProviderLegalAddressAttrTemplate_Constraint_hpd_common_hpdProviderLegalAddressValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : hpdProviderLegalAddressAttrTemplate
    * Verify if an element can be token as a Template of type hpdProviderLegalAddressAttrTemplate
	*
	*/
	private static boolean _isHpdProviderLegalAddressAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hpdproviderlegaladdress");
				
	}
	/**
	* Validation of class-constraint : hpdProviderLegalAddressAttrTemplate
    * Verify if an element of type hpdProviderLegalAddressAttrTemplate can be validated by hpdProviderLegalAddressAttrTemplate
	*
	*/
	public static boolean _isHpdProviderLegalAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHpdProviderLegalAddressAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   hpdProviderLegalAddressAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHpdProviderLegalAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHpdProviderLegalAddressAttrTemplate(aClass)){
			executeCons_HpdProviderLegalAddressAttrTemplate_Constraint_hpd_common_hpdProviderLegalAddressValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HpdProviderLegalAddressAttrTemplate_Constraint_hpd_common_hpdProviderLegalAddressValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHpdProviderLegalAddressAttrTemplate_Constraint_hpd_common_hpdProviderLegalAddressValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_hpdProviderLegalAddressValueCard");
		notif.setDescription("hpdProviderLegalAddress attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-1 HPDProvider Optional Attributes, CP-ITI-601-04)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-hpdProviderLegalAddressAttrTemplate-constraint_hpd_common_hpdProviderLegalAddressValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-041"));
		diagnostic.add(notif);
	}

}
