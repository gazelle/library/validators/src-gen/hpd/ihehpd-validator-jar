package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        credentialTypeAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class CredentialTypeAttrTemplateValidator{


    private CredentialTypeAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_credentialTypeValueCard
	* credentialType attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)
	*
	*/
	private static boolean _validateCredentialTypeAttrTemplate_Constraint_hpd_common_credentialTypeValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : credentialTypeAttrTemplate
    * Verify if an element can be token as a Template of type credentialTypeAttrTemplate
	*
	*/
	private static boolean _isCredentialTypeAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("credentialtype");
				
	}
	/**
	* Validation of class-constraint : credentialTypeAttrTemplate
    * Verify if an element of type credentialTypeAttrTemplate can be validated by credentialTypeAttrTemplate
	*
	*/
	public static boolean _isCredentialTypeAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isCredentialTypeAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   credentialTypeAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateCredentialTypeAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isCredentialTypeAttrTemplate(aClass)){
			executeCons_CredentialTypeAttrTemplate_Constraint_hpd_common_credentialTypeValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CredentialTypeAttrTemplate_Constraint_hpd_common_credentialTypeValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCredentialTypeAttrTemplate_Constraint_hpd_common_credentialTypeValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_credentialTypeValueCard");
		notif.setDescription("credentialType attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-credentialTypeAttrTemplate-constraint_hpd_common_credentialTypeValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-012"));
		diagnostic.add(notif);
	}

}
