package net.ihe.gazelle.hpd.validator.core.ldapresult;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





public class LDAPRESULTPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public LDAPRESULTPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hpd.LDAPResult){
			net.ihe.gazelle.hpd.LDAPResult aClass = ( net.ihe.gazelle.hpd.LDAPResult)obj;
			LDAPResultSpecValidator._validateLDAPResultSpec(aClass, location, diagnostic);
		}
	
	}

}

