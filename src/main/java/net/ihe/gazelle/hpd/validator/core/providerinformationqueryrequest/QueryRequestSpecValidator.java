package net.ihe.gazelle.hpd.validator.core.providerinformationqueryrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        queryRequestSpec
  * package :   providerInformationQueryRequest
  * Constraint Spec Class
  * class of test : BatchRequest
  * 
  */
public final class QueryRequestSpecValidator{


    private QueryRequestSpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_queryRequestAllowedElements
	* A Provider Information Query Request can only contains searchRequest elements.
	*
	*/
	private static boolean _validateQueryRequestSpec_Constraint_hpd_queryRequestAllowedElements(net.ihe.gazelle.hpd.BatchRequest aClass){
		return ((((((((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAbandonRequest())).equals(new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddRequest())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getCompareRequest())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getDelRequest())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getExtendedRequest())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getModDNRequest())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getModifyRequest())).equals(new Integer(0)));
		
	}

	/**
	* Validation of class-constraint : queryRequestSpec
    * Verify if an element of type queryRequestSpec can be validated by queryRequestSpec
	*
	*/
	public static boolean _isQueryRequestSpec(net.ihe.gazelle.hpd.BatchRequest aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   queryRequestSpec
     * class ::  net.ihe.gazelle.hpd.BatchRequest
     * 
     */
    public static void _validateQueryRequestSpec(net.ihe.gazelle.hpd.BatchRequest aClass, String location, List<Notification> diagnostic) {
		if (_isQueryRequestSpec(aClass)){
			executeCons_QueryRequestSpec_Constraint_hpd_queryRequestAllowedElements(aClass, location, diagnostic);
		}
	}

	private static void executeCons_QueryRequestSpec_Constraint_hpd_queryRequestAllowedElements(net.ihe.gazelle.hpd.BatchRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateQueryRequestSpec_Constraint_hpd_queryRequestAllowedElements(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_queryRequestAllowedElements");
		notif.setDescription("A Provider Information Query Request can only contains searchRequest elements.");
		notif.setLocation(location);
		notif.setIdentifiant("providerInformationQueryRequest-queryRequestSpec-constraint_hpd_queryRequestAllowedElements");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-157"));
		diagnostic.add(notif);
	}

}
