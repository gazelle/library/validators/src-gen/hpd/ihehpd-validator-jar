package net.ihe.gazelle.hpd.validator.core.providerinformationqueryresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        queryResponseSpec
  * package :   providerInformationQueryResponse
  * Constraint Spec Class
  * class of test : BatchResponse
  * 
  */
public final class QueryResponseSpecValidator{


    private QueryResponseSpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_queryResponseAllowedElements
	* A provider information query response can only constains searchResponse elements
	*
	*/
	private static boolean _validateQueryResponseSpec_Constraint_hpd_queryResponseAllowedElements(net.ihe.gazelle.hpd.BatchResponse aClass){
		return (((((((((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddResponse())).equals(new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAuthResponse())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getCompareResponse())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getDelResponse())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getExtendedResponse())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getModDNResponse())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getModifyResponse())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getErrorResponse())).equals(new Integer(0)));
		
	}

	/**
	* Validation of class-constraint : queryResponseSpec
    * Verify if an element of type queryResponseSpec can be validated by queryResponseSpec
	*
	*/
	public static boolean _isQueryResponseSpec(net.ihe.gazelle.hpd.BatchResponse aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   queryResponseSpec
     * class ::  net.ihe.gazelle.hpd.BatchResponse
     * 
     */
    public static void _validateQueryResponseSpec(net.ihe.gazelle.hpd.BatchResponse aClass, String location, List<Notification> diagnostic) {
		if (_isQueryResponseSpec(aClass)){
			executeCons_QueryResponseSpec_Constraint_hpd_queryResponseAllowedElements(aClass, location, diagnostic);
		}
	}

	private static void executeCons_QueryResponseSpec_Constraint_hpd_queryResponseAllowedElements(net.ihe.gazelle.hpd.BatchResponse aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateQueryResponseSpec_Constraint_hpd_queryResponseAllowedElements(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_queryResponseAllowedElements");
		notif.setDescription("A provider information query response can only constains searchResponse elements");
		notif.setLocation(location);
		notif.setIdentifiant("providerInformationQueryResponse-queryResponseSpec-constraint_hpd_queryResponseAllowedElements");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-158"));
		diagnostic.add(notif);
	}

}
