package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHpdProviderStatusAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHpdProviderStatusAttrTemplateValidator{


    private ModifHpdProviderStatusAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdProviderStatusValueCard
	* hpdProviderStatus attribute SHALL be single valued (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateModifHpdProviderStatusAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderStatusValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHpdProviderStatusAttrTemplate
    * Verify if an element can be token as a Template of type modifHpdProviderStatusAttrTemplate
	*
	*/
	private static boolean _isModifHpdProviderStatusAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hpdproviderstatus");
				
	}
	/**
	* Validation of class-constraint : modifHpdProviderStatusAttrTemplate
    * Verify if an element of type modifHpdProviderStatusAttrTemplate can be validated by modifHpdProviderStatusAttrTemplate
	*
	*/
	public static boolean _isModifHpdProviderStatusAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHpdProviderStatusAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHpdProviderStatusAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHpdProviderStatusAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHpdProviderStatusAttrTemplate(aClass)){
			executeCons_ModifHpdProviderStatusAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderStatusValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHpdProviderStatusAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderStatusValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdProviderStatusAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderStatusValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdProviderStatusValueCard");
		notif.setDescription("hpdProviderStatus attribute SHALL be single valued (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdProviderStatusAttrTemplate-constraint_hpd_modifyRequest_hpdProviderStatusValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-099"));
		diagnostic.add(notif);
	}

}
