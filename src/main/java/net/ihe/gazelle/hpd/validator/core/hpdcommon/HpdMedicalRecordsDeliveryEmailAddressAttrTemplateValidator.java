package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        hpdMedicalRecordsDeliveryEmailAddressAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HpdMedicalRecordsDeliveryEmailAddressAttrTemplateValidator{


    private HpdMedicalRecordsDeliveryEmailAddressAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_hpdMedicalRecordsDeliveryEmailAddressValueCard
	* hpdMedicalRecordsDeliveryEmailAddress attribute SHALL be single valued (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateHpdMedicalRecordsDeliveryEmailAddressAttrTemplate_Constraint_hpd_common_hpdMedicalRecordsDeliveryEmailAddressValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : hpdMedicalRecordsDeliveryEmailAddressAttrTemplate
    * Verify if an element can be token as a Template of type hpdMedicalRecordsDeliveryEmailAddressAttrTemplate
	*
	*/
	private static boolean _isHpdMedicalRecordsDeliveryEmailAddressAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hpdmedicalrecordsdeliveryemailaddress");
				
	}
	/**
	* Validation of class-constraint : hpdMedicalRecordsDeliveryEmailAddressAttrTemplate
    * Verify if an element of type hpdMedicalRecordsDeliveryEmailAddressAttrTemplate can be validated by hpdMedicalRecordsDeliveryEmailAddressAttrTemplate
	*
	*/
	public static boolean _isHpdMedicalRecordsDeliveryEmailAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHpdMedicalRecordsDeliveryEmailAddressAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   hpdMedicalRecordsDeliveryEmailAddressAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHpdMedicalRecordsDeliveryEmailAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHpdMedicalRecordsDeliveryEmailAddressAttrTemplate(aClass)){
			executeCons_HpdMedicalRecordsDeliveryEmailAddressAttrTemplate_Constraint_hpd_common_hpdMedicalRecordsDeliveryEmailAddressValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HpdMedicalRecordsDeliveryEmailAddressAttrTemplate_Constraint_hpd_common_hpdMedicalRecordsDeliveryEmailAddressValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHpdMedicalRecordsDeliveryEmailAddressAttrTemplate_Constraint_hpd_common_hpdMedicalRecordsDeliveryEmailAddressValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_hpdMedicalRecordsDeliveryEmailAddressValueCard");
		notif.setDescription("hpdMedicalRecordsDeliveryEmailAddress attribute SHALL be single valued (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-hpdMedicalRecordsDeliveryEmailAddressAttrTemplate-constraint_hpd_common_hpdMedicalRecordsDeliveryEmailAddressValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-008"));
		diagnostic.add(notif);
	}

}
