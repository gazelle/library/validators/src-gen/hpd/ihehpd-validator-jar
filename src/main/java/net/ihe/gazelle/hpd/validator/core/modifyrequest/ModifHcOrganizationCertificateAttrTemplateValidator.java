package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHcOrganizationCertificateAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHcOrganizationCertificateAttrTemplateValidator{


    private ModifHcOrganizationCertificateAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_HcOrganizationCertificateSyntax
	* LDAP Syntax to be used for HcSigningCertificate attribute SHALL be Binary (see Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateModifHcOrganizationCertificateAttrTemplate_Constraint_hpd_modifyRequest_HcOrganizationCertificateSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHcOrganizationCertificateAttrTemplate
    * Verify if an element can be token as a Template of type modifHcOrganizationCertificateAttrTemplate
	*
	*/
	private static boolean _isModifHcOrganizationCertificateAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hcorganizationcertificates");
				
	}
	/**
	* Validation of class-constraint : modifHcOrganizationCertificateAttrTemplate
    * Verify if an element of type modifHcOrganizationCertificateAttrTemplate can be validated by modifHcOrganizationCertificateAttrTemplate
	*
	*/
	public static boolean _isModifHcOrganizationCertificateAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHcOrganizationCertificateAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHcOrganizationCertificateAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHcOrganizationCertificateAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHcOrganizationCertificateAttrTemplate(aClass)){
			executeCons_ModifHcOrganizationCertificateAttrTemplate_Constraint_hpd_modifyRequest_HcOrganizationCertificateSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHcOrganizationCertificateAttrTemplate_Constraint_hpd_modifyRequest_HcOrganizationCertificateSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHcOrganizationCertificateAttrTemplate_Constraint_hpd_modifyRequest_HcOrganizationCertificateSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_HcOrganizationCertificateSyntax");
		notif.setDescription("LDAP Syntax to be used for HcSigningCertificate attribute SHALL be Binary (see Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHcOrganizationCertificateAttrTemplate-constraint_hpd_modifyRequest_HcOrganizationCertificateSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-122"));
		diagnostic.add(notif);
	}

}
