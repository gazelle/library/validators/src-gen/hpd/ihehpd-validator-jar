package net.ihe.gazelle.hpd.validator.core.deleterequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        DelRequestSpec
  * package :   deleteRequest
  * Constraint Spec Class
  * class of test : DelRequest
  * 
  */
public final class DelRequestSpecValidator{


    private DelRequestSpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_deleteRequest_dnSyntax
	* dn attribute SHALL be formatted as a Distinguish name string.
	*
	*/
	private static boolean _validateDelRequestSpec_Constraint_hpd_deleteRequest_dnSyntax(net.ihe.gazelle.hpd.DelRequest aClass){
		return aClass.matches(((String) aClass.getDn()), "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$");
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_deleteRequest_dnValidOu
	* Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization,ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)               
	*
	*/
	private static boolean _validateDelRequestSpec_Constraint_hpd_deleteRequest_dnValidOu(net.ihe.gazelle.hpd.DelRequest aClass){
		return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=(hcprofessional|hcregulatedorganization|relationship|hpdcredential)){1}.*");
		
	}

	/**
	* Validation of class-constraint : DelRequestSpec
    * Verify if an element of type DelRequestSpec can be validated by DelRequestSpec
	*
	*/
	public static boolean _isDelRequestSpec(net.ihe.gazelle.hpd.DelRequest aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   DelRequestSpec
     * class ::  net.ihe.gazelle.hpd.DelRequest
     * 
     */
    public static void _validateDelRequestSpec(net.ihe.gazelle.hpd.DelRequest aClass, String location, List<Notification> diagnostic) {
		if (_isDelRequestSpec(aClass)){
			executeCons_DelRequestSpec_Constraint_hpd_deleteRequest_dnSyntax(aClass, location, diagnostic);
			executeCons_DelRequestSpec_Constraint_hpd_deleteRequest_dnValidOu(aClass, location, diagnostic);
		}
	}

	private static void executeCons_DelRequestSpec_Constraint_hpd_deleteRequest_dnSyntax(net.ihe.gazelle.hpd.DelRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateDelRequestSpec_Constraint_hpd_deleteRequest_dnSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_deleteRequest_dnSyntax");
		notif.setDescription("dn attribute SHALL be formatted as a Distinguish name string.");
		notif.setLocation(location);
		notif.setIdentifiant("deleteRequest-DelRequestSpec-constraint_hpd_deleteRequest_dnSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-095"));
		diagnostic.add(notif);
	}

	private static void executeCons_DelRequestSpec_Constraint_hpd_deleteRequest_dnValidOu(net.ihe.gazelle.hpd.DelRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateDelRequestSpec_Constraint_hpd_deleteRequest_dnValidOu(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_deleteRequest_dnValidOu");
		notif.setDescription("Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization,ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)");
		notif.setLocation(location);
		notif.setIdentifiant("deleteRequest-DelRequestSpec-constraint_hpd_deleteRequest_dnValidOu");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-096"));
		diagnostic.add(notif);
	}

}
