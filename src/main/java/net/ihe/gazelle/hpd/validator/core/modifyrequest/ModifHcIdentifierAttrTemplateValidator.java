package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHcIdentifierAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHcIdentifierAttrTemplateValidator{


    private ModifHcIdentifierAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_HcIdentifierValue
	* HcIdentifier SHALL be formatted as defined by ISO 21091 (IssuingAuthority:Type:ID:Status) where valid values for Status are Active, Inactive, Revoked and Suspended  (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping, 3.58.4.1.2.2.3-1: Organizational Provider Mapping and 3.58.4.1.2.3-1: Status Code Category Values)
	*
	*/
	private static boolean _validateModifHcIdentifierAttrTemplate_Constraint_hpd_modifyRequest_HcIdentifierValue(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1.toLowerCase(), ".+(:.+){2}:(active|inactive|revoked|suspended)")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHcIdentifierAttrTemplate
    * Verify if an element can be token as a Template of type modifHcIdentifierAttrTemplate
	*
	*/
	private static boolean _isModifHcIdentifierAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hcidentifier");
				
	}
	/**
	* Validation of class-constraint : modifHcIdentifierAttrTemplate
    * Verify if an element of type modifHcIdentifierAttrTemplate can be validated by modifHcIdentifierAttrTemplate
	*
	*/
	public static boolean _isModifHcIdentifierAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHcIdentifierAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHcIdentifierAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHcIdentifierAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHcIdentifierAttrTemplate(aClass)){
			executeCons_ModifHcIdentifierAttrTemplate_Constraint_hpd_modifyRequest_HcIdentifierValue(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHcIdentifierAttrTemplate_Constraint_hpd_modifyRequest_HcIdentifierValue(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHcIdentifierAttrTemplate_Constraint_hpd_modifyRequest_HcIdentifierValue(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_HcIdentifierValue");
		notif.setDescription("HcIdentifier SHALL be formatted as defined by ISO 21091 (IssuingAuthority:Type:ID:Status) where valid values for Status are Active, Inactive, Revoked and Suspended  (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping, 3.58.4.1.2.2.3-1: Organizational Provider Mapping and 3.58.4.1.2.3-1: Status Code Category Values)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHcIdentifierAttrTemplate-constraint_hpd_modifyRequest_HcIdentifierValue");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-118"));
		diagnostic.add(notif);
	}

}
