package net.ihe.gazelle.hpd.validator.core.modifydnrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        ModifyDNRequestSpec
  * package :   modifyDNRequest
  * Constraint Spec Class
  * class of test : ModifyDNRequest
  * 
  */
public final class ModifyDNRequestSpecValidator{


    private ModifyDNRequestSpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyDNRequest_dnSyntax
	* dn attribute SHALL be formatted as a Distinguish name string.
	*
	*/
	private static boolean _validateModifyDNRequestSpec_Constraint_hpd_modifyDNRequest_dnSyntax(net.ihe.gazelle.hpd.ModifyDNRequest aClass){
		return aClass.matches(((String) aClass.getDn()), "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$");
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyDNRequest_dnValidOu
	* Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization,ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)               
	*
	*/
	private static boolean _validateModifyDNRequestSpec_Constraint_hpd_modifyDNRequest_dnValidOu(net.ihe.gazelle.hpd.ModifyDNRequest aClass){
		return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=(hcprofessional|hcregulatedorganization|relationship|hpdcredential)){1}.*");
		
	}

	/**
	* Validation of class-constraint : ModifyDNRequestSpec
    * Verify if an element of type ModifyDNRequestSpec can be validated by ModifyDNRequestSpec
	*
	*/
	public static boolean _isModifyDNRequestSpec(net.ihe.gazelle.hpd.ModifyDNRequest aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ModifyDNRequestSpec
     * class ::  net.ihe.gazelle.hpd.ModifyDNRequest
     * 
     */
    public static void _validateModifyDNRequestSpec(net.ihe.gazelle.hpd.ModifyDNRequest aClass, String location, List<Notification> diagnostic) {
		if (_isModifyDNRequestSpec(aClass)){
			executeCons_ModifyDNRequestSpec_Constraint_hpd_modifyDNRequest_dnSyntax(aClass, location, diagnostic);
			executeCons_ModifyDNRequestSpec_Constraint_hpd_modifyDNRequest_dnValidOu(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifyDNRequestSpec_Constraint_hpd_modifyDNRequest_dnSyntax(net.ihe.gazelle.hpd.ModifyDNRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifyDNRequestSpec_Constraint_hpd_modifyDNRequest_dnSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyDNRequest_dnSyntax");
		notif.setDescription("dn attribute SHALL be formatted as a Distinguish name string.");
		notif.setLocation(location);
		notif.setIdentifiant("modifyDNRequest-ModifyDNRequestSpec-constraint_hpd_modifyDNRequest_dnSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-097"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifyDNRequestSpec_Constraint_hpd_modifyDNRequest_dnValidOu(net.ihe.gazelle.hpd.ModifyDNRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifyDNRequestSpec_Constraint_hpd_modifyDNRequest_dnValidOu(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyDNRequest_dnValidOu");
		notif.setDescription("Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization,ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyDNRequest-ModifyDNRequestSpec-constraint_hpd_modifyDNRequest_dnValidOu");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-098"));
		diagnostic.add(notif);
	}

}
