package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifNameDatatypeTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifNameDatatypeTemplateValidator{


    private ModifNameDatatypeTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hl7XPNdatatype
	* LDAP ‘name’ attributes marked with a language tag of “lang-x-ihe” shall be encoded using the HL7 XPN Data Type. UTF-8 shall be used for any characters outside ASCII. (see 3.24.5.2.3.1 Use of language tag and HL7 Name Data Type (XPN))
	*
	*/
	private static boolean _validateModifNameDatatypeTemplate_Constraint_hpd_modifyRequest_hl7XPNdatatype(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    Boolean ifExpResult1;
			    
			    if (aClass.matches(anElement1.toLowerCase(), "^(lang-x-ihe){1}.*")) {
			        ifExpResult1 = aClass.matches(anElement1.toLowerCase(), "^(lang-x-ihe:){1}[^\\^]*(\\^[^\\^]*){1,13}");
			    } else {
			        ifExpResult1 = aClass.matches(anElement1.toLowerCase(), "*.");
			    }
		
			    if (!ifExpResult1) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : modifNameDatatypeTemplate
    * Verify if an element can be token as a Template of type modifNameDatatypeTemplate
	*
	*/
	private static boolean _isModifNameDatatypeTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.matches(aClass.getName(), ".*(;lang-x-ihe){1}");
				
	}
	/**
	* Validation of class-constraint : modifNameDatatypeTemplate
    * Verify if an element of type modifNameDatatypeTemplate can be validated by modifNameDatatypeTemplate
	*
	*/
	public static boolean _isModifNameDatatypeTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifNameDatatypeTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifNameDatatypeTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifNameDatatypeTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifNameDatatypeTemplate(aClass)){
			executeCons_ModifNameDatatypeTemplate_Constraint_hpd_modifyRequest_hl7XPNdatatype(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifNameDatatypeTemplate_Constraint_hpd_modifyRequest_hl7XPNdatatype(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifNameDatatypeTemplate_Constraint_hpd_modifyRequest_hl7XPNdatatype(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hl7XPNdatatype");
		notif.setDescription("LDAP ‘name’ attributes marked with a language tag of “lang-x-ihe” shall be encoded using the HL7 XPN Data Type. UTF-8 shall be used for any characters outside ASCII. (see 3.24.5.2.3.1 Use of language tag and HL7 Name Data Type (XPN))");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifNameDatatypeTemplate-constraint_hpd_modifyRequest_hl7XPNdatatype");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-150"));
		diagnostic.add(notif);
	}

}
