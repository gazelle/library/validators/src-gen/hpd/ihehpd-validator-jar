package net.ihe.gazelle.hpd.validator.core.searchresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;


 /**
  * class :        SearchResultEntrySpec
  * package :   searchResponse
  * Constraint Spec Class
  * class of test : SearchResultEntry
  * 
  */
public final class SearchResultEntrySpecValidator{


    private SearchResultEntrySpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_searchReponse_searchResultEntryDNSyntax
	* dn attribute SHALL be formatted as a Distinguish name string.
	*
	*/
	private static boolean _validateSearchResultEntrySpec_Constraint_hpd_searchReponse_searchResultEntryDNSyntax(net.ihe.gazelle.hpd.SearchResultEntry aClass){
		return aClass.matches(((String) aClass.getDn()), "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$");
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_searchResponse_SearchResultEntryDnValidOu
	* Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization, ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)               
	*
	*/
	private static boolean _validateSearchResultEntrySpec_Constraint_hpd_searchResponse_SearchResultEntryDnValidOu(net.ihe.gazelle.hpd.SearchResultEntry aClass){
		return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=(hcprofessional|hcregulatedorganization|relationship|hpdcredential)){1}.*");
		
	}

	/**
	* Validation of class-constraint : SearchResultEntrySpec
    * Verify if an element of type SearchResultEntrySpec can be validated by SearchResultEntrySpec
	*
	*/
	public static boolean _isSearchResultEntrySpec(net.ihe.gazelle.hpd.SearchResultEntry aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   SearchResultEntrySpec
     * class ::  net.ihe.gazelle.hpd.SearchResultEntry
     * 
     */
    public static void _validateSearchResultEntrySpec(net.ihe.gazelle.hpd.SearchResultEntry aClass, String location, List<Notification> diagnostic) {
		if (_isSearchResultEntrySpec(aClass)){
			executeCons_SearchResultEntrySpec_Constraint_hpd_searchReponse_searchResultEntryDNSyntax(aClass, location, diagnostic);
			executeCons_SearchResultEntrySpec_Constraint_hpd_searchResponse_SearchResultEntryDnValidOu(aClass, location, diagnostic);
		}
	}

	private static void executeCons_SearchResultEntrySpec_Constraint_hpd_searchReponse_searchResultEntryDNSyntax(net.ihe.gazelle.hpd.SearchResultEntry aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSearchResultEntrySpec_Constraint_hpd_searchReponse_searchResultEntryDNSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_searchReponse_searchResultEntryDNSyntax");
		notif.setDescription("dn attribute SHALL be formatted as a Distinguish name string.");
		notif.setLocation(location);
		notif.setIdentifiant("searchResponse-SearchResultEntrySpec-constraint_hpd_searchReponse_searchResultEntryDNSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-059"));
		diagnostic.add(notif);
	}

	private static void executeCons_SearchResultEntrySpec_Constraint_hpd_searchResponse_SearchResultEntryDnValidOu(net.ihe.gazelle.hpd.SearchResultEntry aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSearchResultEntrySpec_Constraint_hpd_searchResponse_SearchResultEntryDnValidOu(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_searchResponse_SearchResultEntryDnValidOu");
		notif.setDescription("Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization, ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)");
		notif.setLocation(location);
		notif.setIdentifiant("searchResponse-SearchResultEntrySpec-constraint_hpd_searchResponse_SearchResultEntryDnValidOu");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-060"));
		diagnostic.add(notif);
	}

}
