package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        hpdProviderLanguageSupportedAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HpdProviderLanguageSupportedAttrTemplateValidator{


    private HpdProviderLanguageSupportedAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_hpdProviderLanguageSupportedException
	* Values for the hpdProviderLanguageSupported attribute type MUST conform to the Accept-Language header field defined in [RFC2068] with one Exception: the sequence 'Accept-Language' ':' should  be omitted (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_common_hpdProviderLanguageSupportedException(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!!aClass.matches(anElement1.toLowerCase(), "accepted-language.*")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_common_hpdProviderLanguageSupportedSyntax
	* Values for the hpdProviderLanguageSupported attribute type MUST conform to the Accept-Language header field defined in [RFC2068] (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_common_hpdProviderLanguageSupportedSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "^[a-zA-Z\\-:]+(;q=(1|0\\.[0-9]+))?(,[a-zA-Z\\-\\s]*(;q=(1|0\\.[0-9]+))?)*")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : hpdProviderLanguageSupportedAttrTemplate
    * Verify if an element can be token as a Template of type hpdProviderLanguageSupportedAttrTemplate
	*
	*/
	private static boolean _isHpdProviderLanguageSupportedAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hpdproviderlanguagesupported");
				
	}
	/**
	* Validation of class-constraint : hpdProviderLanguageSupportedAttrTemplate
    * Verify if an element of type hpdProviderLanguageSupportedAttrTemplate can be validated by hpdProviderLanguageSupportedAttrTemplate
	*
	*/
	public static boolean _isHpdProviderLanguageSupportedAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHpdProviderLanguageSupportedAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   hpdProviderLanguageSupportedAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHpdProviderLanguageSupportedAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHpdProviderLanguageSupportedAttrTemplate(aClass)){
			executeCons_HpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_common_hpdProviderLanguageSupportedException(aClass, location, diagnostic);
			executeCons_HpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_common_hpdProviderLanguageSupportedSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_common_hpdProviderLanguageSupportedException(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_common_hpdProviderLanguageSupportedException(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Warning();
		}
		notif.setTest("constraint_hpd_common_hpdProviderLanguageSupportedException");
		notif.setDescription("Values for the hpdProviderLanguageSupported attribute type MUST conform to the Accept-Language header field defined in [RFC2068] with one Exception: the sequence 'Accept-Language' ':' should  be omitted (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-hpdProviderLanguageSupportedAttrTemplate-constraint_hpd_common_hpdProviderLanguageSupportedException");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-036"));
		diagnostic.add(notif);
	}

	private static void executeCons_HpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_common_hpdProviderLanguageSupportedSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_common_hpdProviderLanguageSupportedSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_hpdProviderLanguageSupportedSyntax");
		notif.setDescription("Values for the hpdProviderLanguageSupported attribute type MUST conform to the Accept-Language header field defined in [RFC2068] (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-hpdProviderLanguageSupportedAttrTemplate-constraint_hpd_common_hpdProviderLanguageSupportedSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-035"));
		diagnostic.add(notif);
	}

}
