package net.ihe.gazelle.hpd.validator.core.searchresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;


 /**
  * class :        credentialTypeAttrForHCRegulatedOrganizationTemplate
  * package :   searchResponse
  * Template Class
  * Template identifier : 
  * Class of test : SearchResultEntry
  * 
  */
public final class CredentialTypeAttrForHCRegulatedOrganizationTemplateValidator{


    private CredentialTypeAttrForHCRegulatedOrganizationTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_searchResultEntryHCRegulatedOrganization_credentialTypeValue
	* Degree is not a valid type for Organizational Provider's credential. Valid values are certificate and credential. (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)
	*
	*/
	private static boolean _validateCredentialTypeAttrForHCRegulatedOrganizationTemplate_Constraint_hpd_searchResultEntryHCRegulatedOrganization_credentialTypeValue(net.ihe.gazelle.hpd.SearchResultEntry aClass){
		java.util.ArrayList<DsmlAttr> result3;
		result3 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("credentialtype")) {
			        result3.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		java.util.ArrayList<String> result2;
		result2 = new java.util.ArrayList<String>();
		
		/* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
		for (DsmlAttr anElement2 : result3) {
		    result2.addAll(anElement2.getValue());
		}
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement3 : result2) {
			    if (!(anElement3.toLowerCase().equals("certificate") || anElement3.toLowerCase().equals("credential"))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : credentialTypeAttrForHCRegulatedOrganizationTemplate
    * Verify if an element can be token as a Template of type credentialTypeAttrForHCRegulatedOrganizationTemplate
	*
	*/
	private static boolean _isCredentialTypeAttrForHCRegulatedOrganizationTemplateTemplate(net.ihe.gazelle.hpd.SearchResultEntry aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("credentialtype")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=hcregulatedOrganization){1}.*") && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > new Integer(0)));
		
				
	}
	/**
	* Validation of class-constraint : credentialTypeAttrForHCRegulatedOrganizationTemplate
    * Verify if an element of type credentialTypeAttrForHCRegulatedOrganizationTemplate can be validated by credentialTypeAttrForHCRegulatedOrganizationTemplate
	*
	*/
	public static boolean _isCredentialTypeAttrForHCRegulatedOrganizationTemplate(net.ihe.gazelle.hpd.SearchResultEntry aClass){
		return _isCredentialTypeAttrForHCRegulatedOrganizationTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   credentialTypeAttrForHCRegulatedOrganizationTemplate
     * class ::  net.ihe.gazelle.hpd.SearchResultEntry
     * 
     */
    public static void _validateCredentialTypeAttrForHCRegulatedOrganizationTemplate(net.ihe.gazelle.hpd.SearchResultEntry aClass, String location, List<Notification> diagnostic) {
		if (_isCredentialTypeAttrForHCRegulatedOrganizationTemplate(aClass)){
			executeCons_CredentialTypeAttrForHCRegulatedOrganizationTemplate_Constraint_hpd_searchResultEntryHCRegulatedOrganization_credentialTypeValue(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CredentialTypeAttrForHCRegulatedOrganizationTemplate_Constraint_hpd_searchResultEntryHCRegulatedOrganization_credentialTypeValue(net.ihe.gazelle.hpd.SearchResultEntry aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCredentialTypeAttrForHCRegulatedOrganizationTemplate_Constraint_hpd_searchResultEntryHCRegulatedOrganization_credentialTypeValue(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_searchResultEntryHCRegulatedOrganization_credentialTypeValue");
		notif.setDescription("Degree is not a valid type for Organizational Provider's credential. Valid values are certificate and credential. (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("searchResponse-credentialTypeAttrForHCRegulatedOrganizationTemplate-constraint_hpd_searchResultEntryHCRegulatedOrganization_credentialTypeValue");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-064"));
		diagnostic.add(notif);
	}

}
