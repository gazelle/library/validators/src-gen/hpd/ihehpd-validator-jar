package net.ihe.gazelle.hpd.validator.core.addrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;


 /**
  * class :        addRequestForHCRegulatedOrganizationTemplate
  * package :   addRequest
  * Template Class
  * Template identifier : 
  * Class of test : AddRequest
  * 
  */
public final class AddRequestForHCRegulatedOrganizationTemplateValidator{


    private AddRequestForHCRegulatedOrganizationTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHCRegulaedOrganization_HcRegisteredNameRequired
	* When the addRequest describes an Organizational Provider, the HcRegisteredName attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateAddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulaedOrganization_HcRegisteredNameRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hcregisteredname")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHCRegulatedOrganization_HcIdentifier
	* When the addRequest describes an Organizational Provider, the HcIdentifier attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateAddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_HcIdentifier(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hcidentifier")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHCRegulatedOrganization_oRequired
	* HCRegulatedOrganization SHALL have an o attribute (RFC 2556 section 7.5 describing Organization object class)
	*
	*/
	private static boolean _validateAddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_oRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("o")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHCRegulatedOrganization_uidRequired
	* When the addRequest describes an Organizational Provider, the uid attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateAddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_uidRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("uid")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of template-constraint by a constraint : addRequestForHCRegulatedOrganizationTemplate
    * Verify if an element can be token as a Template of type addRequestForHCRegulatedOrganizationTemplate
	*
	*/
	private static boolean _isAddRequestForHCRegulatedOrganizationTemplateTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=hcregulatedorganization){1}.*");
				
	}
	/**
	* Validation of class-constraint : addRequestForHCRegulatedOrganizationTemplate
    * Verify if an element of type addRequestForHCRegulatedOrganizationTemplate can be validated by addRequestForHCRegulatedOrganizationTemplate
	*
	*/
	public static boolean _isAddRequestForHCRegulatedOrganizationTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		return _isAddRequestForHCRegulatedOrganizationTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   addRequestForHCRegulatedOrganizationTemplate
     * class ::  net.ihe.gazelle.hpd.AddRequest
     * 
     */
    public static void _validateAddRequestForHCRegulatedOrganizationTemplate(net.ihe.gazelle.hpd.AddRequest aClass, String location, List<Notification> diagnostic) {
		if (_isAddRequestForHCRegulatedOrganizationTemplate(aClass)){
			executeCons_AddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulaedOrganization_HcRegisteredNameRequired(aClass, location, diagnostic);
			executeCons_AddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_HcIdentifier(aClass, location, diagnostic);
			executeCons_AddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_oRequired(aClass, location, diagnostic);
			executeCons_AddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_uidRequired(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulaedOrganization_HcRegisteredNameRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulaedOrganization_HcRegisteredNameRequired(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHCRegulaedOrganization_HcRegisteredNameRequired");
		notif.setDescription("When the addRequest describes an Organizational Provider, the HcRegisteredName attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHCRegulatedOrganizationTemplate-constraint_hpd_addRequestHCRegulaedOrganization_HcRegisteredNameRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-075"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_HcIdentifier(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_HcIdentifier(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHCRegulatedOrganization_HcIdentifier");
		notif.setDescription("When the addRequest describes an Organizational Provider, the HcIdentifier attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHCRegulatedOrganizationTemplate-constraint_hpd_addRequestHCRegulatedOrganization_HcIdentifier");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-074"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_oRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_oRequired(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHCRegulatedOrganization_oRequired");
		notif.setDescription("HCRegulatedOrganization SHALL have an o attribute (RFC 2556 section 7.5 describing Organization object class)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHCRegulatedOrganizationTemplate-constraint_hpd_addRequestHCRegulatedOrganization_oRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-076"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_uidRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHCRegulatedOrganizationTemplate_Constraint_hpd_addRequestHCRegulatedOrganization_uidRequired(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHCRegulatedOrganization_uidRequired");
		notif.setDescription("When the addRequest describes an Organizational Provider, the uid attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHCRegulatedOrganizationTemplate-constraint_hpd_addRequestHCRegulatedOrganization_uidRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-073"));
		diagnostic.add(notif);
	}

}
