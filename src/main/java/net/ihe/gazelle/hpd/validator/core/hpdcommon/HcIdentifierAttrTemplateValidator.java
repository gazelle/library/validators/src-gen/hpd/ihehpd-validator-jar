package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        HcIdentifierAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HcIdentifierAttrTemplateValidator{


    private HcIdentifierAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_HcIdentifierValue
	* HcIdentifier SHALL be formatted as defined by ISO 21091 (IssuingAuthority:Type:ID:Status)  (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping, 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateHcIdentifierAttrTemplate_Constraint_hpd_common_HcIdentifierValue(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, ".+(:.+){3}")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_common_HcIdentifierValue2
	* HcIdentifier Status SHALL be from  Status Code Category Values ( see Table 3.58.4.1.2.3-1)
	*
	*/
	private static boolean _validateHcIdentifierAttrTemplate_Constraint_hpd_common_HcIdentifierValue2(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.14.1", aClass.getPartFromValue(anElement1.toLowerCase(), new Integer(4)))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : HcIdentifierAttrTemplate
    * Verify if an element can be token as a Template of type HcIdentifierAttrTemplate
	*
	*/
	private static boolean _isHcIdentifierAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hcidentifier");
				
	}
	/**
	* Validation of class-constraint : HcIdentifierAttrTemplate
    * Verify if an element of type HcIdentifierAttrTemplate can be validated by HcIdentifierAttrTemplate
	*
	*/
	public static boolean _isHcIdentifierAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHcIdentifierAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   HcIdentifierAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHcIdentifierAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHcIdentifierAttrTemplate(aClass)){
			executeCons_HcIdentifierAttrTemplate_Constraint_hpd_common_HcIdentifierValue(aClass, location, diagnostic);
			executeCons_HcIdentifierAttrTemplate_Constraint_hpd_common_HcIdentifierValue2(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HcIdentifierAttrTemplate_Constraint_hpd_common_HcIdentifierValue(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHcIdentifierAttrTemplate_Constraint_hpd_common_HcIdentifierValue(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_HcIdentifierValue");
		notif.setDescription("HcIdentifier SHALL be formatted as defined by ISO 21091 (IssuingAuthority:Type:ID:Status)  (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping, 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-HcIdentifierAttrTemplate-constraint_hpd_common_HcIdentifierValue");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-022"));
		diagnostic.add(notif);
	}

	private static void executeCons_HcIdentifierAttrTemplate_Constraint_hpd_common_HcIdentifierValue2(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHcIdentifierAttrTemplate_Constraint_hpd_common_HcIdentifierValue2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_HcIdentifierValue2");
		notif.setDescription("HcIdentifier Status SHALL be from  Status Code Category Values ( see Table 3.58.4.1.2.3-1)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-HcIdentifierAttrTemplate-constraint_hpd_common_HcIdentifierValue2");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-023"));
		diagnostic.add(notif);
	}

}
