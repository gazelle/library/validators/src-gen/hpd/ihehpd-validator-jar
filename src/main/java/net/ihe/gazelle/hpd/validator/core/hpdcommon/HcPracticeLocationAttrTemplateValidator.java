package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        HcPracticeLocationAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HcPracticeLocationAttrTemplateValidator{


    private HcPracticeLocationAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_HcPracticeLocationSyntax
	* HcPracticeLocation attribute SHALL be formatted as a distinguish name (DN) (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateHcPracticeLocationAttrTemplate_Constraint_hpd_common_HcPracticeLocationSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : HcPracticeLocationAttrTemplate
    * Verify if an element can be token as a Template of type HcPracticeLocationAttrTemplate
	*
	*/
	private static boolean _isHcPracticeLocationAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hcpracticelocation");
				
	}
	/**
	* Validation of class-constraint : HcPracticeLocationAttrTemplate
    * Verify if an element of type HcPracticeLocationAttrTemplate can be validated by HcPracticeLocationAttrTemplate
	*
	*/
	public static boolean _isHcPracticeLocationAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHcPracticeLocationAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   HcPracticeLocationAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHcPracticeLocationAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHcPracticeLocationAttrTemplate(aClass)){
			executeCons_HcPracticeLocationAttrTemplate_Constraint_hpd_common_HcPracticeLocationSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HcPracticeLocationAttrTemplate_Constraint_hpd_common_HcPracticeLocationSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHcPracticeLocationAttrTemplate_Constraint_hpd_common_HcPracticeLocationSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_HcPracticeLocationSyntax");
		notif.setDescription("HcPracticeLocation attribute SHALL be formatted as a distinguish name (DN) (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-HcPracticeLocationAttrTemplate-constraint_hpd_common_HcPracticeLocationSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-032"));
		diagnostic.add(notif);
	}

}
