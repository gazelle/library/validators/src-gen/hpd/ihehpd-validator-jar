package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        objectClassAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class ObjectClassAttrTemplateValidator{


    private ObjectClassAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_objectClassValue
	* objectClass attribute SHALL be present in every entry, one of the values is either  top  or  alias  (see RFC 2256 section 5.1 objectClass)
	*
	*/
	private static boolean _validateObjectClassAttrTemplate_Constraint_hpd_common_objectClassValue(net.ihe.gazelle.hpd.DsmlAttr aClass){
		java.util.ArrayList<String> result1;
		result1 = new java.util.ArrayList<String>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if ((anElement1.toLowerCase().equals("top") || anElement1.toLowerCase().equals("alias"))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_common_objectClassValueCard
	*  The objectClass attribute is present in every entry, with at least two values.  (see RFC 2256 section 5.1 objectClass)
	*
	*/
	private static boolean _validateObjectClassAttrTemplate_Constraint_hpd_common_objectClassValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) >= new Integer(2));
		
	}

	/**
	* Validation of template-constraint by a constraint : objectClassAttrTemplate
    * Verify if an element can be token as a Template of type objectClassAttrTemplate
	*
	*/
	private static boolean _isObjectClassAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("objectclass");
				
	}
	/**
	* Validation of class-constraint : objectClassAttrTemplate
    * Verify if an element of type objectClassAttrTemplate can be validated by objectClassAttrTemplate
	*
	*/
	public static boolean _isObjectClassAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isObjectClassAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   objectClassAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateObjectClassAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isObjectClassAttrTemplate(aClass)){
			executeCons_ObjectClassAttrTemplate_Constraint_hpd_common_objectClassValue(aClass, location, diagnostic);
			executeCons_ObjectClassAttrTemplate_Constraint_hpd_common_objectClassValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ObjectClassAttrTemplate_Constraint_hpd_common_objectClassValue(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObjectClassAttrTemplate_Constraint_hpd_common_objectClassValue(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Warning();
		}
		notif.setTest("constraint_hpd_common_objectClassValue");
		notif.setDescription("objectClass attribute SHALL be present in every entry, one of the values is either  top  or  alias  (see RFC 2256 section 5.1 objectClass)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-objectClassAttrTemplate-constraint_hpd_common_objectClassValue");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-052"));
		diagnostic.add(notif);
	}

	private static void executeCons_ObjectClassAttrTemplate_Constraint_hpd_common_objectClassValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObjectClassAttrTemplate_Constraint_hpd_common_objectClassValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_objectClassValueCard");
		notif.setDescription("The objectClass attribute is present in every entry, with at least two values.  (see RFC 2256 section 5.1 objectClass)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-objectClassAttrTemplate-constraint_hpd_common_objectClassValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-051"));
		diagnostic.add(notif);
	}

}
