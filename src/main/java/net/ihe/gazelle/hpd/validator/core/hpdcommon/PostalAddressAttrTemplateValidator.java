package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        PostalAddressAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class PostalAddressAttrTemplateValidator{


    private PostalAddressAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_PostalAddressAddrMandatory
	* If an address is included, then the addr is required (see section 3.58.4.1.2.2.1 Object Classes)
	*
	*/
	private static boolean _validatePostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressAddrMandatory(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1.toLowerCase(), ".*(addr=){1}.*", new Boolean(true))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_common_PostalAddressStatus
	* Valid values for postal address status are Primary, Secondary, Inactive (see Table 3.58.4.1.2.3-1: Status Code Category Values)
	*
	*/
	private static boolean _validatePostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressStatus(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!(aClass.matches(anElement1.toLowerCase(), ".*(status=primary|status=secondary|status=inactive){1}.*", new Boolean(true)) ^ !aClass.matches(anElement1, ".*(status=){1}.*", new Boolean(true)))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_common_PostalAddressSyntax
	* PostalAddress syntax SHALL be dstring *(  $  dstring) and enforce format of  key=value 
	*
	*/
	private static boolean _validatePostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "[^=\\$]*={1}[^=\\$]*(\\${1}[^=\\$]*={1}[^=\\$]*)*", new Boolean(true))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : PostalAddressAttrTemplate
    * Verify if an element can be token as a Template of type PostalAddressAttrTemplate
	*
	*/
	private static boolean _isPostalAddressAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (((aClass.getName().toLowerCase().equals("hpdproviderlegaladdress") || aClass.getName().toLowerCase().equals("hpdproviderbillingaddress")) || aClass.getName().toLowerCase().equals("hpdprovidermailingaddress")) || aClass.getName().toLowerCase().equals("hpdproviderpracticeaddress"));
				
	}
	/**
	* Validation of class-constraint : PostalAddressAttrTemplate
    * Verify if an element of type PostalAddressAttrTemplate can be validated by PostalAddressAttrTemplate
	*
	*/
	public static boolean _isPostalAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isPostalAddressAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   PostalAddressAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validatePostalAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isPostalAddressAttrTemplate(aClass)){
			executeCons_PostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressAddrMandatory(aClass, location, diagnostic);
			executeCons_PostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressStatus(aClass, location, diagnostic);
			executeCons_PostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_PostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressAddrMandatory(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressAddrMandatory(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_PostalAddressAddrMandatory");
		notif.setDescription("If an address is included, then the addr is required (see section 3.58.4.1.2.2.1 Object Classes)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-PostalAddressAttrTemplate-constraint_hpd_common_PostalAddressAddrMandatory");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-038"));
		diagnostic.add(notif);
	}

	private static void executeCons_PostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressStatus(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressStatus(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_PostalAddressStatus");
		notif.setDescription("Valid values for postal address status are Primary, Secondary, Inactive (see Table 3.58.4.1.2.3-1: Status Code Category Values)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-PostalAddressAttrTemplate-constraint_hpd_common_PostalAddressStatus");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-039"));
		diagnostic.add(notif);
	}

	private static void executeCons_PostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePostalAddressAttrTemplate_Constraint_hpd_common_PostalAddressSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_PostalAddressSyntax");
		notif.setDescription("PostalAddress syntax SHALL be dstring *(  $  dstring) and enforce format of  key=value");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-PostalAddressAttrTemplate-constraint_hpd_common_PostalAddressSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-040"));
		diagnostic.add(notif);
	}

}
