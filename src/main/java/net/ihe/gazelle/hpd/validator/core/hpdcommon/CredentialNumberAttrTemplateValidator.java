package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        credentialNumberAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class CredentialNumberAttrTemplateValidator{


    private CredentialNumberAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_credentialNumberSyntax
	* credentialNumber attribute follows the ISO21091 UID format: Issuing Authority OID: ID (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)
	*
	*/
	private static boolean _validateCredentialNumberAttrTemplate_Constraint_hpd_common_credentialNumberSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "^[0-2](\\.(0|[1-9][0-9]*))*[:]{1}.+")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_common_credentialNumberValueCard
	* credentialNumber attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)
	*
	*/
	private static boolean _validateCredentialNumberAttrTemplate_Constraint_hpd_common_credentialNumberValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : credentialNumberAttrTemplate
    * Verify if an element can be token as a Template of type credentialNumberAttrTemplate
	*
	*/
	private static boolean _isCredentialNumberAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("credentialnumber");
				
	}
	/**
	* Validation of class-constraint : credentialNumberAttrTemplate
    * Verify if an element of type credentialNumberAttrTemplate can be validated by credentialNumberAttrTemplate
	*
	*/
	public static boolean _isCredentialNumberAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isCredentialNumberAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   credentialNumberAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateCredentialNumberAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isCredentialNumberAttrTemplate(aClass)){
			executeCons_CredentialNumberAttrTemplate_Constraint_hpd_common_credentialNumberSyntax(aClass, location, diagnostic);
			executeCons_CredentialNumberAttrTemplate_Constraint_hpd_common_credentialNumberValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CredentialNumberAttrTemplate_Constraint_hpd_common_credentialNumberSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCredentialNumberAttrTemplate_Constraint_hpd_common_credentialNumberSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_credentialNumberSyntax");
		notif.setDescription("credentialNumber attribute follows the ISO21091 UID format: Issuing Authority OID: ID (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-credentialNumberAttrTemplate-constraint_hpd_common_credentialNumberSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-016"));
		diagnostic.add(notif);
	}

	private static void executeCons_CredentialNumberAttrTemplate_Constraint_hpd_common_credentialNumberValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCredentialNumberAttrTemplate_Constraint_hpd_common_credentialNumberValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_credentialNumberValueCard");
		notif.setDescription("credentialNumber attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-credentialNumberAttrTemplate-constraint_hpd_common_credentialNumberValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-015"));
		diagnostic.add(notif);
	}

}
