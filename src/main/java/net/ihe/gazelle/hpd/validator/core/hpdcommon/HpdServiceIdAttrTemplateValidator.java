package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        hpdServiceIdAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HpdServiceIdAttrTemplateValidator{


    private HpdServiceIdAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_hpdServiceIdValueCard
	* hpdServiceId attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)
	*
	*/
	private static boolean _validateHpdServiceIdAttrTemplate_Constraint_hpd_common_hpdServiceIdValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : hpdServiceIdAttrTemplate
    * Verify if an element can be token as a Template of type hpdServiceIdAttrTemplate
	*
	*/
	private static boolean _isHpdServiceIdAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hpdserviceid");
				
	}
	/**
	* Validation of class-constraint : hpdServiceIdAttrTemplate
    * Verify if an element of type hpdServiceIdAttrTemplate can be validated by hpdServiceIdAttrTemplate
	*
	*/
	public static boolean _isHpdServiceIdAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHpdServiceIdAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   hpdServiceIdAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHpdServiceIdAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHpdServiceIdAttrTemplate(aClass)){
			executeCons_HpdServiceIdAttrTemplate_Constraint_hpd_common_hpdServiceIdValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HpdServiceIdAttrTemplate_Constraint_hpd_common_hpdServiceIdValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHpdServiceIdAttrTemplate_Constraint_hpd_common_hpdServiceIdValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_hpdServiceIdValueCard");
		notif.setDescription("hpdServiceId attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-hpdServiceIdAttrTemplate-constraint_hpd_common_hpdServiceIdValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-048"));
		diagnostic.add(notif);
	}

}
