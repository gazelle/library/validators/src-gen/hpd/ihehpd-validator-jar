package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifTelephoneNumberAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifTelephoneNumberAttrTemplateValidator{


    private ModifTelephoneNumberAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_telephoneNumberSyntax
	* telephoneNumber attribute SHALL be representing using the E.123 notation. (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping and 3.58.4.1.2.2.3-1: Organizational Provider Mapping) 
	*
	*/
	private static boolean _validateModifTelephoneNumberAttrTemplate_Constraint_hpd_modifyRequest_telephoneNumberSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "((\\+|[0-9]*|\\([0-9]+\\))[\\s\\-\\.0-9]*/?)*")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : modifTelephoneNumberAttrTemplate
    * Verify if an element can be token as a Template of type modifTelephoneNumberAttrTemplate
	*
	*/
	private static boolean _isModifTelephoneNumberAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("telephonenumber");
				
	}
	/**
	* Validation of class-constraint : modifTelephoneNumberAttrTemplate
    * Verify if an element of type modifTelephoneNumberAttrTemplate can be validated by modifTelephoneNumberAttrTemplate
	*
	*/
	public static boolean _isModifTelephoneNumberAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifTelephoneNumberAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifTelephoneNumberAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifTelephoneNumberAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifTelephoneNumberAttrTemplate(aClass)){
			executeCons_ModifTelephoneNumberAttrTemplate_Constraint_hpd_modifyRequest_telephoneNumberSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifTelephoneNumberAttrTemplate_Constraint_hpd_modifyRequest_telephoneNumberSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifTelephoneNumberAttrTemplate_Constraint_hpd_modifyRequest_telephoneNumberSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_telephoneNumberSyntax");
		notif.setDescription("telephoneNumber attribute SHALL be representing using the E.123 notation. (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping and 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifTelephoneNumberAttrTemplate-constraint_hpd_modifyRequest_telephoneNumberSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-123"));
		diagnostic.add(notif);
	}

}
