package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        telephoneNumberAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class TelephoneNumberAttrTemplateValidator{


    private TelephoneNumberAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_telephoneNumberSyntax
	* telephoneNumber attribute SHALL be representing using the E.123 notation. (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping and 3.58.4.1.2.2.3-1: Organizational Provider Mapping) 
	*
	*/
	private static boolean _validateTelephoneNumberAttrTemplate_Constraint_hpd_common_telephoneNumberSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "((\\+|[0-9]*|\\([0-9]+\\))[\\s\\-\\.0-9]*/?)*")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : telephoneNumberAttrTemplate
    * Verify if an element can be token as a Template of type telephoneNumberAttrTemplate
	*
	*/
	private static boolean _isTelephoneNumberAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("telephonenumber");
				
	}
	/**
	* Validation of class-constraint : telephoneNumberAttrTemplate
    * Verify if an element of type telephoneNumberAttrTemplate can be validated by telephoneNumberAttrTemplate
	*
	*/
	public static boolean _isTelephoneNumberAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isTelephoneNumberAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   telephoneNumberAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateTelephoneNumberAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isTelephoneNumberAttrTemplate(aClass)){
			executeCons_TelephoneNumberAttrTemplate_Constraint_hpd_common_telephoneNumberSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_TelephoneNumberAttrTemplate_Constraint_hpd_common_telephoneNumberSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateTelephoneNumberAttrTemplate_Constraint_hpd_common_telephoneNumberSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_telephoneNumberSyntax");
		notif.setDescription("telephoneNumber attribute SHALL be representing using the E.123 notation. (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping and 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-telephoneNumberAttrTemplate-constraint_hpd_common_telephoneNumberSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-028"));
		diagnostic.add(notif);
	}

}
