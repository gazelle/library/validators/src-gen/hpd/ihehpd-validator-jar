package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHpdServiceIdAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHpdServiceIdAttrTemplateValidator{


    private ModifHpdServiceIdAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdServiceIdValueCard
	* hpdServiceId attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)
	*
	*/
	private static boolean _validateModifHpdServiceIdAttrTemplate_Constraint_hpd_modifyRequest_hpdServiceIdValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHpdServiceIdAttrTemplate
    * Verify if an element can be token as a Template of type modifHpdServiceIdAttrTemplate
	*
	*/
	private static boolean _isModifHpdServiceIdAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hpdserviceid");
				
	}
	/**
	* Validation of class-constraint : modifHpdServiceIdAttrTemplate
    * Verify if an element of type modifHpdServiceIdAttrTemplate can be validated by modifHpdServiceIdAttrTemplate
	*
	*/
	public static boolean _isModifHpdServiceIdAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHpdServiceIdAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHpdServiceIdAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHpdServiceIdAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHpdServiceIdAttrTemplate(aClass)){
			executeCons_ModifHpdServiceIdAttrTemplate_Constraint_hpd_modifyRequest_hpdServiceIdValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHpdServiceIdAttrTemplate_Constraint_hpd_modifyRequest_hpdServiceIdValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdServiceIdAttrTemplate_Constraint_hpd_modifyRequest_hpdServiceIdValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdServiceIdValueCard");
		notif.setDescription("hpdServiceId attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdServiceIdAttrTemplate-constraint_hpd_modifyRequest_hpdServiceIdValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-143"));
		diagnostic.add(notif);
	}

}
