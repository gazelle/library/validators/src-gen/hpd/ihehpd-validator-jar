package net.ihe.gazelle.hpd.validator.core.providerinformationfeedresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        feedResponseSpec
  * package :   providerInformationFeedResponse
  * Constraint Spec Class
  * class of test : BatchResponse
  * 
  */
public final class FeedResponseSpecValidator{


    private FeedResponseSpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_feedResponseAllowedElements
	* A Provider Information Feed Response can only contains the following elements: addResponse, delResponse, modDNResponse, modifyResponse
	*
	*/
	private static boolean _validateFeedResponseSpec_Constraint_hpd_feedResponseAllowedElements(net.ihe.gazelle.hpd.BatchResponse aClass){
		return (((((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAuthResponse())).equals(new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getCompareResponse())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getExtendedResponse())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getSearchResponse())).equals(new Integer(0)));
		
	}

	/**
	* Validation of class-constraint : feedResponseSpec
    * Verify if an element of type feedResponseSpec can be validated by feedResponseSpec
	*
	*/
	public static boolean _isFeedResponseSpec(net.ihe.gazelle.hpd.BatchResponse aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   feedResponseSpec
     * class ::  net.ihe.gazelle.hpd.BatchResponse
     * 
     */
    public static void _validateFeedResponseSpec(net.ihe.gazelle.hpd.BatchResponse aClass, String location, List<Notification> diagnostic) {
		if (_isFeedResponseSpec(aClass)){
			executeCons_FeedResponseSpec_Constraint_hpd_feedResponseAllowedElements(aClass, location, diagnostic);
		}
	}

	private static void executeCons_FeedResponseSpec_Constraint_hpd_feedResponseAllowedElements(net.ihe.gazelle.hpd.BatchResponse aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateFeedResponseSpec_Constraint_hpd_feedResponseAllowedElements(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_feedResponseAllowedElements");
		notif.setDescription("A Provider Information Feed Response can only contains the following elements: addResponse, delResponse, modDNResponse, modifyResponse");
		notif.setLocation(location);
		notif.setIdentifiant("providerInformationFeedResponse-feedResponseSpec-constraint_hpd_feedResponseAllowedElements");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-156"));
		diagnostic.add(notif);
	}

}
