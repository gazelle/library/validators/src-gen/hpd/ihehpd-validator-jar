package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHpdProviderLegalAddressAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHpdProviderLegalAddressAttrTemplateValidator{


    private ModifHpdProviderLegalAddressAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdProviderLegalAddressValueCard
	* hpdProviderLegalAddress attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-1 HPDProvider Optional Attributes, CP-ITI-601-04)
	*
	*/
	private static boolean _validateModifHpdProviderLegalAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLegalAddressValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHpdProviderLegalAddressAttrTemplate
    * Verify if an element can be token as a Template of type modifHpdProviderLegalAddressAttrTemplate
	*
	*/
	private static boolean _isModifHpdProviderLegalAddressAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hpdproviderlegaladdress");
				
	}
	/**
	* Validation of class-constraint : modifHpdProviderLegalAddressAttrTemplate
    * Verify if an element of type modifHpdProviderLegalAddressAttrTemplate can be validated by modifHpdProviderLegalAddressAttrTemplate
	*
	*/
	public static boolean _isModifHpdProviderLegalAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHpdProviderLegalAddressAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHpdProviderLegalAddressAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHpdProviderLegalAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHpdProviderLegalAddressAttrTemplate(aClass)){
			executeCons_ModifHpdProviderLegalAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLegalAddressValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHpdProviderLegalAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLegalAddressValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdProviderLegalAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLegalAddressValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdProviderLegalAddressValueCard");
		notif.setDescription("hpdProviderLegalAddress attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-1 HPDProvider Optional Attributes, CP-ITI-601-04)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdProviderLegalAddressAttrTemplate-constraint_hpd_modifyRequest_hpdProviderLegalAddressValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-136"));
		diagnostic.add(notif);
	}

}
