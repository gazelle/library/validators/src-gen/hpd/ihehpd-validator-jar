package net.ihe.gazelle.hpd.validator.core.providerinformationfeedrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        feedRequestSpec
  * package :   providerInformationFeedRequest
  * Constraint Spec Class
  * class of test : BatchRequest
  * 
  */
public final class FeedRequestSpecValidator{


    private FeedRequestSpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_feedRequestAllowedElements
	* A Provider Information Feed Request only supports the following elements: addRequest, delRequest, modDNRequest and modifyRequest
	*
	*/
	private static boolean _validateFeedRequestSpec_Constraint_hpd_feedRequestAllowedElements(net.ihe.gazelle.hpd.BatchRequest aClass){
		return (((((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAbandonRequest())).equals(new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getCompareRequest())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getExtendedRequest())).equals(new Integer(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getSearchRequest())).equals(new Integer(0)));
		
	}

	/**
	* Validation of class-constraint : feedRequestSpec
    * Verify if an element of type feedRequestSpec can be validated by feedRequestSpec
	*
	*/
	public static boolean _isFeedRequestSpec(net.ihe.gazelle.hpd.BatchRequest aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   feedRequestSpec
     * class ::  net.ihe.gazelle.hpd.BatchRequest
     * 
     */
    public static void _validateFeedRequestSpec(net.ihe.gazelle.hpd.BatchRequest aClass, String location, List<Notification> diagnostic) {
		if (_isFeedRequestSpec(aClass)){
			executeCons_FeedRequestSpec_Constraint_hpd_feedRequestAllowedElements(aClass, location, diagnostic);
		}
	}

	private static void executeCons_FeedRequestSpec_Constraint_hpd_feedRequestAllowedElements(net.ihe.gazelle.hpd.BatchRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateFeedRequestSpec_Constraint_hpd_feedRequestAllowedElements(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_feedRequestAllowedElements");
		notif.setDescription("A Provider Information Feed Request only supports the following elements: addRequest, delRequest, modDNRequest and modifyRequest");
		notif.setLocation(location);
		notif.setIdentifiant("providerInformationFeedRequest-feedRequestSpec-constraint_hpd_feedRequestAllowedElements");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-155"));
		diagnostic.add(notif);
	}

}
