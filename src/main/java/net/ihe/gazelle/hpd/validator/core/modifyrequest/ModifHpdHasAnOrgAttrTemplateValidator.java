package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHpdHasAnOrgAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHpdHasAnOrgAttrTemplateValidator{


    private ModifHpdHasAnOrgAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdHasAnOrgSyntax
	* hpdHasAnOrg attribute SHALL be formatted as a distinguished name (DN) (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership MandatoryAttributes)
	*
	*/
	private static boolean _validateModifHpdHasAnOrgAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAnOrgSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdHasAnOrgValueCard
	* hpdHasAnOrg attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)
	*
	*/
	private static boolean _validateModifHpdHasAnOrgAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAnOrgValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHpdHasAnOrgAttrTemplate
    * Verify if an element can be token as a Template of type modifHpdHasAnOrgAttrTemplate
	*
	*/
	private static boolean _isModifHpdHasAnOrgAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hpdhasanorg");
				
	}
	/**
	* Validation of class-constraint : modifHpdHasAnOrgAttrTemplate
    * Verify if an element of type modifHpdHasAnOrgAttrTemplate can be validated by modifHpdHasAnOrgAttrTemplate
	*
	*/
	public static boolean _isModifHpdHasAnOrgAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHpdHasAnOrgAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHpdHasAnOrgAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHpdHasAnOrgAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHpdHasAnOrgAttrTemplate(aClass)){
			executeCons_ModifHpdHasAnOrgAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAnOrgSyntax(aClass, location, diagnostic);
			executeCons_ModifHpdHasAnOrgAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAnOrgValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHpdHasAnOrgAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAnOrgSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdHasAnOrgAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAnOrgSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdHasAnOrgSyntax");
		notif.setDescription("hpdHasAnOrg attribute SHALL be formatted as a distinguished name (DN) (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership MandatoryAttributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdHasAnOrgAttrTemplate-constraint_hpd_modifyRequest_hpdHasAnOrgSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-141"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifHpdHasAnOrgAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAnOrgValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdHasAnOrgAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAnOrgValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdHasAnOrgValueCard");
		notif.setDescription("hpdHasAnOrg attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdHasAnOrgAttrTemplate-constraint_hpd_modifyRequest_hpdHasAnOrgValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-142"));
		diagnostic.add(notif);
	}

}
