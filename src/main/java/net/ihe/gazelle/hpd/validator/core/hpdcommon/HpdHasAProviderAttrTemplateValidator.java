package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        hpdHasAProviderAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HpdHasAProviderAttrTemplateValidator{


    private HpdHasAProviderAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_hpdHasAProviderSyntax
	* hpdHasAProvider attribute SHALL be formatted as a distinguished name (DN) (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership MandatoryAttributes)
	*
	*/
	private static boolean _validateHpdHasAProviderAttrTemplate_Constraint_hpd_common_hpdHasAProviderSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_common_hpdHasAProviderValueCard
	* hpdHasAProvider attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)
	*
	*/
	private static boolean _validateHpdHasAProviderAttrTemplate_Constraint_hpd_common_hpdHasAProviderValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : hpdHasAProviderAttrTemplate
    * Verify if an element can be token as a Template of type hpdHasAProviderAttrTemplate
	*
	*/
	private static boolean _isHpdHasAProviderAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hpdhasaprovider");
				
	}
	/**
	* Validation of class-constraint : hpdHasAProviderAttrTemplate
    * Verify if an element of type hpdHasAProviderAttrTemplate can be validated by hpdHasAProviderAttrTemplate
	*
	*/
	public static boolean _isHpdHasAProviderAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHpdHasAProviderAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   hpdHasAProviderAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHpdHasAProviderAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHpdHasAProviderAttrTemplate(aClass)){
			executeCons_HpdHasAProviderAttrTemplate_Constraint_hpd_common_hpdHasAProviderSyntax(aClass, location, diagnostic);
			executeCons_HpdHasAProviderAttrTemplate_Constraint_hpd_common_hpdHasAProviderValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HpdHasAProviderAttrTemplate_Constraint_hpd_common_hpdHasAProviderSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHpdHasAProviderAttrTemplate_Constraint_hpd_common_hpdHasAProviderSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_hpdHasAProviderSyntax");
		notif.setDescription("hpdHasAProvider attribute SHALL be formatted as a distinguished name (DN) (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership MandatoryAttributes)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-hpdHasAProviderAttrTemplate-constraint_hpd_common_hpdHasAProviderSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-044"));
		diagnostic.add(notif);
	}

	private static void executeCons_HpdHasAProviderAttrTemplate_Constraint_hpd_common_hpdHasAProviderValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHpdHasAProviderAttrTemplate_Constraint_hpd_common_hpdHasAProviderValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_hpdHasAProviderValueCard");
		notif.setDescription("hpdHasAProvider attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-hpdHasAProviderAttrTemplate-constraint_hpd_common_hpdHasAProviderValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-045"));
		diagnostic.add(notif);
	}

}
