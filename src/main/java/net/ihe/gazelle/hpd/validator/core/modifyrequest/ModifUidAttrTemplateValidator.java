package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifUidAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifUidAttrTemplateValidator{


    private ModifUidAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_uidSyntax
	* uid attribute SHALL be formatted using the RDN Format as defined by ISO21091 section 9.2: IssuingAuhorityName:ID (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping and 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateModifUidAttrTemplate_Constraint_hpd_modifyRequest_uidSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, ".+[:]{1}.+")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_uidValueCard
	* uid attribute SHALL be single valued (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping and 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateModifUidAttrTemplate_Constraint_hpd_modifyRequest_uidValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifUidAttrTemplate
    * Verify if an element can be token as a Template of type modifUidAttrTemplate
	*
	*/
	private static boolean _isModifUidAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("uid");
				
	}
	/**
	* Validation of class-constraint : modifUidAttrTemplate
    * Verify if an element of type modifUidAttrTemplate can be validated by modifUidAttrTemplate
	*
	*/
	public static boolean _isModifUidAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifUidAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifUidAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifUidAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifUidAttrTemplate(aClass)){
			executeCons_ModifUidAttrTemplate_Constraint_hpd_modifyRequest_uidSyntax(aClass, location, diagnostic);
			executeCons_ModifUidAttrTemplate_Constraint_hpd_modifyRequest_uidValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifUidAttrTemplate_Constraint_hpd_modifyRequest_uidSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifUidAttrTemplate_Constraint_hpd_modifyRequest_uidSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_uidSyntax");
		notif.setDescription("uid attribute SHALL be formatted using the RDN Format as defined by ISO21091 section 9.2: IssuingAuhorityName:ID (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping and 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifUidAttrTemplate-constraint_hpd_modifyRequest_uidSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-101"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifUidAttrTemplate_Constraint_hpd_modifyRequest_uidValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifUidAttrTemplate_Constraint_hpd_modifyRequest_uidValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_uidValueCard");
		notif.setDescription("uid attribute SHALL be single valued (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping and 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifUidAttrTemplate-constraint_hpd_modifyRequest_uidValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-100"));
		diagnostic.add(notif);
	}

}
