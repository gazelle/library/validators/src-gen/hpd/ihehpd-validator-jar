package net.ihe.gazelle.hpd.validator.core.searchresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;


 /**
  * class :        hpdProviderStatusAttrForHCRegulatedOrganizationTemplate
  * package :   searchResponse
  * Template Class
  * Template identifier : 
  * Class of test : SearchResultEntry
  * 
  */
public final class HpdProviderStatusAttrForHCRegulatedOrganizationTemplateValidator{


    private HpdProviderStatusAttrForHCRegulatedOrganizationTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_searchResultEntryHCRegulatedOrganization_hpdProviderStatusValue
	* When the returned entry is for Organization Provider, valid values for attribute hpdProviderStatus are Active and Inactive (see Table 3.58.4.1.2.3-1: Status Code Category Values)
	*
	*/
	private static boolean _validateHpdProviderStatusAttrForHCRegulatedOrganizationTemplate_Constraint_hpd_searchResultEntryHCRegulatedOrganization_hpdProviderStatusValue(net.ihe.gazelle.hpd.SearchResultEntry aClass){
		java.util.ArrayList<DsmlAttr> result3;
		result3 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hpdproviderstatus")) {
			        result3.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		java.util.ArrayList<String> result2;
		result2 = new java.util.ArrayList<String>();
		
		/* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
		for (DsmlAttr anElement2 : result3) {
		    result2.addAll(anElement2.getValue());
		}
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement3 : result2) {
			    if (!(anElement3.toLowerCase().equals("active") || anElement3.toLowerCase().equals("inactive"))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : hpdProviderStatusAttrForHCRegulatedOrganizationTemplate
    * Verify if an element can be token as a Template of type hpdProviderStatusAttrForHCRegulatedOrganizationTemplate
	*
	*/
	private static boolean _isHpdProviderStatusAttrForHCRegulatedOrganizationTemplateTemplate(net.ihe.gazelle.hpd.SearchResultEntry aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hpdproviderstatus")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=hcregulatedorganization){1}.*") && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > new Integer(0)));
		
				
	}
	/**
	* Validation of class-constraint : hpdProviderStatusAttrForHCRegulatedOrganizationTemplate
    * Verify if an element of type hpdProviderStatusAttrForHCRegulatedOrganizationTemplate can be validated by hpdProviderStatusAttrForHCRegulatedOrganizationTemplate
	*
	*/
	public static boolean _isHpdProviderStatusAttrForHCRegulatedOrganizationTemplate(net.ihe.gazelle.hpd.SearchResultEntry aClass){
		return _isHpdProviderStatusAttrForHCRegulatedOrganizationTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   hpdProviderStatusAttrForHCRegulatedOrganizationTemplate
     * class ::  net.ihe.gazelle.hpd.SearchResultEntry
     * 
     */
    public static void _validateHpdProviderStatusAttrForHCRegulatedOrganizationTemplate(net.ihe.gazelle.hpd.SearchResultEntry aClass, String location, List<Notification> diagnostic) {
		if (_isHpdProviderStatusAttrForHCRegulatedOrganizationTemplate(aClass)){
			executeCons_HpdProviderStatusAttrForHCRegulatedOrganizationTemplate_Constraint_hpd_searchResultEntryHCRegulatedOrganization_hpdProviderStatusValue(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HpdProviderStatusAttrForHCRegulatedOrganizationTemplate_Constraint_hpd_searchResultEntryHCRegulatedOrganization_hpdProviderStatusValue(net.ihe.gazelle.hpd.SearchResultEntry aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHpdProviderStatusAttrForHCRegulatedOrganizationTemplate_Constraint_hpd_searchResultEntryHCRegulatedOrganization_hpdProviderStatusValue(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_searchResultEntryHCRegulatedOrganization_hpdProviderStatusValue");
		notif.setDescription("When the returned entry is for Organization Provider, valid values for attribute hpdProviderStatus are Active and Inactive (see Table 3.58.4.1.2.3-1: Status Code Category Values)");
		notif.setLocation(location);
		notif.setIdentifiant("searchResponse-hpdProviderStatusAttrForHCRegulatedOrganizationTemplate-constraint_hpd_searchResultEntryHCRegulatedOrganization_hpdProviderStatusValue");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-062"));
		diagnostic.add(notif);
	}

}
