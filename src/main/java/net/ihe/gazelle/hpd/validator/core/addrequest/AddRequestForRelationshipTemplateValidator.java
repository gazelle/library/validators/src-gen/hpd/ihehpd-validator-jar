package net.ihe.gazelle.hpd.validator.core.addrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;


 /**
  * class :        addRequestForRelationshipTemplate
  * package :   addRequest
  * Template Class
  * Template identifier : 
  * Class of test : AddRequest
  * 
  */
public final class AddRequestForRelationshipTemplateValidator{


    private AddRequestForRelationshipTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestRelationship_cnRequired
	* When the addRequest describes a relationship, the cn attribute is required (see ISO 21021:2013, section B.4 GroupOfNames)
	*
	*/
	private static boolean _validateAddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_cnRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("cn")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestRelationship_groupOfNamesObjectClass
	* Relationships in HPD are represented by LDAP objects using the groupOfNames class (see section 3.58.4.1.2.2.4 Relationships)
	*
	*/
	private static boolean _validateAddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_groupOfNamesObjectClass(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    java.util.ArrayList<String> result2;
			    result2 = new java.util.ArrayList<String>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (String anElement2 : anElement1.getValue()) {
			    	    if (anElement2.toLowerCase().equals("groupofnames")) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if ((anElement1.getName().toLowerCase().equals("objectclass") && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(new Integer(1)))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestRelationship_ownerSV
	* When the addRequest describes a relationship, the owner attribute is singled-valued (see section 3.58.4.1.2.2.4 Relationships)
	*
	*/
	private static boolean _validateAddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_ownerSV(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("owner")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) <= new Integer(1));
		
		
	}

	/**
	* Validation of template-constraint by a constraint : addRequestForRelationshipTemplate
    * Verify if an element can be token as a Template of type addRequestForRelationshipTemplate
	*
	*/
	private static boolean _isAddRequestForRelationshipTemplateTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=relationship){1}.*");
				
	}
	/**
	* Validation of class-constraint : addRequestForRelationshipTemplate
    * Verify if an element of type addRequestForRelationshipTemplate can be validated by addRequestForRelationshipTemplate
	*
	*/
	public static boolean _isAddRequestForRelationshipTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		return _isAddRequestForRelationshipTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   addRequestForRelationshipTemplate
     * class ::  net.ihe.gazelle.hpd.AddRequest
     * 
     */
    public static void _validateAddRequestForRelationshipTemplate(net.ihe.gazelle.hpd.AddRequest aClass, String location, List<Notification> diagnostic) {
		if (_isAddRequestForRelationshipTemplate(aClass)){
			executeCons_AddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_cnRequired(aClass, location, diagnostic);
			executeCons_AddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_groupOfNamesObjectClass(aClass, location, diagnostic);
			executeCons_AddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_ownerSV(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_cnRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_cnRequired(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestRelationship_cnRequired");
		notif.setDescription("When the addRequest describes a relationship, the cn attribute is required (see ISO 21021:2013, section B.4 GroupOfNames)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForRelationshipTemplate-constraint_hpd_addRequestRelationship_cnRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-082"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_groupOfNamesObjectClass(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_groupOfNamesObjectClass(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestRelationship_groupOfNamesObjectClass");
		notif.setDescription("Relationships in HPD are represented by LDAP objects using the groupOfNames class (see section 3.58.4.1.2.2.4 Relationships)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForRelationshipTemplate-constraint_hpd_addRequestRelationship_groupOfNamesObjectClass");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-084"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_ownerSV(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForRelationshipTemplate_Constraint_hpd_addRequestRelationship_ownerSV(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestRelationship_ownerSV");
		notif.setDescription("When the addRequest describes a relationship, the owner attribute is singled-valued (see section 3.58.4.1.2.2.4 Relationships)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForRelationshipTemplate-constraint_hpd_addRequestRelationship_ownerSV");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-159"));
		diagnostic.add(notif);
	}

}
