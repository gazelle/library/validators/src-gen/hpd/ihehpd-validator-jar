package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplateValidator{


    private ModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdMedicalRecordsDeliveryEmailAddressValueCard
	* hpdMedicalRecordsDeliveryEmailAddress attribute SHALL be single valued (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdMedicalRecordsDeliveryEmailAddressValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate
    * Verify if an element can be token as a Template of type modifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate
	*
	*/
	private static boolean _isModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hpdmedicalrecordsdeliveryemailaddress");
				
	}
	/**
	* Validation of class-constraint : modifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate
    * Verify if an element of type modifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate can be validated by modifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate
	*
	*/
	public static boolean _isModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate(aClass)){
			executeCons_ModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdMedicalRecordsDeliveryEmailAddressValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdMedicalRecordsDeliveryEmailAddressValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdMedicalRecordsDeliveryEmailAddressValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdMedicalRecordsDeliveryEmailAddressValueCard");
		notif.setDescription("hpdMedicalRecordsDeliveryEmailAddress attribute SHALL be single valued (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate-constraint_hpd_modifyRequest_hpdMedicalRecordsDeliveryEmailAddressValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-106"));
		diagnostic.add(notif);
	}

}
