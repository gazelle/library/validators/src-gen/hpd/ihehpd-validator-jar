package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifGeneralizedTimeAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifGeneralizedTimeAttrTemplateValidator{


    private ModifGeneralizedTimeAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_GeneralizedTimeSyntax
	* attributes of type date SHALL be formatted as a Generalized time and the time zone MUST be specified (see RFC 2252 section 6.14)
	*
	*/
	private static boolean _validateModifGeneralizedTimeAttrTemplate_Constraint_hpd_modifyRequest_GeneralizedTimeSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "\\d{4}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(0[0-9]|1[0-9]|2[123])((0[0-9]|[0-5][0-9])(0[0-9]|[0-5][0-9])?)?([,\\.]\\d*)?([+\\-]\\d{4}|Z)")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_GeneralizedTimeTimeZone
	*  It is strongly recommended that GMT time be used. (see RFC 2252 section 6.14. Generalized Time)
	*
	*/
	private static boolean _validateModifGeneralizedTimeAttrTemplate_Constraint_hpd_modifyRequest_GeneralizedTimeTimeZone(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, ".*Z$")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : modifGeneralizedTimeAttrTemplate
    * Verify if an element can be token as a Template of type modifGeneralizedTimeAttrTemplate
	*
	*/
	private static boolean _isModifGeneralizedTimeAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (aClass.getName().toLowerCase().equals("credentialissuedate") || aClass.getName().toLowerCase().equals("credentialrenewaldate"));
				
	}
	/**
	* Validation of class-constraint : modifGeneralizedTimeAttrTemplate
    * Verify if an element of type modifGeneralizedTimeAttrTemplate can be validated by modifGeneralizedTimeAttrTemplate
	*
	*/
	public static boolean _isModifGeneralizedTimeAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifGeneralizedTimeAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifGeneralizedTimeAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifGeneralizedTimeAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifGeneralizedTimeAttrTemplate(aClass)){
			executeCons_ModifGeneralizedTimeAttrTemplate_Constraint_hpd_modifyRequest_GeneralizedTimeSyntax(aClass, location, diagnostic);
			executeCons_ModifGeneralizedTimeAttrTemplate_Constraint_hpd_modifyRequest_GeneralizedTimeTimeZone(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifGeneralizedTimeAttrTemplate_Constraint_hpd_modifyRequest_GeneralizedTimeSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifGeneralizedTimeAttrTemplate_Constraint_hpd_modifyRequest_GeneralizedTimeSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_GeneralizedTimeSyntax");
		notif.setDescription("attributes of type date SHALL be formatted as a Generalized time and the time zone MUST be specified (see RFC 2252 section 6.14)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifGeneralizedTimeAttrTemplate-constraint_hpd_modifyRequest_GeneralizedTimeSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-148"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifGeneralizedTimeAttrTemplate_Constraint_hpd_modifyRequest_GeneralizedTimeTimeZone(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifGeneralizedTimeAttrTemplate_Constraint_hpd_modifyRequest_GeneralizedTimeTimeZone(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Warning();
		}
		notif.setTest("constraint_hpd_modifyRequest_GeneralizedTimeTimeZone");
		notif.setDescription("It is strongly recommended that GMT time be used. (see RFC 2252 section 6.14. Generalized Time)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifGeneralizedTimeAttrTemplate-constraint_hpd_modifyRequest_GeneralizedTimeTimeZone");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-149"));
		diagnostic.add(notif);
	}

}
