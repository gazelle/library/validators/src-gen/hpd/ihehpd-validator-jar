package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHpdProviderLanguageSupportedAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHpdProviderLanguageSupportedAttrTemplateValidator{


    private ModifHpdProviderLanguageSupportedAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdProviderLanguageSupportedException
	* Values for the hpdProviderLanguageSupported attribute type MUST conform to the Accept-Language header field defined in [RFC2068] with one Exception: the sequence 'Accept-Language' ':' should  be omitted (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateModifHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLanguageSupportedException(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!!aClass.matches(anElement1.toLowerCase(), "accepted-language.*")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdProviderLanguageSupportedSyntax
	* Values for the hpdProviderLanguageSupported attribute type MUST conform to the Accept-Language header field defined in [RFC2068] (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateModifHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLanguageSupportedSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "^[a-zA-Z\\-:]+(;q=(1|0\\.[0-9]+))?(,[a-zA-Z\\-\\s]*(;q=(1|0\\.[0-9]+))?)*")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHpdProviderLanguageSupportedAttrTemplate
    * Verify if an element can be token as a Template of type modifHpdProviderLanguageSupportedAttrTemplate
	*
	*/
	private static boolean _isModifHpdProviderLanguageSupportedAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hpdproviderlanguagesupported");
				
	}
	/**
	* Validation of class-constraint : modifHpdProviderLanguageSupportedAttrTemplate
    * Verify if an element of type modifHpdProviderLanguageSupportedAttrTemplate can be validated by modifHpdProviderLanguageSupportedAttrTemplate
	*
	*/
	public static boolean _isModifHpdProviderLanguageSupportedAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHpdProviderLanguageSupportedAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHpdProviderLanguageSupportedAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHpdProviderLanguageSupportedAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHpdProviderLanguageSupportedAttrTemplate(aClass)){
			executeCons_ModifHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLanguageSupportedException(aClass, location, diagnostic);
			executeCons_ModifHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLanguageSupportedSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLanguageSupportedException(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLanguageSupportedException(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Warning();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdProviderLanguageSupportedException");
		notif.setDescription("Values for the hpdProviderLanguageSupported attribute type MUST conform to the Accept-Language header field defined in [RFC2068] with one Exception: the sequence 'Accept-Language' ':' should  be omitted (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdProviderLanguageSupportedAttrTemplate-constraint_hpd_modifyRequest_hpdProviderLanguageSupportedException");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-131"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLanguageSupportedSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdProviderLanguageSupportedAttrTemplate_Constraint_hpd_modifyRequest_hpdProviderLanguageSupportedSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdProviderLanguageSupportedSyntax");
		notif.setDescription("Values for the hpdProviderLanguageSupported attribute type MUST conform to the Accept-Language header field defined in [RFC2068] (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdProviderLanguageSupportedAttrTemplate-constraint_hpd_modifyRequest_hpdProviderLanguageSupportedSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-130"));
		diagnostic.add(notif);
	}

}
