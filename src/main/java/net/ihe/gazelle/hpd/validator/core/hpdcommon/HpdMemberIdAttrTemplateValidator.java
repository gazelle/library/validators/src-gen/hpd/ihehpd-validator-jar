package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        hpdMemberIdAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HpdMemberIdAttrTemplateValidator{


    private HpdMemberIdAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_hpdMemberIdValueCard
	* hpdMemberId attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)
	*
	*/
	private static boolean _validateHpdMemberIdAttrTemplate_Constraint_hpd_common_hpdMemberIdValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : hpdMemberIdAttrTemplate
    * Verify if an element can be token as a Template of type hpdMemberIdAttrTemplate
	*
	*/
	private static boolean _isHpdMemberIdAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hpdmemberid");
				
	}
	/**
	* Validation of class-constraint : hpdMemberIdAttrTemplate
    * Verify if an element of type hpdMemberIdAttrTemplate can be validated by hpdMemberIdAttrTemplate
	*
	*/
	public static boolean _isHpdMemberIdAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHpdMemberIdAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   hpdMemberIdAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHpdMemberIdAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHpdMemberIdAttrTemplate(aClass)){
			executeCons_HpdMemberIdAttrTemplate_Constraint_hpd_common_hpdMemberIdValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HpdMemberIdAttrTemplate_Constraint_hpd_common_hpdMemberIdValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHpdMemberIdAttrTemplate_Constraint_hpd_common_hpdMemberIdValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_hpdMemberIdValueCard");
		notif.setDescription("hpdMemberId attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-hpdMemberIdAttrTemplate-constraint_hpd_common_hpdMemberIdValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-043"));
		diagnostic.add(notif);
	}

}
