package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifCredentialDescriptionAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifCredentialDescriptionAttrTemplateValidator{


    private ModifCredentialDescriptionAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_credentialDescriptionValueCard
	* credentialDescription attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-3: HPDProviderCredential Optional Attributes)
	*
	*/
	private static boolean _validateModifCredentialDescriptionAttrTemplate_Constraint_hpd_modifyRequest_credentialDescriptionValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifCredentialDescriptionAttrTemplate
    * Verify if an element can be token as a Template of type modifCredentialDescriptionAttrTemplate
	*
	*/
	private static boolean _isModifCredentialDescriptionAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("credentialdescription");
				
	}
	/**
	* Validation of class-constraint : modifCredentialDescriptionAttrTemplate
    * Verify if an element of type modifCredentialDescriptionAttrTemplate can be validated by modifCredentialDescriptionAttrTemplate
	*
	*/
	public static boolean _isModifCredentialDescriptionAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifCredentialDescriptionAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifCredentialDescriptionAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifCredentialDescriptionAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifCredentialDescriptionAttrTemplate(aClass)){
			executeCons_ModifCredentialDescriptionAttrTemplate_Constraint_hpd_modifyRequest_credentialDescriptionValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifCredentialDescriptionAttrTemplate_Constraint_hpd_modifyRequest_credentialDescriptionValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifCredentialDescriptionAttrTemplate_Constraint_hpd_modifyRequest_credentialDescriptionValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_credentialDescriptionValueCard");
		notif.setDescription("credentialDescription attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-3: HPDProviderCredential Optional Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifCredentialDescriptionAttrTemplate-constraint_hpd_modifyRequest_credentialDescriptionValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-113"));
		diagnostic.add(notif);
	}

}
