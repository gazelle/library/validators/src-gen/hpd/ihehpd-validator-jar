package net.ihe.gazelle.hpd.validator.core.ldapresult;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        LDAPResultSpec
  * package :   LDAPResult
  * Constraint Spec Class
  * class of test : LDAPResult
  * 
  */
public final class LDAPResultSpecValidator{


    private LDAPResultSpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_ldapResult_errorMessageCard
	* The response shall not contain any errorMessage element as any processing errors are not in scope. (see Section 3.59.4.2.2: Message Semantics)
	*
	*/
	private static boolean _validateLDAPResultSpec_Constraint_hpd_ldapResult_errorMessageCard(net.ihe.gazelle.hpd.LDAPResult aClass){
		return (((String) aClass.getErrorMessage()) == null);
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_ldapResult_resultCodeValue
	* The resultCode for an acknowledgement shall be reported as '0' to imply acknowledgement. (see Session 3.59.4.2.2 Message Semantics)
	*
	*/
	private static boolean _validateLDAPResultSpec_Constraint_hpd_ldapResult_resultCodeValue(net.ihe.gazelle.hpd.LDAPResult aClass){
		return ((!(aClass.getResultCode() == null) && !(((Integer) aClass.getResultCode().getCode()) == null)) && ((Object) aClass.getResultCode().getCode()).equals(new Integer(0)));
		
	}

	/**
	* Validation of class-constraint : LDAPResultSpec
    * Verify if an element of type LDAPResultSpec can be validated by LDAPResultSpec
	*
	*/
	public static boolean _isLDAPResultSpec(net.ihe.gazelle.hpd.LDAPResult aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   LDAPResultSpec
     * class ::  net.ihe.gazelle.hpd.LDAPResult
     * 
     */
    public static void _validateLDAPResultSpec(net.ihe.gazelle.hpd.LDAPResult aClass, String location, List<Notification> diagnostic) {
		if (_isLDAPResultSpec(aClass)){
			executeCons_LDAPResultSpec_Constraint_hpd_ldapResult_errorMessageCard(aClass, location, diagnostic);
			executeCons_LDAPResultSpec_Constraint_hpd_ldapResult_resultCodeValue(aClass, location, diagnostic);
		}
	}

	private static void executeCons_LDAPResultSpec_Constraint_hpd_ldapResult_errorMessageCard(net.ihe.gazelle.hpd.LDAPResult aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateLDAPResultSpec_Constraint_hpd_ldapResult_errorMessageCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_ldapResult_errorMessageCard");
		notif.setDescription("The response shall not contain any errorMessage element as any processing errors are not in scope. (see Section 3.59.4.2.2: Message Semantics)");
		notif.setLocation(location);
		notif.setIdentifiant("LDAPResult-LDAPResultSpec-constraint_hpd_ldapResult_errorMessageCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-057"));
		diagnostic.add(notif);
	}

	private static void executeCons_LDAPResultSpec_Constraint_hpd_ldapResult_resultCodeValue(net.ihe.gazelle.hpd.LDAPResult aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateLDAPResultSpec_Constraint_hpd_ldapResult_resultCodeValue(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_ldapResult_resultCodeValue");
		notif.setDescription("The resultCode for an acknowledgement shall be reported as '0' to imply acknowledgement. (see Session 3.59.4.2.2 Message Semantics)");
		notif.setLocation(location);
		notif.setIdentifiant("LDAPResult-LDAPResultSpec-constraint_hpd_ldapResult_resultCodeValue");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-056"));
		diagnostic.add(notif);
	}

}
