package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        ModifyRequestSpec
  * package :   modifyRequest
  * Constraint Spec Class
  * class of test : ModifyRequest
  * 
  */
public final class ModifyRequestSpecValidator{


    private ModifyRequestSpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_createTimestampCard
	* createTimestamp is an operation attribute that LDAP directory server maintains to capture the time when an entry was created
	*
	*/
	private static boolean _validateModifyRequestSpec_Constraint_hpd_modifyRequest_createTimestampCard(net.ihe.gazelle.hpd.ModifyRequest aClass){
		java.util.ArrayList<DsmlModification> result1;
		result1 = new java.util.ArrayList<DsmlModification>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlModification anElement1 : aClass.getModification()) {
			    if (anElement1.getName().toLowerCase().equals("createtimestamp")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(0));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_dnSyntax
	* dn attribute SHALL be formatted as a Distinguish name string.
	*
	*/
	private static boolean _validateModifyRequestSpec_Constraint_hpd_modifyRequest_dnSyntax(net.ihe.gazelle.hpd.ModifyRequest aClass){
		return aClass.matches(((String) aClass.getDn()), "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$");
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_dnValidOu
	* Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization, ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)               
	*
	*/
	private static boolean _validateModifyRequestSpec_Constraint_hpd_modifyRequest_dnValidOu(net.ihe.gazelle.hpd.ModifyRequest aClass){
		return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=(hcprofessional|hcregulatedorganization|relationship|hpdcredential)){1}.*");
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_modifyTimestampCard
	* modifyTimestamp is an operation attribute that LDAP directory server maintains to capture the time when an entry was modified
	*
	*/
	private static boolean _validateModifyRequestSpec_Constraint_hpd_modifyRequest_modifyTimestampCard(net.ihe.gazelle.hpd.ModifyRequest aClass){
		java.util.ArrayList<DsmlModification> result1;
		result1 = new java.util.ArrayList<DsmlModification>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlModification anElement1 : aClass.getModification()) {
			    if (anElement1.getName().toLowerCase().equals("modifytimestamp")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(0));
		
		
	}

	/**
	* Validation of class-constraint : ModifyRequestSpec
    * Verify if an element of type ModifyRequestSpec can be validated by ModifyRequestSpec
	*
	*/
	public static boolean _isModifyRequestSpec(net.ihe.gazelle.hpd.ModifyRequest aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ModifyRequestSpec
     * class ::  net.ihe.gazelle.hpd.ModifyRequest
     * 
     */
    public static void _validateModifyRequestSpec(net.ihe.gazelle.hpd.ModifyRequest aClass, String location, List<Notification> diagnostic) {
		if (_isModifyRequestSpec(aClass)){
			executeCons_ModifyRequestSpec_Constraint_hpd_modifyRequest_createTimestampCard(aClass, location, diagnostic);
			executeCons_ModifyRequestSpec_Constraint_hpd_modifyRequest_dnSyntax(aClass, location, diagnostic);
			executeCons_ModifyRequestSpec_Constraint_hpd_modifyRequest_dnValidOu(aClass, location, diagnostic);
			executeCons_ModifyRequestSpec_Constraint_hpd_modifyRequest_modifyTimestampCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifyRequestSpec_Constraint_hpd_modifyRequest_createTimestampCard(net.ihe.gazelle.hpd.ModifyRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifyRequestSpec_Constraint_hpd_modifyRequest_createTimestampCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_createTimestampCard");
		notif.setDescription("createTimestamp is an operation attribute that LDAP directory server maintains to capture the time when an entry was created");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-ModifyRequestSpec-constraint_hpd_modifyRequest_createTimestampCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-154"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifyRequestSpec_Constraint_hpd_modifyRequest_dnSyntax(net.ihe.gazelle.hpd.ModifyRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifyRequestSpec_Constraint_hpd_modifyRequest_dnSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_dnSyntax");
		notif.setDescription("dn attribute SHALL be formatted as a Distinguish name string.");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-ModifyRequestSpec-constraint_hpd_modifyRequest_dnSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-151"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifyRequestSpec_Constraint_hpd_modifyRequest_dnValidOu(net.ihe.gazelle.hpd.ModifyRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifyRequestSpec_Constraint_hpd_modifyRequest_dnValidOu(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_dnValidOu");
		notif.setDescription("Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization, ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-ModifyRequestSpec-constraint_hpd_modifyRequest_dnValidOu");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-152"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifyRequestSpec_Constraint_hpd_modifyRequest_modifyTimestampCard(net.ihe.gazelle.hpd.ModifyRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifyRequestSpec_Constraint_hpd_modifyRequest_modifyTimestampCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_modifyTimestampCard");
		notif.setDescription("modifyTimestamp is an operation attribute that LDAP directory server maintains to capture the time when an entry was modified");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-ModifyRequestSpec-constraint_hpd_modifyRequest_modifyTimestampCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-153"));
		diagnostic.add(notif);
	}

}
