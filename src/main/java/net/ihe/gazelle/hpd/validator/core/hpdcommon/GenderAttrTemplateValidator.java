package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        genderAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class GenderAttrTemplateValidator{


    private GenderAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_genderValue
	* gender attribute SHALL follow the Natural Person auxilary class as defined in RFC 2985, that means that only Male (M or m) and Female (F or f) values are allowed. (see Table 3.58.4.1.2.2.2-1)
	*
	*/
	private static boolean _validateGenderAttrTemplate_Constraint_hpd_common_genderValue(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!(((anElement1.equals("M") || anElement1.equals("m")) || anElement1.equals("F")) || anElement1.equals("f"))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_common_genderValueCard
	* gender attribute SHALL be single valued (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateGenderAttrTemplate_Constraint_hpd_common_genderValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : genderAttrTemplate
    * Verify if an element can be token as a Template of type genderAttrTemplate
	*
	*/
	private static boolean _isGenderAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("gender");
				
	}
	/**
	* Validation of class-constraint : genderAttrTemplate
    * Verify if an element of type genderAttrTemplate can be validated by genderAttrTemplate
	*
	*/
	public static boolean _isGenderAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isGenderAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   genderAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateGenderAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isGenderAttrTemplate(aClass)){
			executeCons_GenderAttrTemplate_Constraint_hpd_common_genderValue(aClass, location, diagnostic);
			executeCons_GenderAttrTemplate_Constraint_hpd_common_genderValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_GenderAttrTemplate_Constraint_hpd_common_genderValue(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateGenderAttrTemplate_Constraint_hpd_common_genderValue(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_genderValue");
		notif.setDescription("gender attribute SHALL follow the Natural Person auxilary class as defined in RFC 2985, that means that only Male (M or m) and Female (F or f) values are allowed. (see Table 3.58.4.1.2.2.2-1)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-genderAttrTemplate-constraint_hpd_common_genderValue");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-007"));
		diagnostic.add(notif);
	}

	private static void executeCons_GenderAttrTemplate_Constraint_hpd_common_genderValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateGenderAttrTemplate_Constraint_hpd_common_genderValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_genderValueCard");
		notif.setDescription("gender attribute SHALL be single valued (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-genderAttrTemplate-constraint_hpd_common_genderValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-006"));
		diagnostic.add(notif);
	}

}
