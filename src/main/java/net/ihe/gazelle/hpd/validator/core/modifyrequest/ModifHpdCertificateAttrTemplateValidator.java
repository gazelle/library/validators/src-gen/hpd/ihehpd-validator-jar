package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHpdCertificateAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHpdCertificateAttrTemplateValidator{


    private ModifHpdCertificateAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdCertificateSyntax
	* LDAP syntax to be used for hpdCertificate attribute is Binary (see Table 3.58.4.1.2.2.1-6 HPDElectronicService Mandatory Attributes)
	*
	*/
	private static boolean _validateModifHpdCertificateAttrTemplate_Constraint_hpd_modifyRequest_hpdCertificateSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHpdCertificateAttrTemplate
    * Verify if an element can be token as a Template of type modifHpdCertificateAttrTemplate
	*
	*/
	private static boolean _isModifHpdCertificateAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hpdcertificate");
				
	}
	/**
	* Validation of class-constraint : modifHpdCertificateAttrTemplate
    * Verify if an element of type modifHpdCertificateAttrTemplate can be validated by modifHpdCertificateAttrTemplate
	*
	*/
	public static boolean _isModifHpdCertificateAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHpdCertificateAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHpdCertificateAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHpdCertificateAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHpdCertificateAttrTemplate(aClass)){
			executeCons_ModifHpdCertificateAttrTemplate_Constraint_hpd_modifyRequest_hpdCertificateSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHpdCertificateAttrTemplate_Constraint_hpd_modifyRequest_hpdCertificateSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdCertificateAttrTemplate_Constraint_hpd_modifyRequest_hpdCertificateSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdCertificateSyntax");
		notif.setDescription("LDAP syntax to be used for hpdCertificate attribute is Binary (see Table 3.58.4.1.2.2.1-6 HPDElectronicService Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdCertificateAttrTemplate-constraint_hpd_modifyRequest_hpdCertificateSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-145"));
		diagnostic.add(notif);
	}

}
