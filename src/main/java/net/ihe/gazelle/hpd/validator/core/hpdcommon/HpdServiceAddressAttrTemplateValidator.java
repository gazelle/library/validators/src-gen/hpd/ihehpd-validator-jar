package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        hpdServiceAddressAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HpdServiceAddressAttrTemplateValidator{


    private HpdServiceAddressAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_hpdServiceAddressValueCard
	* hpdServiceAddress attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)
	*
	*/
	private static boolean _validateHpdServiceAddressAttrTemplate_Constraint_hpd_common_hpdServiceAddressValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : hpdServiceAddressAttrTemplate
    * Verify if an element can be token as a Template of type hpdServiceAddressAttrTemplate
	*
	*/
	private static boolean _isHpdServiceAddressAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hpdserviceaddress");
				
	}
	/**
	* Validation of class-constraint : hpdServiceAddressAttrTemplate
    * Verify if an element of type hpdServiceAddressAttrTemplate can be validated by hpdServiceAddressAttrTemplate
	*
	*/
	public static boolean _isHpdServiceAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHpdServiceAddressAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   hpdServiceAddressAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHpdServiceAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHpdServiceAddressAttrTemplate(aClass)){
			executeCons_HpdServiceAddressAttrTemplate_Constraint_hpd_common_hpdServiceAddressValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HpdServiceAddressAttrTemplate_Constraint_hpd_common_hpdServiceAddressValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHpdServiceAddressAttrTemplate_Constraint_hpd_common_hpdServiceAddressValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_hpdServiceAddressValueCard");
		notif.setDescription("hpdServiceAddress attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-hpdServiceAddressAttrTemplate-constraint_hpd_common_hpdServiceAddressValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-049"));
		diagnostic.add(notif);
	}

}
