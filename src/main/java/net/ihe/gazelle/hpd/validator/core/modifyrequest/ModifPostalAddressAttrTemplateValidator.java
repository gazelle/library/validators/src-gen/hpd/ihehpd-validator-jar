package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifPostalAddressAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifPostalAddressAttrTemplateValidator{


    private ModifPostalAddressAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_PostalAddressAddrMandatory
	* If an address is included, then the addr is required (see section 3.58.4.1.2.2.1 Object Classes)
	*
	*/
	private static boolean _validateModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressAddrMandatory(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1.toLowerCase(), ".*(addr=){1}.*", new Boolean(true))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_PostalAddressStatus
	* Valid values for postal address status are Primary, Secondary, Inactive (see Table 3.58.4.1.2.3-1: Status Code Category Values)
	*
	*/
	private static boolean _validateModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressStatus(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!(aClass.matches(anElement1.toLowerCase(), ".*(status=primary|status=secondary|status=inactive){1}.*", new Boolean(true)) ^ !aClass.matches(anElement1, ".*(status=){1}.*", new Boolean(true)))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_PostalAddressSyntax
	* PostalAddress syntax SHALL be dstring *(  $  dstring) and enforce format of  key=value 
	*
	*/
	private static boolean _validateModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "[^=\\$]*={1}[^=\\$]*(\\${1}[^=\\$]*={1}[^=\\$]*)*", new Boolean(true))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : modifPostalAddressAttrTemplate
    * Verify if an element can be token as a Template of type modifPostalAddressAttrTemplate
	*
	*/
	private static boolean _isModifPostalAddressAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (((aClass.getName().toLowerCase().equals("hpdproviderlegaladdress") || aClass.getName().toLowerCase().equals("hpdproviderbillingaddress")) || aClass.getName().toLowerCase().equals("hpdprovidermailingaddress")) || aClass.getName().toLowerCase().equals("hpdproviderpracticeaddress"));
				
	}
	/**
	* Validation of class-constraint : modifPostalAddressAttrTemplate
    * Verify if an element of type modifPostalAddressAttrTemplate can be validated by modifPostalAddressAttrTemplate
	*
	*/
	public static boolean _isModifPostalAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifPostalAddressAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifPostalAddressAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifPostalAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifPostalAddressAttrTemplate(aClass)){
			executeCons_ModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressAddrMandatory(aClass, location, diagnostic);
			executeCons_ModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressStatus(aClass, location, diagnostic);
			executeCons_ModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressAddrMandatory(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressAddrMandatory(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_PostalAddressAddrMandatory");
		notif.setDescription("If an address is included, then the addr is required (see section 3.58.4.1.2.2.1 Object Classes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifPostalAddressAttrTemplate-constraint_hpd_modifyRequest_PostalAddressAddrMandatory");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-133"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressStatus(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressStatus(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_PostalAddressStatus");
		notif.setDescription("Valid values for postal address status are Primary, Secondary, Inactive (see Table 3.58.4.1.2.3-1: Status Code Category Values)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifPostalAddressAttrTemplate-constraint_hpd_modifyRequest_PostalAddressStatus");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-134"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifPostalAddressAttrTemplate_Constraint_hpd_modifyRequest_PostalAddressSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_PostalAddressSyntax");
		notif.setDescription("PostalAddress syntax SHALL be dstring *(  $  dstring) and enforce format of  key=value");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifPostalAddressAttrTemplate-constraint_hpd_modifyRequest_PostalAddressSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-135"));
		diagnostic.add(notif);
	}

}
