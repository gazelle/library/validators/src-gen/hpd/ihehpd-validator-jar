package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


public class HPDCOMMONPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public HPDCOMMONPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hpd.DsmlAttr){
			net.ihe.gazelle.hpd.DsmlAttr aClass = ( net.ihe.gazelle.hpd.DsmlAttr)obj;
			ClinicalInformationContactAttrTemplateValidator._validateClinicalInformationContactAttrTemplate(aClass, location, diagnostic);
			GeneralizedTimeAttrTemplateValidator._validateGeneralizedTimeAttrTemplate(aClass, location, diagnostic);
			HcIdentifierAttrTemplateValidator._validateHcIdentifierAttrTemplate(aClass, location, diagnostic);
			HcOrganizationCertificateAttrTemplateValidator._validateHcOrganizationCertificateAttrTemplate(aClass, location, diagnostic);
			HcPracticeLocationAttrTemplateValidator._validateHcPracticeLocationAttrTemplate(aClass, location, diagnostic);
			HcSigningCertificateAttrTemplateValidator._validateHcSigningCertificateAttrTemplate(aClass, location, diagnostic);
			HcSpecialisationAttrTemplateValidator._validateHcSpecialisationAttrTemplate(aClass, location, diagnostic);
			PostalAddressAttrTemplateValidator._validatePostalAddressAttrTemplate(aClass, location, diagnostic);
			CreateTimestampAttrTemplateValidator._validateCreateTimestampAttrTemplate(aClass, location, diagnostic);
			CredentialDescriptionAttrTemplateValidator._validateCredentialDescriptionAttrTemplate(aClass, location, diagnostic);
			CredentialIssueDateAttrTemplateValidator._validateCredentialIssueDateAttrTemplate(aClass, location, diagnostic);
			CredentialNameAttrTemplateValidator._validateCredentialNameAttrTemplate(aClass, location, diagnostic);
			CredentialNumberAttrTemplateValidator._validateCredentialNumberAttrTemplate(aClass, location, diagnostic);
			CredentialRenewalDateAttrTemplateValidator._validateCredentialRenewalDateAttrTemplate(aClass, location, diagnostic);
			CredentialStatusAttrTemplateValidator._validateCredentialStatusAttrTemplate(aClass, location, diagnostic);
			CredentialTypeAttrTemplateValidator._validateCredentialTypeAttrTemplate(aClass, location, diagnostic);
			DisplayNameAttrTemplateValidator._validateDisplayNameAttrTemplate(aClass, location, diagnostic);
			GenderAttrTemplateValidator._validateGenderAttrTemplate(aClass, location, diagnostic);
			HpdCertificateAttrTemplateValidator._validateHpdCertificateAttrTemplate(aClass, location, diagnostic);
			HpdCredentialAttrTemplateValidator._validateHpdCredentialAttrTemplate(aClass, location, diagnostic);
			HpdHasAProviderAttrTemplateValidator._validateHpdHasAProviderAttrTemplate(aClass, location, diagnostic);
			HpdHasAServiceAttrTemplateValidator._validateHpdHasAServiceAttrTemplate(aClass, location, diagnostic);
			HpdHasAnOrgAttrTemplateValidator._validateHpdHasAnOrgAttrTemplate(aClass, location, diagnostic);
			HpdMedicalRecordsDeliveryEmailAddressAttrTemplateValidator._validateHpdMedicalRecordsDeliveryEmailAddressAttrTemplate(aClass, location, diagnostic);
			HpdMemberIdAttrTemplateValidator._validateHpdMemberIdAttrTemplate(aClass, location, diagnostic);
			HpdProviderLanguageSupportedAttrTemplateValidator._validateHpdProviderLanguageSupportedAttrTemplate(aClass, location, diagnostic);
			HpdProviderLegalAddressAttrTemplateValidator._validateHpdProviderLegalAddressAttrTemplate(aClass, location, diagnostic);
			HpdProviderStatusAttrTemplateValidator._validateHpdProviderStatusAttrTemplate(aClass, location, diagnostic);
			HpdServiceAddressAttrTemplateValidator._validateHpdServiceAddressAttrTemplate(aClass, location, diagnostic);
			HpdServiceIdAttrTemplateValidator._validateHpdServiceIdAttrTemplate(aClass, location, diagnostic);
			MemberOfAttrTemplateValidator._validateMemberOfAttrTemplate(aClass, location, diagnostic);
			MobileAttrTemplateValidator._validateMobileAttrTemplate(aClass, location, diagnostic);
			ModifyTimestampAttrTemplateValidator._validateModifyTimestampAttrTemplate(aClass, location, diagnostic);
			NameDatatypeTemplateValidator._validateNameDatatypeTemplate(aClass, location, diagnostic);
			ObjectClassAttrTemplateValidator._validateObjectClassAttrTemplate(aClass, location, diagnostic);
			PagerAttrTemplateValidator._validatePagerAttrTemplate(aClass, location, diagnostic);
			TelephoneNumberAttrTemplateValidator._validateTelephoneNumberAttrTemplate(aClass, location, diagnostic);
			TitleAttrTemplateValidator._validateTitleAttrTemplate(aClass, location, diagnostic);
			UidAttrTemplateValidator._validateUidAttrTemplate(aClass, location, diagnostic);
			UserCertificateAttrTemplateValidator._validateUserCertificateAttrTemplate(aClass, location, diagnostic);
			UserSMIMECertificateAttrTemplateValidator._validateUserSMIMECertificateAttrTemplate(aClass, location, diagnostic);
		}
	
	}

}

