package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifTitleAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifTitleAttrTemplateValidator{


    private ModifTitleAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_titleValueCard
	* title attribute SHALL be single valued (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateModifTitleAttrTemplate_Constraint_hpd_modifyRequest_titleValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifTitleAttrTemplate
    * Verify if an element can be token as a Template of type modifTitleAttrTemplate
	*
	*/
	private static boolean _isModifTitleAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.matches(aClass.getName().toLowerCase(), "title((;lang\\-){1}.+)?");
				
	}
	/**
	* Validation of class-constraint : modifTitleAttrTemplate
    * Verify if an element of type modifTitleAttrTemplate can be validated by modifTitleAttrTemplate
	*
	*/
	public static boolean _isModifTitleAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifTitleAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifTitleAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifTitleAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifTitleAttrTemplate(aClass)){
			executeCons_ModifTitleAttrTemplate_Constraint_hpd_modifyRequest_titleValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifTitleAttrTemplate_Constraint_hpd_modifyRequest_titleValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifTitleAttrTemplate_Constraint_hpd_modifyRequest_titleValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_titleValueCard");
		notif.setDescription("title attribute SHALL be single valued (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifTitleAttrTemplate-constraint_hpd_modifyRequest_titleValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-103"));
		diagnostic.add(notif);
	}

}
