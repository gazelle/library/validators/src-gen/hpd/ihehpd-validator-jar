package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        GeneralizedTimeAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class GeneralizedTimeAttrTemplateValidator{


    private GeneralizedTimeAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_GeneralizedTimeSyntax
	* attributes of type date SHALL be formatted  as a Generalized time and the time zone MUST be specified (see RFC 2252 section 6.14)
	*
	*/
	private static boolean _validateGeneralizedTimeAttrTemplate_Constraint_hpd_common_GeneralizedTimeSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "\\d{4}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(0[0-9]|1[0-9]|2[0123])((0[0-9]|[0-5][0-9])(0[0-9]|[0-5][0-9])?)?([,\\.]\\d*)?([+\\-]\\d{4}|Z)")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_common_GeneralizedTimeTimeZone
	*  It is strongly recommended that GMT time be used. (see RFC 2252 section 6.14. Generalized Time)
	*
	*/
	private static boolean _validateGeneralizedTimeAttrTemplate_Constraint_hpd_common_GeneralizedTimeTimeZone(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, ".*Z$")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : GeneralizedTimeAttrTemplate
    * Verify if an element can be token as a Template of type GeneralizedTimeAttrTemplate
	*
	*/
	private static boolean _isGeneralizedTimeAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (((aClass.getName().toLowerCase().equals("createtimestamp") || aClass.getName().toLowerCase().equals("modifytimestamp")) || aClass.getName().toLowerCase().equals("credentialissuedate")) || aClass.getName().toLowerCase().equals("credentialrenewaldate"));
				
	}
	/**
	* Validation of class-constraint : GeneralizedTimeAttrTemplate
    * Verify if an element of type GeneralizedTimeAttrTemplate can be validated by GeneralizedTimeAttrTemplate
	*
	*/
	public static boolean _isGeneralizedTimeAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isGeneralizedTimeAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   GeneralizedTimeAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateGeneralizedTimeAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isGeneralizedTimeAttrTemplate(aClass)){
			executeCons_GeneralizedTimeAttrTemplate_Constraint_hpd_common_GeneralizedTimeSyntax(aClass, location, diagnostic);
			executeCons_GeneralizedTimeAttrTemplate_Constraint_hpd_common_GeneralizedTimeTimeZone(aClass, location, diagnostic);
		}
	}

	private static void executeCons_GeneralizedTimeAttrTemplate_Constraint_hpd_common_GeneralizedTimeSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateGeneralizedTimeAttrTemplate_Constraint_hpd_common_GeneralizedTimeSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_GeneralizedTimeSyntax");
		notif.setDescription("attributes of type date SHALL be formatted  as a Generalized time and the time zone MUST be specified (see RFC 2252 section 6.14)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-GeneralizedTimeAttrTemplate-constraint_hpd_common_GeneralizedTimeSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-053"));
		diagnostic.add(notif);
	}

	private static void executeCons_GeneralizedTimeAttrTemplate_Constraint_hpd_common_GeneralizedTimeTimeZone(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateGeneralizedTimeAttrTemplate_Constraint_hpd_common_GeneralizedTimeTimeZone(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Warning();
		}
		notif.setTest("constraint_hpd_common_GeneralizedTimeTimeZone");
		notif.setDescription("It is strongly recommended that GMT time be used. (see RFC 2252 section 6.14. Generalized Time)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-GeneralizedTimeAttrTemplate-constraint_hpd_common_GeneralizedTimeTimeZone");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-054"));
		diagnostic.add(notif);
	}

}
