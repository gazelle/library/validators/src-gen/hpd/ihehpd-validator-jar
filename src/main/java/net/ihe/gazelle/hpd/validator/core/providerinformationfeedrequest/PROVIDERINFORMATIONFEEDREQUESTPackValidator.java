package net.ihe.gazelle.hpd.validator.core.providerinformationfeedrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





public class PROVIDERINFORMATIONFEEDREQUESTPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public PROVIDERINFORMATIONFEEDREQUESTPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hpd.BatchRequest){
			net.ihe.gazelle.hpd.BatchRequest aClass = ( net.ihe.gazelle.hpd.BatchRequest)obj;
			FeedRequestSpecValidator._validateFeedRequestSpec(aClass, location, diagnostic);
		}
	
	}

}

