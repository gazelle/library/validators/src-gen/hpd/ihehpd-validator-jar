package net.ihe.gazelle.hpd.validator.core.addrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;


 /**
  * class :        addRequestForHPDProviderCredentialTemplate
  * package :   addRequest
  * Template Class
  * Template identifier : 
  * Class of test : AddRequest
  * 
  */
public final class AddRequestForHPDProviderCredentialTemplateValidator{


    private AddRequestForHPDProviderCredentialTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHPDProviderCredential_credentialNameMandatory
	* credentialName is a mandatory attribute of HPDProviderCredential object class. (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)
	*
	*/
	private static boolean _validateAddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialNameMandatory(net.ihe.gazelle.hpd.AddRequest aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator One: Iterate and check, if exactly one element fulfills the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("credentialname")) {
			        if (result1) {
			            // Found a second element.
			            result1 = false;
			            break;
			        } else {
			            // Found a first element.
			            result1 = true;
				    }
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHPDProviderCredential_credentialNumberMandatory
	* credentialNumber is a mandatory attribute of HPDProviderCredential object class. (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)
	*
	*/
	private static boolean _validateAddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialNumberMandatory(net.ihe.gazelle.hpd.AddRequest aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator One: Iterate and check, if exactly one element fulfills the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("credentialnumber")) {
			        if (result1) {
			            // Found a second element.
			            result1 = false;
			            break;
			        } else {
			            // Found a first element.
			            result1 = true;
				    }
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHPDProviderCredential_credentialTypeMandatory
	* credentialType is a mandatory attribute of HPDProviderCredential object class. (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)
	*
	*/
	private static boolean _validateAddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialTypeMandatory(net.ihe.gazelle.hpd.AddRequest aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator One: Iterate and check, if exactly one element fulfills the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("credentialtype")) {
			        if (result1) {
			            // Found a second element.
			            result1 = false;
			            break;
			        } else {
			            // Found a first element.
			            result1 = true;
				    }
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : addRequestForHPDProviderCredentialTemplate
    * Verify if an element can be token as a Template of type addRequestForHPDProviderCredentialTemplate
	*
	*/
	private static boolean _isAddRequestForHPDProviderCredentialTemplateTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    java.util.ArrayList<String> result2;
			    result2 = new java.util.ArrayList<String>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (String anElement2 : anElement1.getValue()) {
			    	    if (anElement2.toLowerCase().equals("hpdprovidercredential")) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if ((anElement1.getName().toLowerCase().equals("objectclass") && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2) > new Integer(0)))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > new Integer(0));
		
				
	}
	/**
	* Validation of class-constraint : addRequestForHPDProviderCredentialTemplate
    * Verify if an element of type addRequestForHPDProviderCredentialTemplate can be validated by addRequestForHPDProviderCredentialTemplate
	*
	*/
	public static boolean _isAddRequestForHPDProviderCredentialTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		return _isAddRequestForHPDProviderCredentialTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   addRequestForHPDProviderCredentialTemplate
     * class ::  net.ihe.gazelle.hpd.AddRequest
     * 
     */
    public static void _validateAddRequestForHPDProviderCredentialTemplate(net.ihe.gazelle.hpd.AddRequest aClass, String location, List<Notification> diagnostic) {
		if (_isAddRequestForHPDProviderCredentialTemplate(aClass)){
			executeCons_AddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialNameMandatory(aClass, location, diagnostic);
			executeCons_AddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialNumberMandatory(aClass, location, diagnostic);
			executeCons_AddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialTypeMandatory(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialNameMandatory(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialNameMandatory(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHPDProviderCredential_credentialNameMandatory");
		notif.setDescription("credentialName is a mandatory attribute of HPDProviderCredential object class. (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHPDProviderCredentialTemplate-constraint_hpd_addRequestHPDProviderCredential_credentialNameMandatory");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-086"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialNumberMandatory(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialNumberMandatory(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHPDProviderCredential_credentialNumberMandatory");
		notif.setDescription("credentialNumber is a mandatory attribute of HPDProviderCredential object class. (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHPDProviderCredentialTemplate-constraint_hpd_addRequestHPDProviderCredential_credentialNumberMandatory");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-087"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialTypeMandatory(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHPDProviderCredentialTemplate_Constraint_hpd_addRequestHPDProviderCredential_credentialTypeMandatory(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHPDProviderCredential_credentialTypeMandatory");
		notif.setDescription("credentialType is a mandatory attribute of HPDProviderCredential object class. (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHPDProviderCredentialTemplate-constraint_hpd_addRequestHPDProviderCredential_credentialTypeMandatory");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-085"));
		diagnostic.add(notif);
	}

}
