package net.ihe.gazelle.hpd.validator.core.addrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;


 /**
  * class :        addRequestForHPDProviderMembershipTemplate
  * package :   addRequest
  * Template Class
  * Template identifier : 
  * Class of test : AddRequest
  * 
  */
public final class AddRequestForHPDProviderMembershipTemplateValidator{


    private AddRequestForHPDProviderMembershipTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestForHPDProviderMembership_hpdHasAProviderMandatory
	* hpdHasAProvider is a mandatory attribute of HPDProviderMembership object class. (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)
	*
	*/
	private static boolean _validateAddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdHasAProviderMandatory(net.ihe.gazelle.hpd.AddRequest aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator One: Iterate and check, if exactly one element fulfills the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hpdhasaprovider")) {
			        if (result1) {
			            // Found a second element.
			            result1 = false;
			            break;
			        } else {
			            // Found a first element.
			            result1 = true;
				    }
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestForHPDProviderMembership_hpdHasAnOrgMandatory
	* hpdHasAnOrg is a mandatory attribute of HPDProviderMembership object class. (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)
	*
	*/
	private static boolean _validateAddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdHasAnOrgMandatory(net.ihe.gazelle.hpd.AddRequest aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator One: Iterate and check, if exactly one element fulfills the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hpdhasanorg")) {
			        if (result1) {
			            // Found a second element.
			            result1 = false;
			            break;
			        } else {
			            // Found a first element.
			            result1 = true;
				    }
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestForHPDProviderMembership_hpdMemberIdMandatory
	* hpdMemberId is a mandatory attribute of HPDProviderMembership object class. (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)
	*
	*/
	private static boolean _validateAddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdMemberIdMandatory(net.ihe.gazelle.hpd.AddRequest aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator One: Iterate and check, if exactly one element fulfills the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hpdmemberid")) {
			        if (result1) {
			            // Found a second element.
			            result1 = false;
			            break;
			        } else {
			            // Found a first element.
			            result1 = true;
				    }
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : addRequestForHPDProviderMembershipTemplate
    * Verify if an element can be token as a Template of type addRequestForHPDProviderMembershipTemplate
	*
	*/
	private static boolean _isAddRequestForHPDProviderMembershipTemplateTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    java.util.ArrayList<String> result2;
			    result2 = new java.util.ArrayList<String>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (String anElement2 : anElement1.getValue()) {
			    	    if (anElement2.toLowerCase().equals("hpdprovidermembership")) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if ((anElement1.getName().toLowerCase().equals("objectclass") && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2) > new Integer(0)))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > new Integer(0));
		
				
	}
	/**
	* Validation of class-constraint : addRequestForHPDProviderMembershipTemplate
    * Verify if an element of type addRequestForHPDProviderMembershipTemplate can be validated by addRequestForHPDProviderMembershipTemplate
	*
	*/
	public static boolean _isAddRequestForHPDProviderMembershipTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		return _isAddRequestForHPDProviderMembershipTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   addRequestForHPDProviderMembershipTemplate
     * class ::  net.ihe.gazelle.hpd.AddRequest
     * 
     */
    public static void _validateAddRequestForHPDProviderMembershipTemplate(net.ihe.gazelle.hpd.AddRequest aClass, String location, List<Notification> diagnostic) {
		if (_isAddRequestForHPDProviderMembershipTemplate(aClass)){
			executeCons_AddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdHasAProviderMandatory(aClass, location, diagnostic);
			executeCons_AddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdHasAnOrgMandatory(aClass, location, diagnostic);
			executeCons_AddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdMemberIdMandatory(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdHasAProviderMandatory(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdHasAProviderMandatory(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestForHPDProviderMembership_hpdHasAProviderMandatory");
		notif.setDescription("hpdHasAProvider is a mandatory attribute of HPDProviderMembership object class. (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHPDProviderMembershipTemplate-constraint_hpd_addRequestForHPDProviderMembership_hpdHasAProviderMandatory");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-089"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdHasAnOrgMandatory(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdHasAnOrgMandatory(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestForHPDProviderMembership_hpdHasAnOrgMandatory");
		notif.setDescription("hpdHasAnOrg is a mandatory attribute of HPDProviderMembership object class. (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHPDProviderMembershipTemplate-constraint_hpd_addRequestForHPDProviderMembership_hpdHasAnOrgMandatory");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-090"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdMemberIdMandatory(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHPDProviderMembershipTemplate_Constraint_hpd_addRequestForHPDProviderMembership_hpdMemberIdMandatory(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestForHPDProviderMembership_hpdMemberIdMandatory");
		notif.setDescription("hpdMemberId is a mandatory attribute of HPDProviderMembership object class. (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHPDProviderMembershipTemplate-constraint_hpd_addRequestForHPDProviderMembership_hpdMemberIdMandatory");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-088"));
		diagnostic.add(notif);
	}

}
