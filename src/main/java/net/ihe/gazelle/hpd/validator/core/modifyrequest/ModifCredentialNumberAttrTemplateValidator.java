package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifCredentialNumberAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifCredentialNumberAttrTemplateValidator{


    private ModifCredentialNumberAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_credentialNumberSyntax
	* credentialNumber attribute follows the ISO21091 UID format: Issuing Authority OID: ID (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)
	*
	*/
	private static boolean _validateModifCredentialNumberAttrTemplate_Constraint_hpd_modifyRequest_credentialNumberSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "^[0-2](\\.(0|[1-9][0-9]*))*[:]{1}.+")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_credentialNumberValueCard
	* credentialNumber attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)
	*
	*/
	private static boolean _validateModifCredentialNumberAttrTemplate_Constraint_hpd_modifyRequest_credentialNumberValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifCredentialNumberAttrTemplate
    * Verify if an element can be token as a Template of type modifCredentialNumberAttrTemplate
	*
	*/
	private static boolean _isModifCredentialNumberAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("credentialnumber");
				
	}
	/**
	* Validation of class-constraint : modifCredentialNumberAttrTemplate
    * Verify if an element of type modifCredentialNumberAttrTemplate can be validated by modifCredentialNumberAttrTemplate
	*
	*/
	public static boolean _isModifCredentialNumberAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifCredentialNumberAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifCredentialNumberAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifCredentialNumberAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifCredentialNumberAttrTemplate(aClass)){
			executeCons_ModifCredentialNumberAttrTemplate_Constraint_hpd_modifyRequest_credentialNumberSyntax(aClass, location, diagnostic);
			executeCons_ModifCredentialNumberAttrTemplate_Constraint_hpd_modifyRequest_credentialNumberValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifCredentialNumberAttrTemplate_Constraint_hpd_modifyRequest_credentialNumberSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifCredentialNumberAttrTemplate_Constraint_hpd_modifyRequest_credentialNumberSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_credentialNumberSyntax");
		notif.setDescription("credentialNumber attribute follows the ISO21091 UID format: Issuing Authority OID: ID (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifCredentialNumberAttrTemplate-constraint_hpd_modifyRequest_credentialNumberSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-112"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifCredentialNumberAttrTemplate_Constraint_hpd_modifyRequest_credentialNumberValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifCredentialNumberAttrTemplate_Constraint_hpd_modifyRequest_credentialNumberValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_credentialNumberValueCard");
		notif.setDescription("credentialNumber attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-2: HPDProviderCredential Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifCredentialNumberAttrTemplate-constraint_hpd_modifyRequest_credentialNumberValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-111"));
		diagnostic.add(notif);
	}

}
