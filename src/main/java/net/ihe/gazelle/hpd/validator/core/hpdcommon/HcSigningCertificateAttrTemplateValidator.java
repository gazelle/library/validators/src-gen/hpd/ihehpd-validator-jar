package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        HcSigningCertificateAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HcSigningCertificateAttrTemplateValidator{


    private HcSigningCertificateAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_HcSigningCertificateSyntax
	* LDAP Syntax to be used for hcSigningCertificate attribute SHALL be Binary (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateHcSigningCertificateAttrTemplate_Constraint_hpd_common_HcSigningCertificateSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : HcSigningCertificateAttrTemplate
    * Verify if an element can be token as a Template of type HcSigningCertificateAttrTemplate
	*
	*/
	private static boolean _isHcSigningCertificateAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hcsigningcertificate");
				
	}
	/**
	* Validation of class-constraint : HcSigningCertificateAttrTemplate
    * Verify if an element of type HcSigningCertificateAttrTemplate can be validated by HcSigningCertificateAttrTemplate
	*
	*/
	public static boolean _isHcSigningCertificateAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHcSigningCertificateAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   HcSigningCertificateAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHcSigningCertificateAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHcSigningCertificateAttrTemplate(aClass)){
			executeCons_HcSigningCertificateAttrTemplate_Constraint_hpd_common_HcSigningCertificateSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HcSigningCertificateAttrTemplate_Constraint_hpd_common_HcSigningCertificateSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHcSigningCertificateAttrTemplate_Constraint_hpd_common_HcSigningCertificateSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_HcSigningCertificateSyntax");
		notif.setDescription("LDAP Syntax to be used for hcSigningCertificate attribute SHALL be Binary (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-HcSigningCertificateAttrTemplate-constraint_hpd_common_HcSigningCertificateSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-025"));
		diagnostic.add(notif);
	}

}
