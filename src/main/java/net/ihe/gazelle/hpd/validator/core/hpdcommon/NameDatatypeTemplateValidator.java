package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        nameDatatypeTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class NameDatatypeTemplateValidator{


    private NameDatatypeTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_hl7XPNdatatype
	* LDAP ‘name’ attributes marked with a language tag of “lang-x-ihe” shall be encoded using the HL7 XPN Data Type. UTF-8 shall be used for any characters outside ASCII. (see 3.24.5.2.3.1 Use of language tag and HL7 Name Data Type (XPN))
	*
	*/
	private static boolean _validateNameDatatypeTemplate_Constraint_hpd_common_hl7XPNdatatype(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    Boolean ifExpResult1;
			    
			    if (aClass.matches(anElement1.toLowerCase(), "^(lang-x-ihe){1}.*")) {
			        ifExpResult1 = aClass.matches(anElement1.toLowerCase(), "^(lang-x-ihe:){1}[^\\^]*(\\^[^\\^]*){1,13}");
			    } else {
			        ifExpResult1 = aClass.matches(anElement1.toLowerCase(), "*.");
			    }
		
			    if (!ifExpResult1) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : nameDatatypeTemplate
    * Verify if an element can be token as a Template of type nameDatatypeTemplate
	*
	*/
	private static boolean _isNameDatatypeTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.matches(aClass.getName(), ".*(;lang-x-ihe){1}");
				
	}
	/**
	* Validation of class-constraint : nameDatatypeTemplate
    * Verify if an element of type nameDatatypeTemplate can be validated by nameDatatypeTemplate
	*
	*/
	public static boolean _isNameDatatypeTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isNameDatatypeTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   nameDatatypeTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateNameDatatypeTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isNameDatatypeTemplate(aClass)){
			executeCons_NameDatatypeTemplate_Constraint_hpd_common_hl7XPNdatatype(aClass, location, diagnostic);
		}
	}

	private static void executeCons_NameDatatypeTemplate_Constraint_hpd_common_hl7XPNdatatype(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateNameDatatypeTemplate_Constraint_hpd_common_hl7XPNdatatype(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_hl7XPNdatatype");
		notif.setDescription("LDAP ‘name’ attributes marked with a language tag of “lang-x-ihe” shall be encoded using the HL7 XPN Data Type. UTF-8 shall be used for any characters outside ASCII. (see 3.24.5.2.3.1 Use of language tag and HL7 Name Data Type (XPN))");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-nameDatatypeTemplate-constraint_hpd_common_hl7XPNdatatype");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-055"));
		diagnostic.add(notif);
	}

}
