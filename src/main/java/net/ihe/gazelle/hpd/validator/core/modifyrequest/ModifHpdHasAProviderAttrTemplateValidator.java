package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHpdHasAProviderAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHpdHasAProviderAttrTemplateValidator{


    private ModifHpdHasAProviderAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdHasAProviderSyntax
	* hpdHasAProvider attribute SHALL be formatted as a distinguished name (DN) (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership MandatoryAttributes)
	*
	*/
	private static boolean _validateModifHpdHasAProviderAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAProviderSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdHasAProviderValueCard
	* hpdHasAProvider attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)
	*
	*/
	private static boolean _validateModifHpdHasAProviderAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAProviderValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHpdHasAProviderAttrTemplate
    * Verify if an element can be token as a Template of type modifHpdHasAProviderAttrTemplate
	*
	*/
	private static boolean _isModifHpdHasAProviderAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hpdhasaprovider");
				
	}
	/**
	* Validation of class-constraint : modifHpdHasAProviderAttrTemplate
    * Verify if an element of type modifHpdHasAProviderAttrTemplate can be validated by modifHpdHasAProviderAttrTemplate
	*
	*/
	public static boolean _isModifHpdHasAProviderAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHpdHasAProviderAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHpdHasAProviderAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHpdHasAProviderAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHpdHasAProviderAttrTemplate(aClass)){
			executeCons_ModifHpdHasAProviderAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAProviderSyntax(aClass, location, diagnostic);
			executeCons_ModifHpdHasAProviderAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAProviderValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHpdHasAProviderAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAProviderSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdHasAProviderAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAProviderSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdHasAProviderSyntax");
		notif.setDescription("hpdHasAProvider attribute SHALL be formatted as a distinguished name (DN) (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership MandatoryAttributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdHasAProviderAttrTemplate-constraint_hpd_modifyRequest_hpdHasAProviderSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-139"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifHpdHasAProviderAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAProviderValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdHasAProviderAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAProviderValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdHasAProviderValueCard");
		notif.setDescription("hpdHasAProvider attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdHasAProviderAttrTemplate-constraint_hpd_modifyRequest_hpdHasAProviderValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-140"));
		diagnostic.add(notif);
	}

}
