package net.ihe.gazelle.hpd.validator.core.searchresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;


public class SEARCHRESPONSEPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public SEARCHRESPONSEPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hpd.SearchResponse){
			net.ihe.gazelle.hpd.SearchResponse aClass = ( net.ihe.gazelle.hpd.SearchResponse)obj;
			SearchResponseSpecValidator._validateSearchResponseSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hpd.SearchResultEntry){
			net.ihe.gazelle.hpd.SearchResultEntry aClass = ( net.ihe.gazelle.hpd.SearchResultEntry)obj;
			SearchResultEntrySpecValidator._validateSearchResultEntrySpec(aClass, location, diagnostic);
			CredentialTypeAttrForHCProfessionalTemplateValidator._validateCredentialTypeAttrForHCProfessionalTemplate(aClass, location, diagnostic);
			CredentialTypeAttrForHCRegulatedOrganizationTemplateValidator._validateCredentialTypeAttrForHCRegulatedOrganizationTemplate(aClass, location, diagnostic);
			HpdProviderStatusAttrForHCProfessionalTemplateValidator._validateHpdProviderStatusAttrForHCProfessionalTemplate(aClass, location, diagnostic);
			HpdProviderStatusAttrForHCRegulatedOrganizationTemplateValidator._validateHpdProviderStatusAttrForHCRegulatedOrganizationTemplate(aClass, location, diagnostic);
		}
	
	}

}

