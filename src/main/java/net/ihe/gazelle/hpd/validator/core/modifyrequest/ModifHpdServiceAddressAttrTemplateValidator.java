package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHpdServiceAddressAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHpdServiceAddressAttrTemplateValidator{


    private ModifHpdServiceAddressAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdServiceAddressValueCard
	* hpdServiceAddress attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)
	*
	*/
	private static boolean _validateModifHpdServiceAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdServiceAddressValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHpdServiceAddressAttrTemplate
    * Verify if an element can be token as a Template of type modifHpdServiceAddressAttrTemplate
	*
	*/
	private static boolean _isModifHpdServiceAddressAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hpdserviceaddress");
				
	}
	/**
	* Validation of class-constraint : modifHpdServiceAddressAttrTemplate
    * Verify if an element of type modifHpdServiceAddressAttrTemplate can be validated by modifHpdServiceAddressAttrTemplate
	*
	*/
	public static boolean _isModifHpdServiceAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHpdServiceAddressAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHpdServiceAddressAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHpdServiceAddressAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHpdServiceAddressAttrTemplate(aClass)){
			executeCons_ModifHpdServiceAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdServiceAddressValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHpdServiceAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdServiceAddressValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdServiceAddressAttrTemplate_Constraint_hpd_modifyRequest_hpdServiceAddressValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdServiceAddressValueCard");
		notif.setDescription("hpdServiceAddress attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdServiceAddressAttrTemplate-constraint_hpd_modifyRequest_hpdServiceAddressValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-144"));
		diagnostic.add(notif);
	}

}
