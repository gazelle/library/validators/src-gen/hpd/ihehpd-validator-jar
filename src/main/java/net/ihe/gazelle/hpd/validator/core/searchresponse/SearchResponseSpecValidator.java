package net.ihe.gazelle.hpd.validator.core.searchresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.SearchResultEntry;


 /**
  * class :        SearchResponseSpec
  * package :   searchResponse
  * Constraint Spec Class
  * class of test : SearchResponse
  * 
  */
public final class SearchResponseSpecValidator{


    private SearchResponseSpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_searchResponse_requestIDCard
	* The Provider Information Query Response shall contain the requestID to associate the response to the Provider Information Query Request. (see section 3.58.4.2.2: Message Semantics)
	*
	*/
	private static boolean _validateSearchResponseSpec_Constraint_hpd_searchResponse_requestIDCard(net.ihe.gazelle.hpd.SearchResponse aClass){
		return !(aClass.getRequestID() == null);
		
	}

	/**
	* Validation of class-constraint : SearchResponseSpec
    * Verify if an element of type SearchResponseSpec can be validated by SearchResponseSpec
	*
	*/
	public static boolean _isSearchResponseSpec(net.ihe.gazelle.hpd.SearchResponse aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   SearchResponseSpec
     * class ::  net.ihe.gazelle.hpd.SearchResponse
     * 
     */
    public static void _validateSearchResponseSpec(net.ihe.gazelle.hpd.SearchResponse aClass, String location, List<Notification> diagnostic) {
		if (_isSearchResponseSpec(aClass)){
			executeCons_SearchResponseSpec_Constraint_hpd_searchResponse_requestIDCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_SearchResponseSpec_Constraint_hpd_searchResponse_requestIDCard(net.ihe.gazelle.hpd.SearchResponse aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSearchResponseSpec_Constraint_hpd_searchResponse_requestIDCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_searchResponse_requestIDCard");
		notif.setDescription("The Provider Information Query Response shall contain the requestID to associate the response to the Provider Information Query Request. (see section 3.58.4.2.2: Message Semantics)");
		notif.setLocation(location);
		notif.setIdentifiant("searchResponse-SearchResponseSpec-constraint_hpd_searchResponse_requestIDCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-058"));
		diagnostic.add(notif);
	}

}
