package net.ihe.gazelle.hpd.validator.core.searchrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





public class SEARCHREQUESTPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public SEARCHREQUESTPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hpd.SearchRequest){
			net.ihe.gazelle.hpd.SearchRequest aClass = ( net.ihe.gazelle.hpd.SearchRequest)obj;
			SearchRequestSpecValidator._validateSearchRequestSpec(aClass, location, diagnostic);
		}
	
	}

}

