package net.ihe.gazelle.hpd.validator.core.addrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;


 /**
  * class :        addRequestSpec
  * package :   addRequest
  * Constraint Spec Class
  * class of test : AddRequest
  * 
  */
public final class AddRequestSpecValidator{


    private AddRequestSpecValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_addRequest_createTimestampCard
	* createTimestamp is an operation attribute that LDAP directory server maintains to capture the time when an entry was created
	*
	*/
	private static boolean _validateAddRequestSpec_Constraint_hpd_addRequest_createTimestampCard(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("createtimestamp")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(0));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequest_dnSyntax
	* dn attribute SHALL be formatted as a Distinguish name string.
	*
	*/
	private static boolean _validateAddRequestSpec_Constraint_hpd_addRequest_dnSyntax(net.ihe.gazelle.hpd.AddRequest aClass){
		return aClass.matches(((String) aClass.getDn()), "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$");
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequest_dnValidOu
	* Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization,ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)               
	*
	*/
	private static boolean _validateAddRequestSpec_Constraint_hpd_addRequest_dnValidOu(net.ihe.gazelle.hpd.AddRequest aClass){
		return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=(hcprofessional|hcregulatedorganization|relationship|hpdcredential)){1}.*");
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequest_modifyTimestampCard
	* modifyTimestamp is an operation attribute that LDAP directory server maintains to capture the time when an entry was modified
	*
	*/
	private static boolean _validateAddRequestSpec_Constraint_hpd_addRequest_modifyTimestampCard(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("modifytimestamp")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(0));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequest_objectClassRequired
	* objectClass attribute is required in each entry (see RFC 2256 section 7.1 top)
	*
	*/
	private static boolean _validateAddRequestSpec_Constraint_hpd_addRequest_objectClassRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("objectclass")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of class-constraint : addRequestSpec
    * Verify if an element of type addRequestSpec can be validated by addRequestSpec
	*
	*/
	public static boolean _isAddRequestSpec(net.ihe.gazelle.hpd.AddRequest aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   addRequestSpec
     * class ::  net.ihe.gazelle.hpd.AddRequest
     * 
     */
    public static void _validateAddRequestSpec(net.ihe.gazelle.hpd.AddRequest aClass, String location, List<Notification> diagnostic) {
		if (_isAddRequestSpec(aClass)){
			executeCons_AddRequestSpec_Constraint_hpd_addRequest_createTimestampCard(aClass, location, diagnostic);
			executeCons_AddRequestSpec_Constraint_hpd_addRequest_dnSyntax(aClass, location, diagnostic);
			executeCons_AddRequestSpec_Constraint_hpd_addRequest_dnValidOu(aClass, location, diagnostic);
			executeCons_AddRequestSpec_Constraint_hpd_addRequest_modifyTimestampCard(aClass, location, diagnostic);
			executeCons_AddRequestSpec_Constraint_hpd_addRequest_objectClassRequired(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AddRequestSpec_Constraint_hpd_addRequest_createTimestampCard(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestSpec_Constraint_hpd_addRequest_createTimestampCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequest_createTimestampCard");
		notif.setDescription("createTimestamp is an operation attribute that LDAP directory server maintains to capture the time when an entry was created");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestSpec-constraint_hpd_addRequest_createTimestampCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-080"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestSpec_Constraint_hpd_addRequest_dnSyntax(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestSpec_Constraint_hpd_addRequest_dnSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequest_dnSyntax");
		notif.setDescription("dn attribute SHALL be formatted as a Distinguish name string.");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestSpec-constraint_hpd_addRequest_dnSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-077"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestSpec_Constraint_hpd_addRequest_dnValidOu(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestSpec_Constraint_hpd_addRequest_dnValidOu(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequest_dnValidOu");
		notif.setDescription("Nodes that are subordinates to dc=HPD are ou=HCProfessional, ou=HCRegulatedOrganization,ou=HPDCredential and ou=Relationship (see                   section 3.58.4.1.2.1 HPD Schema Structure)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestSpec-constraint_hpd_addRequest_dnValidOu");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-078"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestSpec_Constraint_hpd_addRequest_modifyTimestampCard(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestSpec_Constraint_hpd_addRequest_modifyTimestampCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequest_modifyTimestampCard");
		notif.setDescription("modifyTimestamp is an operation attribute that LDAP directory server maintains to capture the time when an entry was modified");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestSpec-constraint_hpd_addRequest_modifyTimestampCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-081"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestSpec_Constraint_hpd_addRequest_objectClassRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestSpec_Constraint_hpd_addRequest_objectClassRequired(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequest_objectClassRequired");
		notif.setDescription("objectClass attribute is required in each entry (see RFC 2256 section 7.1 top)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestSpec-constraint_hpd_addRequest_objectClassRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-079"));
		diagnostic.add(notif);
	}

}
