package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifCredentialRenewalDateAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifCredentialRenewalDateAttrTemplateValidator{


    private ModifCredentialRenewalDateAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_credentialRenewalDateValueCard
	* credentialRenewalDate attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-3: HPDProviderCredential Optional Attributes)
	*
	*/
	private static boolean _validateModifCredentialRenewalDateAttrTemplate_Constraint_hpd_modifyRequest_credentialRenewalDateValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifCredentialRenewalDateAttrTemplate
    * Verify if an element can be token as a Template of type modifCredentialRenewalDateAttrTemplate
	*
	*/
	private static boolean _isModifCredentialRenewalDateAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("credentialrenewaldate");
				
	}
	/**
	* Validation of class-constraint : modifCredentialRenewalDateAttrTemplate
    * Verify if an element of type modifCredentialRenewalDateAttrTemplate can be validated by modifCredentialRenewalDateAttrTemplate
	*
	*/
	public static boolean _isModifCredentialRenewalDateAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifCredentialRenewalDateAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifCredentialRenewalDateAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifCredentialRenewalDateAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifCredentialRenewalDateAttrTemplate(aClass)){
			executeCons_ModifCredentialRenewalDateAttrTemplate_Constraint_hpd_modifyRequest_credentialRenewalDateValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifCredentialRenewalDateAttrTemplate_Constraint_hpd_modifyRequest_credentialRenewalDateValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifCredentialRenewalDateAttrTemplate_Constraint_hpd_modifyRequest_credentialRenewalDateValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_credentialRenewalDateValueCard");
		notif.setDescription("credentialRenewalDate attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-3: HPDProviderCredential Optional Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifCredentialRenewalDateAttrTemplate-constraint_hpd_modifyRequest_credentialRenewalDateValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-115"));
		diagnostic.add(notif);
	}

}
