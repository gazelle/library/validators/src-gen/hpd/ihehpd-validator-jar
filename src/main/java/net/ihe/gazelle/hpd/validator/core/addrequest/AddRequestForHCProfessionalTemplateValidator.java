package net.ihe.gazelle.hpd.validator.core.addrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;


 /**
  * class :        addRequestForHCProfessionalTemplate
  * package :   addRequest
  * Template Class
  * Template identifier : 
  * Class of test : AddRequest
  * 
  */
public final class AddRequestForHCProfessionalTemplateValidator{


    private AddRequestForHCProfessionalTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHCProfessional_HcIdentifierRequired
	* When the addRequest describes an Individual Provider, the HcIdentifier attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcIdentifierRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hcidentifier")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHCProfessional_HcProfessionRequired
	* When the addRequest describes an Individual Provider, the HcProfession attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcProfessionRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hcprofession")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHCProfessional_HcRegistrationStatusRequired
	* When the addRequest describes an Individual Provider, the HcRegistrationStatus attribute is required (see ISO 21091 section 8.1.3 Healthcare Professional)
	*
	*/
	private static boolean _validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcRegistrationStatusRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hcregistrationstatus")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHCProfessional_cnRequired
	* When the addRequest describes an Individual Provider, the cn attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_cnRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.matches(anElement1.getName().toLowerCase(), "cn((;lang\\-){1}.+)?")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHCProfessional_displayNameRequired
	* When the addRequest describes an Individual Provider, the displayName attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_displayNameRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.matches(anElement1.getName().toLowerCase(), "displayname((;lang\\-){1}.+)?")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHCProfessional_snRequired
	* When the addRequest describes an Individual Provider, the sn attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_snRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.matches(anElement1.getName().toLowerCase(), "sn((;lang\\-){1}.+)?")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestHCProfessional_uidRequired
	* When the addRequest describes an Individual Provider, the uid attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_uidRequired(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("uid")) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of template-constraint by a constraint : addRequestForHCProfessionalTemplate
    * Verify if an element can be token as a Template of type addRequestForHCProfessionalTemplate
	*
	*/
	private static boolean _isAddRequestForHCProfessionalTemplateTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=hcprofessional){1}.*");
				
	}
	/**
	* Validation of class-constraint : addRequestForHCProfessionalTemplate
    * Verify if an element of type addRequestForHCProfessionalTemplate can be validated by addRequestForHCProfessionalTemplate
	*
	*/
	public static boolean _isAddRequestForHCProfessionalTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		return _isAddRequestForHCProfessionalTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   addRequestForHCProfessionalTemplate
     * class ::  net.ihe.gazelle.hpd.AddRequest
     * 
     */
    public static void _validateAddRequestForHCProfessionalTemplate(net.ihe.gazelle.hpd.AddRequest aClass, String location, List<Notification> diagnostic) {
		if (_isAddRequestForHCProfessionalTemplate(aClass)){
			executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcIdentifierRequired(aClass, location, diagnostic);
			executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcProfessionRequired(aClass, location, diagnostic);
			executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcRegistrationStatusRequired(aClass, location, diagnostic);
			executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_cnRequired(aClass, location, diagnostic);
			executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_displayNameRequired(aClass, location, diagnostic);
			executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_snRequired(aClass, location, diagnostic);
			executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_uidRequired(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcIdentifierRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcIdentifierRequired(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHCProfessional_HcIdentifierRequired");
		notif.setDescription("When the addRequest describes an Individual Provider, the HcIdentifier attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHCProfessionalTemplate-constraint_hpd_addRequestHCProfessional_HcIdentifierRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-066"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcProfessionRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcProfessionRequired(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHCProfessional_HcProfessionRequired");
		notif.setDescription("When the addRequest describes an Individual Provider, the HcProfession attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHCProfessionalTemplate-constraint_hpd_addRequestHCProfessional_HcProfessionRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-067"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcRegistrationStatusRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_HcRegistrationStatusRequired(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Warning();
		}
		notif.setTest("constraint_hpd_addRequestHCProfessional_HcRegistrationStatusRequired");
		notif.setDescription("When the addRequest describes an Individual Provider, the HcRegistrationStatus attribute is required (see ISO 21091 section 8.1.3 Healthcare Professional)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHCProfessionalTemplate-constraint_hpd_addRequestHCProfessional_HcRegistrationStatusRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-071"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_cnRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_cnRequired(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHCProfessional_cnRequired");
		notif.setDescription("When the addRequest describes an Individual Provider, the cn attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHCProfessionalTemplate-constraint_hpd_addRequestHCProfessional_cnRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-070"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_displayNameRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_displayNameRequired(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHCProfessional_displayNameRequired");
		notif.setDescription("When the addRequest describes an Individual Provider, the displayName attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHCProfessionalTemplate-constraint_hpd_addRequestHCProfessional_displayNameRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-068"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_snRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_snRequired(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHCProfessional_snRequired");
		notif.setDescription("When the addRequest describes an Individual Provider, the sn attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHCProfessionalTemplate-constraint_hpd_addRequestHCProfessional_snRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-069"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_uidRequired(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHCProfessionalTemplate_Constraint_hpd_addRequestHCProfessional_uidRequired(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestHCProfessional_uidRequired");
		notif.setDescription("When the addRequest describes an Individual Provider, the uid attribute is required (see 3.59.1 and Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHCProfessionalTemplate-constraint_hpd_addRequestHCProfessional_uidRequired");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-065"));
		diagnostic.add(notif);
	}

}
