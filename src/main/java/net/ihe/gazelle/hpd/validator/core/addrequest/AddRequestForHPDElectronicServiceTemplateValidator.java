package net.ihe.gazelle.hpd.validator.core.addrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;


 /**
  * class :        addRequestForHPDElectronicServiceTemplate
  * package :   addRequest
  * Template Class
  * Template identifier : 
  * Class of test : AddRequest
  * 
  */
public final class AddRequestForHPDElectronicServiceTemplateValidator{


    private AddRequestForHPDElectronicServiceTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestForHPDElectronicService_hpdServiceAddressMandatory
	* hpdServiceAddress is a mandatory attribute of HPDElecronicService object class. (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)
	*
	*/
	private static boolean _validateAddRequestForHPDElectronicServiceTemplate_Constraint_hpd_addRequestForHPDElectronicService_hpdServiceAddressMandatory(net.ihe.gazelle.hpd.AddRequest aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator One: Iterate and check, if exactly one element fulfills the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hpdserviceaddress")) {
			        if (result1) {
			            // Found a second element.
			            result1 = false;
			            break;
			        } else {
			            // Found a first element.
			            result1 = true;
				    }
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_addRequestForHPDElectronicService_hpdServiceIdMandatory
	* hpdServiceId is a mandatory attribute of HDPElectronicService object class. (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)
	*
	*/
	private static boolean _validateAddRequestForHPDElectronicServiceTemplate_Constraint_hpd_addRequestForHPDElectronicService_hpdServiceIdMandatory(net.ihe.gazelle.hpd.AddRequest aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator One: Iterate and check, if exactly one element fulfills the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    if (anElement1.getName().toLowerCase().equals("hpdserviceid")) {
			        if (result1) {
			            // Found a second element.
			            result1 = false;
			            break;
			        } else {
			            // Found a first element.
			            result1 = true;
				    }
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : addRequestForHPDElectronicServiceTemplate
    * Verify if an element can be token as a Template of type addRequestForHPDElectronicServiceTemplate
	*
	*/
	private static boolean _isAddRequestForHPDElectronicServiceTemplateTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		java.util.ArrayList<DsmlAttr> result1;
		result1 = new java.util.ArrayList<DsmlAttr>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (DsmlAttr anElement1 : aClass.getAttr()) {
			    java.util.ArrayList<String> result2;
			    result2 = new java.util.ArrayList<String>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (String anElement2 : anElement1.getValue()) {
			    	    if (anElement2.toLowerCase().equals("hpdelectronicservice")) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if ((anElement1.getName().toLowerCase().equals("objectclass") && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2) > new Integer(0)))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > new Integer(0));
		
				
	}
	/**
	* Validation of class-constraint : addRequestForHPDElectronicServiceTemplate
    * Verify if an element of type addRequestForHPDElectronicServiceTemplate can be validated by addRequestForHPDElectronicServiceTemplate
	*
	*/
	public static boolean _isAddRequestForHPDElectronicServiceTemplate(net.ihe.gazelle.hpd.AddRequest aClass){
		return _isAddRequestForHPDElectronicServiceTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   addRequestForHPDElectronicServiceTemplate
     * class ::  net.ihe.gazelle.hpd.AddRequest
     * 
     */
    public static void _validateAddRequestForHPDElectronicServiceTemplate(net.ihe.gazelle.hpd.AddRequest aClass, String location, List<Notification> diagnostic) {
		if (_isAddRequestForHPDElectronicServiceTemplate(aClass)){
			executeCons_AddRequestForHPDElectronicServiceTemplate_Constraint_hpd_addRequestForHPDElectronicService_hpdServiceAddressMandatory(aClass, location, diagnostic);
			executeCons_AddRequestForHPDElectronicServiceTemplate_Constraint_hpd_addRequestForHPDElectronicService_hpdServiceIdMandatory(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AddRequestForHPDElectronicServiceTemplate_Constraint_hpd_addRequestForHPDElectronicService_hpdServiceAddressMandatory(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHPDElectronicServiceTemplate_Constraint_hpd_addRequestForHPDElectronicService_hpdServiceAddressMandatory(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestForHPDElectronicService_hpdServiceAddressMandatory");
		notif.setDescription("hpdServiceAddress is a mandatory attribute of HPDElecronicService object class. (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHPDElectronicServiceTemplate-constraint_hpd_addRequestForHPDElectronicService_hpdServiceAddressMandatory");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-092"));
		diagnostic.add(notif);
	}

	private static void executeCons_AddRequestForHPDElectronicServiceTemplate_Constraint_hpd_addRequestForHPDElectronicService_hpdServiceIdMandatory(net.ihe.gazelle.hpd.AddRequest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAddRequestForHPDElectronicServiceTemplate_Constraint_hpd_addRequestForHPDElectronicService_hpdServiceIdMandatory(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_addRequestForHPDElectronicService_hpdServiceIdMandatory");
		notif.setDescription("hpdServiceId is a mandatory attribute of HDPElectronicService object class. (see Table 3.58.4.1.2.2.1-6: HPDElectronicService Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("addRequest-addRequestForHPDElectronicServiceTemplate-constraint_hpd_addRequestForHPDElectronicService_hpdServiceIdMandatory");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-091"));
		diagnostic.add(notif);
	}

}
