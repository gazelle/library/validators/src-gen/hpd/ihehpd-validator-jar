package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifGenderAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifGenderAttrTemplateValidator{


    private ModifGenderAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_genderValue
	* gender attribute SHALL follow the Natural Person auxilary class as defined in RFC 2985, that means that only Male (M or m) and Female (F or f) values are allowed. (see Table 3.58.4.1.2.2.2-1)
	*
	*/
	private static boolean _validateModifGenderAttrTemplate_Constraint_hpd_modifyRequest_genderValue(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!(((anElement1.equals("M") || anElement1.equals("m")) || anElement1.equals("F")) || anElement1.equals("f"))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_genderValueCard
	* gender attribute SHALL be single valued (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateModifGenderAttrTemplate_Constraint_hpd_modifyRequest_genderValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifGenderAttrTemplate
    * Verify if an element can be token as a Template of type modifGenderAttrTemplate
	*
	*/
	private static boolean _isModifGenderAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("gender");
				
	}
	/**
	* Validation of class-constraint : modifGenderAttrTemplate
    * Verify if an element of type modifGenderAttrTemplate can be validated by modifGenderAttrTemplate
	*
	*/
	public static boolean _isModifGenderAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifGenderAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifGenderAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifGenderAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifGenderAttrTemplate(aClass)){
			executeCons_ModifGenderAttrTemplate_Constraint_hpd_modifyRequest_genderValue(aClass, location, diagnostic);
			executeCons_ModifGenderAttrTemplate_Constraint_hpd_modifyRequest_genderValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifGenderAttrTemplate_Constraint_hpd_modifyRequest_genderValue(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifGenderAttrTemplate_Constraint_hpd_modifyRequest_genderValue(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_genderValue");
		notif.setDescription("gender attribute SHALL follow the Natural Person auxilary class as defined in RFC 2985, that means that only Male (M or m) and Female (F or f) values are allowed. (see Table 3.58.4.1.2.2.2-1)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifGenderAttrTemplate-constraint_hpd_modifyRequest_genderValue");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-105"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifGenderAttrTemplate_Constraint_hpd_modifyRequest_genderValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifGenderAttrTemplate_Constraint_hpd_modifyRequest_genderValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_genderValueCard");
		notif.setDescription("gender attribute SHALL be single valued (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifGenderAttrTemplate-constraint_hpd_modifyRequest_genderValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-104"));
		diagnostic.add(notif);
	}

}
