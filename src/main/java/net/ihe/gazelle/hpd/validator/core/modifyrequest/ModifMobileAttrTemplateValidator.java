package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifMobileAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifMobileAttrTemplateValidator{


    private ModifMobileAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_mobileSyntax
	* mobile attribute SHALL be representing using the E.123 notation. (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping) 
	*
	*/
	private static boolean _validateModifMobileAttrTemplate_Constraint_hpd_modifyRequest_mobileSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "((\\+|[0-9]*|\\([0-9]+\\))[\\s\\-\\.0-9]*/?)*")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : modifMobileAttrTemplate
    * Verify if an element can be token as a Template of type modifMobileAttrTemplate
	*
	*/
	private static boolean _isModifMobileAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("mobile");
				
	}
	/**
	* Validation of class-constraint : modifMobileAttrTemplate
    * Verify if an element of type modifMobileAttrTemplate can be validated by modifMobileAttrTemplate
	*
	*/
	public static boolean _isModifMobileAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifMobileAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifMobileAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifMobileAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifMobileAttrTemplate(aClass)){
			executeCons_ModifMobileAttrTemplate_Constraint_hpd_modifyRequest_mobileSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifMobileAttrTemplate_Constraint_hpd_modifyRequest_mobileSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifMobileAttrTemplate_Constraint_hpd_modifyRequest_mobileSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_mobileSyntax");
		notif.setDescription("mobile attribute SHALL be representing using the E.123 notation. (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifMobileAttrTemplate-constraint_hpd_modifyRequest_mobileSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-124"));
		diagnostic.add(notif);
	}

}
