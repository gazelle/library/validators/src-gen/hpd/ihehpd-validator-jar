package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        hpdProviderStatusAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HpdProviderStatusAttrTemplateValidator{


    private HpdProviderStatusAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_hpdProviderStatusValueCard
	* hpdProviderStatus attribute SHALL be single valued (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)
	*
	*/
	private static boolean _validateHpdProviderStatusAttrTemplate_Constraint_hpd_common_hpdProviderStatusValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : hpdProviderStatusAttrTemplate
    * Verify if an element can be token as a Template of type hpdProviderStatusAttrTemplate
	*
	*/
	private static boolean _isHpdProviderStatusAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hpdproviderstatus");
				
	}
	/**
	* Validation of class-constraint : hpdProviderStatusAttrTemplate
    * Verify if an element of type hpdProviderStatusAttrTemplate can be validated by hpdProviderStatusAttrTemplate
	*
	*/
	public static boolean _isHpdProviderStatusAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHpdProviderStatusAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   hpdProviderStatusAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHpdProviderStatusAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHpdProviderStatusAttrTemplate(aClass)){
			executeCons_HpdProviderStatusAttrTemplate_Constraint_hpd_common_hpdProviderStatusValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HpdProviderStatusAttrTemplate_Constraint_hpd_common_hpdProviderStatusValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHpdProviderStatusAttrTemplate_Constraint_hpd_common_hpdProviderStatusValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_hpdProviderStatusValueCard");
		notif.setDescription("hpdProviderStatus attribute SHALL be single valued (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-hpdProviderStatusAttrTemplate-constraint_hpd_common_hpdProviderStatusValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-001"));
		diagnostic.add(notif);
	}

}
