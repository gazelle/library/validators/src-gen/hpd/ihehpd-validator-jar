package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        credentialRenewalDateAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class CredentialRenewalDateAttrTemplateValidator{


    private CredentialRenewalDateAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_credentialRenewalDateValueCard
	* credentialRenewalDate attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-3: HPDProviderCredential Optional Attributes)
	*
	*/
	private static boolean _validateCredentialRenewalDateAttrTemplate_Constraint_hpd_common_credentialRenewalDateValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : credentialRenewalDateAttrTemplate
    * Verify if an element can be token as a Template of type credentialRenewalDateAttrTemplate
	*
	*/
	private static boolean _isCredentialRenewalDateAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("credentialrenewaldate");
				
	}
	/**
	* Validation of class-constraint : credentialRenewalDateAttrTemplate
    * Verify if an element of type credentialRenewalDateAttrTemplate can be validated by credentialRenewalDateAttrTemplate
	*
	*/
	public static boolean _isCredentialRenewalDateAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isCredentialRenewalDateAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   credentialRenewalDateAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateCredentialRenewalDateAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isCredentialRenewalDateAttrTemplate(aClass)){
			executeCons_CredentialRenewalDateAttrTemplate_Constraint_hpd_common_credentialRenewalDateValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CredentialRenewalDateAttrTemplate_Constraint_hpd_common_credentialRenewalDateValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCredentialRenewalDateAttrTemplate_Constraint_hpd_common_credentialRenewalDateValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_credentialRenewalDateValueCard");
		notif.setDescription("credentialRenewalDate attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-3: HPDProviderCredential Optional Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-credentialRenewalDateAttrTemplate-constraint_hpd_common_credentialRenewalDateValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-019"));
		diagnostic.add(notif);
	}

}
