package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifCredentialStatusAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifCredentialStatusAttrTemplateValidator{


    private ModifCredentialStatusAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_credentialStatusValue
	* Valid values for credentialStatus attribute are Active, Inactive, Revoked and Suspended (see Table 3.58.4.1.2.3-1: Status Code Category Values)
	*
	*/
	private static boolean _validateModifCredentialStatusAttrTemplate_Constraint_hpd_modifyRequest_credentialStatusValue(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!(((anElement1.toLowerCase().equals("active") || anElement1.toLowerCase().equals("inactive")) || anElement1.toLowerCase().equals("revoked")) || anElement1.toLowerCase().equals("suspended"))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_credentialStatusValueCard
	* credentialStatus attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-3: HPDProviderCredential Optional Attributes)
	*
	*/
	private static boolean _validateModifCredentialStatusAttrTemplate_Constraint_hpd_modifyRequest_credentialStatusValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifCredentialStatusAttrTemplate
    * Verify if an element can be token as a Template of type modifCredentialStatusAttrTemplate
	*
	*/
	private static boolean _isModifCredentialStatusAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("credentialstatus");
				
	}
	/**
	* Validation of class-constraint : modifCredentialStatusAttrTemplate
    * Verify if an element of type modifCredentialStatusAttrTemplate can be validated by modifCredentialStatusAttrTemplate
	*
	*/
	public static boolean _isModifCredentialStatusAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifCredentialStatusAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifCredentialStatusAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifCredentialStatusAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifCredentialStatusAttrTemplate(aClass)){
			executeCons_ModifCredentialStatusAttrTemplate_Constraint_hpd_modifyRequest_credentialStatusValue(aClass, location, diagnostic);
			executeCons_ModifCredentialStatusAttrTemplate_Constraint_hpd_modifyRequest_credentialStatusValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifCredentialStatusAttrTemplate_Constraint_hpd_modifyRequest_credentialStatusValue(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifCredentialStatusAttrTemplate_Constraint_hpd_modifyRequest_credentialStatusValue(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_credentialStatusValue");
		notif.setDescription("Valid values for credentialStatus attribute are Active, Inactive, Revoked and Suspended (see Table 3.58.4.1.2.3-1: Status Code Category Values)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifCredentialStatusAttrTemplate-constraint_hpd_modifyRequest_credentialStatusValue");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-117"));
		diagnostic.add(notif);
	}

	private static void executeCons_ModifCredentialStatusAttrTemplate_Constraint_hpd_modifyRequest_credentialStatusValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifCredentialStatusAttrTemplate_Constraint_hpd_modifyRequest_credentialStatusValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_credentialStatusValueCard");
		notif.setDescription("credentialStatus attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-3: HPDProviderCredential Optional Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifCredentialStatusAttrTemplate-constraint_hpd_modifyRequest_credentialStatusValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-116"));
		diagnostic.add(notif);
	}

}
