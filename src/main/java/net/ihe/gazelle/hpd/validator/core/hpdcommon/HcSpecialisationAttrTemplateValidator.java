package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        HcSpecialisationAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class HcSpecialisationAttrTemplateValidator{


    private HcSpecialisationAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_HcSpecialisationSyntax
	* HcSpecialisation attribute SHALL be formatted as follows : Issuing Authority:Code System:Code:CodeDisplayName (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateHcSpecialisationAttrTemplate_Constraint_hpd_common_HcSpecialisationSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, ".+(:.+){2}(:.+)?")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : HcSpecialisationAttrTemplate
    * Verify if an element can be token as a Template of type HcSpecialisationAttrTemplate
	*
	*/
	private static boolean _isHcSpecialisationAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("hcspecialisation");
				
	}
	/**
	* Validation of class-constraint : HcSpecialisationAttrTemplate
    * Verify if an element of type HcSpecialisationAttrTemplate can be validated by HcSpecialisationAttrTemplate
	*
	*/
	public static boolean _isHcSpecialisationAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isHcSpecialisationAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   HcSpecialisationAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateHcSpecialisationAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isHcSpecialisationAttrTemplate(aClass)){
			executeCons_HcSpecialisationAttrTemplate_Constraint_hpd_common_HcSpecialisationSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HcSpecialisationAttrTemplate_Constraint_hpd_common_HcSpecialisationSyntax(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHcSpecialisationAttrTemplate_Constraint_hpd_common_HcSpecialisationSyntax(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Warning();
		}
		notif.setTest("constraint_hpd_common_HcSpecialisationSyntax");
		notif.setDescription("HcSpecialisation attribute SHALL be formatted as follows : Issuing Authority:Code System:Code:CodeDisplayName (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-HcSpecialisationAttrTemplate-constraint_hpd_common_HcSpecialisationSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-037"));
		diagnostic.add(notif);
	}

}
