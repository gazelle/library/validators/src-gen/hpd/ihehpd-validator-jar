package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


public class MODIFYREQUESTPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public MODIFYREQUESTPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hpd.ModifyRequest){
			net.ihe.gazelle.hpd.ModifyRequest aClass = ( net.ihe.gazelle.hpd.ModifyRequest)obj;
			ModifyRequestSpecValidator._validateModifyRequestSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hpd.DsmlModification){
			net.ihe.gazelle.hpd.DsmlModification aClass = ( net.ihe.gazelle.hpd.DsmlModification)obj;
			ModifClinicalInformationContactAttrTemplateValidator._validateModifClinicalInformationContactAttrTemplate(aClass, location, diagnostic);
			ModifCredentialDescriptionAttrTemplateValidator._validateModifCredentialDescriptionAttrTemplate(aClass, location, diagnostic);
			ModifCredentialIssueDateAttrTemplateValidator._validateModifCredentialIssueDateAttrTemplate(aClass, location, diagnostic);
			ModifCredentialNameAttrTemplateValidator._validateModifCredentialNameAttrTemplate(aClass, location, diagnostic);
			ModifCredentialNumberAttrTemplateValidator._validateModifCredentialNumberAttrTemplate(aClass, location, diagnostic);
			ModifCredentialRenewalDateAttrTemplateValidator._validateModifCredentialRenewalDateAttrTemplate(aClass, location, diagnostic);
			ModifCredentialStatusAttrTemplateValidator._validateModifCredentialStatusAttrTemplate(aClass, location, diagnostic);
			ModifCredentialTypeAttrTemplateValidator._validateModifCredentialTypeAttrTemplate(aClass, location, diagnostic);
			ModifDisplayNameAttrTemplateValidator._validateModifDisplayNameAttrTemplate(aClass, location, diagnostic);
			ModifGenderAttrTemplateValidator._validateModifGenderAttrTemplate(aClass, location, diagnostic);
			ModifGeneralizedTimeAttrTemplateValidator._validateModifGeneralizedTimeAttrTemplate(aClass, location, diagnostic);
			ModifHcIdentifierAttrTemplateValidator._validateModifHcIdentifierAttrTemplate(aClass, location, diagnostic);
			ModifHcOrganizationCertificateAttrTemplateValidator._validateModifHcOrganizationCertificateAttrTemplate(aClass, location, diagnostic);
			ModifHcPracticeLocationAttrTemplateValidator._validateModifHcPracticeLocationAttrTemplate(aClass, location, diagnostic);
			ModifHcSigningCertificateAttrTemplateValidator._validateModifHcSigningCertificateAttrTemplate(aClass, location, diagnostic);
			ModifHcSpecialisationAttrTemplateValidator._validateModifHcSpecialisationAttrTemplate(aClass, location, diagnostic);
			ModifHpdCertificateAttrTemplateValidator._validateModifHpdCertificateAttrTemplate(aClass, location, diagnostic);
			ModifHpdCredentialAttrTemplateValidator._validateModifHpdCredentialAttrTemplate(aClass, location, diagnostic);
			ModifHpdHasAProviderAttrTemplateValidator._validateModifHpdHasAProviderAttrTemplate(aClass, location, diagnostic);
			ModifHpdHasAServiceAttrTemplateValidator._validateModifHpdHasAServiceAttrTemplate(aClass, location, diagnostic);
			ModifHpdHasAnOrgAttrTemplateValidator._validateModifHpdHasAnOrgAttrTemplate(aClass, location, diagnostic);
			ModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplateValidator._validateModifHpdMedicalRecordsDeliveryEmailAddressAttrTemplate(aClass, location, diagnostic);
			ModifHpdMemberIdAttrTemplateValidator._validateModifHpdMemberIdAttrTemplate(aClass, location, diagnostic);
			ModifHpdProviderLanguageSupportedAttrTemplateValidator._validateModifHpdProviderLanguageSupportedAttrTemplate(aClass, location, diagnostic);
			ModifHpdProviderLegalAddressAttrTemplateValidator._validateModifHpdProviderLegalAddressAttrTemplate(aClass, location, diagnostic);
			ModifHpdProviderStatusAttrTemplateValidator._validateModifHpdProviderStatusAttrTemplate(aClass, location, diagnostic);
			ModifHpdServiceAddressAttrTemplateValidator._validateModifHpdServiceAddressAttrTemplate(aClass, location, diagnostic);
			ModifHpdServiceIdAttrTemplateValidator._validateModifHpdServiceIdAttrTemplate(aClass, location, diagnostic);
			ModifMemberOfAttrTemplateValidator._validateModifMemberOfAttrTemplate(aClass, location, diagnostic);
			ModifMobileAttrTemplateValidator._validateModifMobileAttrTemplate(aClass, location, diagnostic);
			ModifNameDatatypeTemplateValidator._validateModifNameDatatypeTemplate(aClass, location, diagnostic);
			ModifObjectClassAttrTemplateValidator._validateModifObjectClassAttrTemplate(aClass, location, diagnostic);
			ModifPagerAttrTemplateValidator._validateModifPagerAttrTemplate(aClass, location, diagnostic);
			ModifPostalAddressAttrTemplateValidator._validateModifPostalAddressAttrTemplate(aClass, location, diagnostic);
			ModifTelephoneNumberAttrTemplateValidator._validateModifTelephoneNumberAttrTemplate(aClass, location, diagnostic);
			ModifTitleAttrTemplateValidator._validateModifTitleAttrTemplate(aClass, location, diagnostic);
			ModifUidAttrTemplateValidator._validateModifUidAttrTemplate(aClass, location, diagnostic);
			ModifUserCertificateAttrTemplateValidator._validateModifUserCertificateAttrTemplate(aClass, location, diagnostic);
			ModifUserSMIMECertificateAttrTemplateValidator._validateModifUserSMIMECertificateAttrTemplate(aClass, location, diagnostic);
		}
	
	}

}

