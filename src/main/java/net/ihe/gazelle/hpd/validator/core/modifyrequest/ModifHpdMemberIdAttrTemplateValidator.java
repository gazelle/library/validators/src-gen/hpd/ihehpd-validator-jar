package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHpdMemberIdAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHpdMemberIdAttrTemplateValidator{


    private ModifHpdMemberIdAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdMemberIdValueCard
	* hpdMemberId attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)
	*
	*/
	private static boolean _validateModifHpdMemberIdAttrTemplate_Constraint_hpd_modifyRequest_hpdMemberIdValueCard(net.ihe.gazelle.hpd.DsmlModification aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHpdMemberIdAttrTemplate
    * Verify if an element can be token as a Template of type modifHpdMemberIdAttrTemplate
	*
	*/
	private static boolean _isModifHpdMemberIdAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hpdmemberid");
				
	}
	/**
	* Validation of class-constraint : modifHpdMemberIdAttrTemplate
    * Verify if an element of type modifHpdMemberIdAttrTemplate can be validated by modifHpdMemberIdAttrTemplate
	*
	*/
	public static boolean _isModifHpdMemberIdAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHpdMemberIdAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHpdMemberIdAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHpdMemberIdAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHpdMemberIdAttrTemplate(aClass)){
			executeCons_ModifHpdMemberIdAttrTemplate_Constraint_hpd_modifyRequest_hpdMemberIdValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHpdMemberIdAttrTemplate_Constraint_hpd_modifyRequest_hpdMemberIdValueCard(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdMemberIdAttrTemplate_Constraint_hpd_modifyRequest_hpdMemberIdValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdMemberIdValueCard");
		notif.setDescription("hpdMemberId attribute SHALL be single valued (see Table 3.58.4.1.2.2.1-4: HPDProviderMembership Mandatory Attributes)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdMemberIdAttrTemplate-constraint_hpd_modifyRequest_hpdMemberIdValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-138"));
		diagnostic.add(notif);
	}

}
