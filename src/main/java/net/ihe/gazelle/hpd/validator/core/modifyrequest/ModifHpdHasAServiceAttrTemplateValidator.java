package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifHpdHasAServiceAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifHpdHasAServiceAttrTemplateValidator{


    private ModifHpdHasAServiceAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_hpdHasAServiceSyntax
	* hpdHasAService attribute SHALL be formatted as a distinguished name (DN) (see Table 3.58.4.1.2.2.1-1: HPDProvider Optional Attributes, CP-ITI-601-04)
	*
	*/
	private static boolean _validateModifHpdHasAServiceAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAServiceSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : modifHpdHasAServiceAttrTemplate
    * Verify if an element can be token as a Template of type modifHpdHasAServiceAttrTemplate
	*
	*/
	private static boolean _isModifHpdHasAServiceAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("hpdhasaservice");
				
	}
	/**
	* Validation of class-constraint : modifHpdHasAServiceAttrTemplate
    * Verify if an element of type modifHpdHasAServiceAttrTemplate can be validated by modifHpdHasAServiceAttrTemplate
	*
	*/
	public static boolean _isModifHpdHasAServiceAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifHpdHasAServiceAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifHpdHasAServiceAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifHpdHasAServiceAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifHpdHasAServiceAttrTemplate(aClass)){
			executeCons_ModifHpdHasAServiceAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAServiceSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifHpdHasAServiceAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAServiceSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifHpdHasAServiceAttrTemplate_Constraint_hpd_modifyRequest_hpdHasAServiceSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_hpdHasAServiceSyntax");
		notif.setDescription("hpdHasAService attribute SHALL be formatted as a distinguished name (DN) (see Table 3.58.4.1.2.2.1-1: HPDProvider Optional Attributes, CP-ITI-601-04)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifHpdHasAServiceAttrTemplate-constraint_hpd_modifyRequest_hpdHasAServiceSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-137"));
		diagnostic.add(notif);
	}

}
