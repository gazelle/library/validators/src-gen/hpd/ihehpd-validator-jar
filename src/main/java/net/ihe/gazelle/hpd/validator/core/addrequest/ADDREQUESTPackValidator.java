package net.ihe.gazelle.hpd.validator.core.addrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlAttr;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AddRequest;


public class ADDREQUESTPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ADDREQUESTPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hpd.AddRequest){
			net.ihe.gazelle.hpd.AddRequest aClass = ( net.ihe.gazelle.hpd.AddRequest)obj;
			AddRequestForHCProfessionalTemplateValidator._validateAddRequestForHCProfessionalTemplate(aClass, location, diagnostic);
			AddRequestForHCRegulatedOrganizationTemplateValidator._validateAddRequestForHCRegulatedOrganizationTemplate(aClass, location, diagnostic);
			AddRequestForHPDElectronicServiceTemplateValidator._validateAddRequestForHPDElectronicServiceTemplate(aClass, location, diagnostic);
			AddRequestForHPDProviderCredentialTemplateValidator._validateAddRequestForHPDProviderCredentialTemplate(aClass, location, diagnostic);
			AddRequestForHPDProviderMembershipTemplateValidator._validateAddRequestForHPDProviderMembershipTemplate(aClass, location, diagnostic);
			AddRequestForRelationshipTemplateValidator._validateAddRequestForRelationshipTemplate(aClass, location, diagnostic);
			AddRequestSpecValidator._validateAddRequestSpec(aClass, location, diagnostic);
		}
	
	}

}

