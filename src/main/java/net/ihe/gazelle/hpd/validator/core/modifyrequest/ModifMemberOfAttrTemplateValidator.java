package net.ihe.gazelle.hpd.validator.core.modifyrequest;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hpd.DsmlModification;

import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.DsmlModification;


 /**
  * class :        modifMemberOfAttrTemplate
  * package :   modifyRequest
  * Template Class
  * Template identifier : 
  * Class of test : DsmlModification
  * 
  */
public final class ModifMemberOfAttrTemplateValidator{


    private ModifMemberOfAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_modifyRequest_memberOfSyntax
	* memberOf attribute SHALL be formatted as a distinguish name (DN) (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateModifMemberOfAttrTemplate_Constraint_hpd_modifyRequest_memberOfSyntax(net.ihe.gazelle.hpd.DsmlModification aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getValue()) {
			    if (!aClass.matches(anElement1, "^(.+[=]{1}.+)([,{1}].+[=]{1}.+)*$")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : modifMemberOfAttrTemplate
    * Verify if an element can be token as a Template of type modifMemberOfAttrTemplate
	*
	*/
	private static boolean _isModifMemberOfAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return aClass.getName().toLowerCase().equals("memberof");
				
	}
	/**
	* Validation of class-constraint : modifMemberOfAttrTemplate
    * Verify if an element of type modifMemberOfAttrTemplate can be validated by modifMemberOfAttrTemplate
	*
	*/
	public static boolean _isModifMemberOfAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass){
		return _isModifMemberOfAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifMemberOfAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlModification
     * 
     */
    public static void _validateModifMemberOfAttrTemplate(net.ihe.gazelle.hpd.DsmlModification aClass, String location, List<Notification> diagnostic) {
		if (_isModifMemberOfAttrTemplate(aClass)){
			executeCons_ModifMemberOfAttrTemplate_Constraint_hpd_modifyRequest_memberOfSyntax(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifMemberOfAttrTemplate_Constraint_hpd_modifyRequest_memberOfSyntax(net.ihe.gazelle.hpd.DsmlModification aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifMemberOfAttrTemplate_Constraint_hpd_modifyRequest_memberOfSyntax(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_modifyRequest_memberOfSyntax");
		notif.setDescription("memberOf attribute SHALL be formatted as a distinguish name (DN) (see Table 3.58.4.1.2.2.2-1: Individual Provider Mapping and Table 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("modifyRequest-modifMemberOfAttrTemplate-constraint_hpd_modifyRequest_memberOfSyntax");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-126"));
		diagnostic.add(notif);
	}

}
