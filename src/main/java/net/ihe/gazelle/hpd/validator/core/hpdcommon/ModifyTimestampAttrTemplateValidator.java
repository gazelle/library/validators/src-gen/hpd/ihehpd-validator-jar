package net.ihe.gazelle.hpd.validator.core.hpdcommon;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;



import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlAttr;


 /**
  * class :        modifyTimestampAttrTemplate
  * package :   hpdCommon
  * Template Class
  * Template identifier : 
  * Class of test : DsmlAttr
  * 
  */
public final class ModifyTimestampAttrTemplateValidator{


    private ModifyTimestampAttrTemplateValidator() {}



	/**
	* Validation of instance by a constraint : constraint_hpd_common_modifyTimestampValueCard
	* modifyTimestamp SHALL be single valued (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping and 3.58.4.1.2.2.3-1: Organizational Provider Mapping)
	*
	*/
	private static boolean _validateModifyTimestampAttrTemplate_Constraint_hpd_common_modifyTimestampValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) <= new Integer(1));
		
	}

	/**
	* Validation of template-constraint by a constraint : modifyTimestampAttrTemplate
    * Verify if an element can be token as a Template of type modifyTimestampAttrTemplate
	*
	*/
	private static boolean _isModifyTimestampAttrTemplateTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return aClass.getName().toLowerCase().equals("modifytimestamp");
				
	}
	/**
	* Validation of class-constraint : modifyTimestampAttrTemplate
    * Verify if an element of type modifyTimestampAttrTemplate can be validated by modifyTimestampAttrTemplate
	*
	*/
	public static boolean _isModifyTimestampAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass){
		return _isModifyTimestampAttrTemplateTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   modifyTimestampAttrTemplate
     * class ::  net.ihe.gazelle.hpd.DsmlAttr
     * 
     */
    public static void _validateModifyTimestampAttrTemplate(net.ihe.gazelle.hpd.DsmlAttr aClass, String location, List<Notification> diagnostic) {
		if (_isModifyTimestampAttrTemplate(aClass)){
			executeCons_ModifyTimestampAttrTemplate_Constraint_hpd_common_modifyTimestampValueCard(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ModifyTimestampAttrTemplate_Constraint_hpd_common_modifyTimestampValueCard(net.ihe.gazelle.hpd.DsmlAttr aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateModifyTimestampAttrTemplate_Constraint_hpd_common_modifyTimestampValueCard(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("constraint_hpd_common_modifyTimestampValueCard");
		notif.setDescription("modifyTimestamp SHALL be single valued (see Tables 3.58.4.1.2.2.2-1: Individual Provider Mapping and 3.58.4.1.2.2.3-1: Organizational Provider Mapping)");
		notif.setLocation(location);
		notif.setIdentifiant("hpdCommon-modifyTimestampAttrTemplate-constraint_hpd_common_modifyTimestampValueCard");
		notif.getAssertions().add(new Assertion("IHEHPD","IHEHPD-010"));
		diagnostic.add(notif);
	}

}
