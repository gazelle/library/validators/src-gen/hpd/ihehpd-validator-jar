package net.ihe.gazelle.hpd.validator.hpdcommon.test;

import org.junit.Assert;
import org.junit.Test;

public class BinaryTest {

    HPDCommonTestUtil testExecutor = new HPDCommonTestUtil();

    @Test
    public void test_ok_constraint_hpd_common_HcSigningCertificateSyntax() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/binary-ok.xml",
                        "hpdCommon-HcSigningCertificateAttrTemplate-constraint_hpd_common_HcSigningCertificateSyntax"));
    }
}		
