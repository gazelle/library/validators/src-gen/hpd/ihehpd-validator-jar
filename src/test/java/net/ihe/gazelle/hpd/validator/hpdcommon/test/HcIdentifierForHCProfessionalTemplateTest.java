package net.ihe.gazelle.hpd.validator.hpdcommon.test;

import org.junit.Assert;
import org.junit.Test;

public class HcIdentifierForHCProfessionalTemplateTest {

    HPDCommonTestUtil testExecutor = new HPDCommonTestUtil();

    @Test
    public void test_ok_constraint_hpd_common_HcIdentifierValue_nonOID() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/hcIdentifier-ok.xml",
                        "hpdCommon-HcIdentifierAttrTemplate-constraint_hpd_common_HcIdentifierValue"));
    }

    @Test
    public void test_ok_constraint_hpd_common_HcIdentifierValue_OID() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/hcIdentifier-oid-ok.xml",
                        "hpdCommon-HcIdentifierAttrTemplate-constraint_hpd_common_HcIdentifierValue"));
    }

    @Test
    public void test_ko_constraint_hpd_common_HcIdentifierValue() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/hcIdentifier-ko.xml",
                        "hpdCommon-HcIdentifierAttrTemplate-constraint_hpd_common_HcIdentifierValue"));
    }

    @Test
    public void test_ok_constraint_hpd_common_HcIdentifierValue2_capitalizeStatus() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/hcIdentifier-ok.xml",
                        "hpdCommon-HcIdentifierAttrTemplate-constraint_hpd_common_HcIdentifierValue2"));
    }

    @Test
    public void test_ok_constraint_hpd_common_HcIdentifierValue2_lowerStatus() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/hcIdentifier-lowerStatus.xml",
                        "hpdCommon-HcIdentifierAttrTemplate-constraint_hpd_common_HcIdentifierValue2"));
    }

    @Test
    public void test_ko_constraint_hpd_common_HcIdentifierValue2() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/hcIdentifier-ko.xml",
                        "hpdCommon-HcIdentifierAttrTemplate-constraint_hpd_common_HcIdentifierValue2"));
    }

    @Test
    public void test_ok_constraint_hpd_common_HcSigningCertificateSyntax() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/binary-ok.xml",
                        "hpdCommon-HcSigningCertificateAttrTemplate-constraint_hpd_common_HcSigningCertificateSyntax"));
    }
}		
