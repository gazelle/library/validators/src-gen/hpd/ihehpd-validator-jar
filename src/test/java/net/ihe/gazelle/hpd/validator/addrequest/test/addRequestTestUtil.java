package net.ihe.gazelle.hpd.validator.addrequest.test;

import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.test.AbstractValidator;
import net.ihe.gazelle.hpd.validator.core.addrequest.ADDREQUESTPackValidator;
import net.ihe.gazelle.hpd.validator.core.hpdcommon.HPDCOMMONPackValidator;
import net.ihe.gazelle.validation.Notification;

import java.util.List;

public class addRequestTestUtil extends AbstractValidator<BatchRequest> {

    @Override
    protected void validate(BatchRequest message,
                            List<Notification> notifications) {
        BatchRequest.validateByModule(message, "/batchRequest", new ADDREQUESTPackValidator(), notifications);
    }

    @Override
    protected Class<BatchRequest> getMessageClass() {
        return BatchRequest.class;
    }
}
