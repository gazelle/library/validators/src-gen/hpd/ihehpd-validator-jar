package net.ihe.gazelle.hpd.validator.hpdcommon.test;

import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.test.AbstractValidator;
import net.ihe.gazelle.hpd.validator.core.hpdcommon.HPDCOMMONPackValidator;
import net.ihe.gazelle.validation.Notification;

import java.util.List;

public class HPDCommonTestUtil extends AbstractValidator<BatchRequest> {

    static {
        CommonOperations.setValueSetProvider(new SVSConsumer() {
            @Override
            protected String getSVSRepositoryUrl() {
                return "https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
            }
        });
    }

    @Override
    protected void validate(BatchRequest message,
                            List<Notification> notifications) {
        BatchRequest.validateByModule(message, "/batchRequest", new HPDCOMMONPackValidator(), notifications);
    }

    @Override
    protected Class<BatchRequest> getMessageClass() {
        return BatchRequest.class;
    }
}

