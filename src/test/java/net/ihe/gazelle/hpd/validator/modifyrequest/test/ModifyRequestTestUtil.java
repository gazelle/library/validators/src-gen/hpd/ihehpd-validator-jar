package net.ihe.gazelle.hpd.validator.modifyrequest.test;

import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.test.AbstractValidator;
import net.ihe.gazelle.hpd.validator.core.addrequest.ADDREQUESTPackValidator;
import net.ihe.gazelle.hpd.validator.core.modifyrequest.MODIFYREQUESTPackValidator;
import net.ihe.gazelle.validation.Notification;

import java.util.List;

public class ModifyRequestTestUtil extends AbstractValidator<BatchRequest> {

    @Override
    protected void validate(BatchRequest message,
                            List<Notification> notifications) {
        BatchRequest.validateByModule(message, "/batchRequest", new MODIFYREQUESTPackValidator(), notifications);
    }

    @Override
    protected Class<BatchRequest> getMessageClass() {
        return BatchRequest.class;
    }
}
