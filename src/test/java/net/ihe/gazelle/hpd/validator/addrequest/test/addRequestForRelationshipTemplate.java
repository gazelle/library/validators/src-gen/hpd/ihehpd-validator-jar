package net.ihe.gazelle.hpd.validator.addrequest.test;

import net.ihe.gazelle.hpd.validator.hpdcommon.test.HPDCommonTestUtil;
import org.junit.Assert;
import org.junit.Test;

public class addRequestForRelationshipTemplate {

    HPDCommonTestUtil testExecutor = new HPDCommonTestUtil();

    @Test
    public void test_ok_constraint_hpd_addRequest_ownerSV() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ownerSV-ok.xml",
                        "addRequest-addRequestForRelationshipTemplate-constraint_hpd_addRequestRelationship_ownerSV"));
    }

    @Test
    public void test_ko_constraint_hpd_addRequest_ownerSV() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ownerSV-ko.xml",
                        "addRequest-addRequestForRelationshipTemplate-constraint_hpd_addRequestRelationship_ownerSV"));
    }
}
