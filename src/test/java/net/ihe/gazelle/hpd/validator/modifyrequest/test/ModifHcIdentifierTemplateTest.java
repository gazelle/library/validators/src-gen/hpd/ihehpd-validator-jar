package net.ihe.gazelle.hpd.validator.modifyrequest.test;

import net.ihe.gazelle.hpd.validator.hpdcommon.test.HPDCommonTestUtil;
import org.junit.Assert;
import org.junit.Test;

/**
 * <p>ModifHcIdentifierTemplateTest class.</p>
 *
 * @author aberge
 * @version 1.0: 24/10/17
 */

public class ModifHcIdentifierTemplateTest {

    ModifyRequestTestUtil testExecutor = new ModifyRequestTestUtil();

    @Test
    public void test_ok_constraint_hpd_modifyRequest_HcIdentifierValue() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/request-modify.xml",
                        "modifyRequest-modifHcIdentifierAttrTemplate-constraint_hpd_modifyRequest_HcIdentifierValue"));
    }
}
