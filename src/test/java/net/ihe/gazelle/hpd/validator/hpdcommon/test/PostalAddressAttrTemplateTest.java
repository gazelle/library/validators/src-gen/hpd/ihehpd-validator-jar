package net.ihe.gazelle.hpd.validator.hpdcommon.test;

import org.junit.Assert;
import org.junit.Test;

public class PostalAddressAttrTemplateTest {

    HPDCommonTestUtil testExecutor = new HPDCommonTestUtil();

    @Test
    public void test_ok_constraint_hpd_common_PostalAddressStatus() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/postalAddressStatus-ok.xml",
                        "hpdCommon-PostalAddressAttrTemplate-constraint_hpd_common_PostalAddressStatus"));
    }

    @Test
    public void test_ok_constraint_hpd_common_PostalAddressAddrMandatory() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/postalAddressAddr-ok.xml",
                        "hpdCommon-PostalAddressAttrTemplate-constraint_hpd_common_PostalAddressAddrMandatory"));
    }
}		
