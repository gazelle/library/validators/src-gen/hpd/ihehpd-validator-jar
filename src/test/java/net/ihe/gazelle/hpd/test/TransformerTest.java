package net.ihe.gazelle.hpd.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.BatchResponse;
import net.ihe.gazelle.hpdTransformer.HPDTransformer;

import org.junit.Test;

public class TransformerTest {

    /**
     * @param args
     *
     * @throws JAXBException
     * @throws FileNotFoundException
     */
    @Test
    public void transformResponseTest() {
        try {
            BatchResponse message = HPDTransformer.unmarshallMessage(BatchResponse.class, new FileInputStream(new File
                    ("src/test/resources/searchResponseSample.xml")));
            if (message != null) {
                HPDTransformer.marshallMessage(BatchResponse.class, System.out, message);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void transformRequestTest() {
        try {
            BatchRequest message = HPDTransformer.unmarshallMessage(BatchRequest.class, new FileInputStream(new File
                    ("src/test/resources/searchRequestSample.xml")));
            if (message != null) {
                HPDTransformer.marshallMessage(BatchRequest.class, System.out, message);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
