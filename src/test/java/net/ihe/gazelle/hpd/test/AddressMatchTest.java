package net.ihe.gazelle.hpd.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddressMatchTest {

    private static Boolean matches(String value, String regex) {
        Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(value.toLowerCase());
        return matcher.matches();
        // return value.matches(regex);
    }

    public static void main(String[] args) {
        String pattern1 = ".*(addr=){1}.*";
        String pattern2 = ".*(status=primary|status=secondary|status=inactive){1}.*";
        String pattern2bis = ".*(status=){1}.*";
        String pattern3 = "[^=\\$]*={1}[^=\\$]*(\\${1}[^=\\$]*={1}[^=\\$]*)*";

        String test = "status=primary\n"
                + "$ streetNumber=890 \n"
                + "$ street Name=Network Lane\n"
                + "$ city=Healthy Living \n"
                + "$ state=Medstate \n"
                + "$ postalCode=46578-1234 $ addr=890 Network Lane, Healthy Living, Medstate 44456542";

        System.out.println("Test1: " + matches(test, pattern1));
//		System.out.println("Test2: " + matches(test, pattern2));
        System.out.println("Test2 bis: " + (matches(test, pattern2) ^ !matches(test, pattern2bis)));
        System.out.println("Test3: " + matches(test, pattern3));
    }
}
